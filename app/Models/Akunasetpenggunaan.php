<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Akunasetpenggunaan extends Model
{
    use HasFactory;
    protected $guarded = [];
}
