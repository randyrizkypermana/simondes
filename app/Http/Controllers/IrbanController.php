<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Akunkel;
use App\Models\Akunwil;
use App\Models\Asal;
use App\Models\Datum;
use App\Models\Datum_perangkat;
use App\Models\Kecamatan;
use App\Models\Nilai_pemdes;
use App\Models\Rpjmd;

class IrbanController extends Controller
{
    public function index(Request $request)
    {
        $tahun = now()->format('Y');
        if ($request->tahun) {
            $tahun = $request->tahun;
        }
        $infos = Admin::with('asal')->where('id', session('loggedIrban'))->first();
        $kecamatan = Asal::where('kecamatan', '!=', '')
            ->orderBy('kecamatan')
            ->distinct()
            ->get('kecamatan');

        return view('irban.index', [
            'infos' => $infos,
            'tahun' => $tahun,
            'kecamatans' => $kecamatan,
        ]);
    }

    public function lihat($subindikator, $id_desa, $tahun)
    {
        $kecamatan = Asal::where('id', $id_desa)->get('kecamatan');
        $infos = Admin::where('id', session('loggedIrban'))->first();

        if ($subindikator == 'dasarhukum') {
            $dasar = ['dasar_hukum'];
            $dasarhukum = Datum::where([
                'jenis' => 'kewilayahan',
                'asal_id' => $id_desa,
                'tahun' => $tahun
            ])->whereIn('nama_data', $dasar)->get();

            return view('irban.kewilayahan.dasarhukum',  [
                'kecamatans' => $kecamatan,
                'infos' => $infos,
                'dasarhukum' => $dasarhukum,
                'dataAkun' => Akunwil::where([
                    'asal_id' => $id_desa,
                    'tahun' => $tahun
                ])->get()
            ]);
        } else if ($subindikator == 'patokbatas') {
            $patok = ['batas_utara', 'batas_selatan', 'batas_barat', 'batas_timur'];
            $patoks = Datum::where([
                'jenis' => 'kewilayahan',
                'asal_id' => $id_desa,
                'tahun' => $tahun
            ])->whereIn('nama_data', $patok)->get();

            return view('irban.kewilayahan.patokbatas',  [
                'kecamatans' => $kecamatan,
                'infos' => $infos,
                'patok' => $patoks,
                'dataAkun' => Akunwil::where([
                    'asal_id' => $id_desa,
                    'tahun' => $tahun
                ])->get()
            ]);
        } else if ($subindikator == 'petabatas') {
            $peta = ['luas_wilayah'];
            $petas = Datum::where([
                'jenis' => 'kewilayahan',
                'asal_id' => $id_desa,
                'tahun' => $tahun
            ])->whereIn('nama_data', $peta)->get();

            return view('irban.kewilayahan.petabatas',  [
                'kecamatans' => $kecamatan,
                'infos' => $infos,
                'peta' => $petas,
                'dataAkun' => Akunwil::where([
                    'asal_id' => $id_desa,
                    'tahun' => $tahun
                ])->get()
            ]);
        }
        if ($subindikator == 'pddusia') {
            $usia = ['usia_0_15', 'usia_15_65', 'usia_65_keatas'];
            $damon = Datum::where([
                'jenis' => 'kependudukan',
                'asal_id' => $id_desa,
                'tahun' => $tahun
            ])->whereIn('nama_data', $usia)->get();

            return view('irban.monografi.pddusia',  [
                'kecamatans' => $kecamatan,
                'infos' => $infos,
                'damon' => $damon,
            ]);
        }
        if ($subindikator == 'pddmiskin') {
            $miskin = ['penduduk_miskin', 'kk_miskin'];
            $damon = Datum::where([
                'jenis' => 'kependudukan',
                'asal_id' => $id_desa,
                'tahun' => $tahun
            ])->whereIn('nama_data', $miskin)->get();

            return view('irban.monografi.pddmiskin',  [
                'kecamatans' => $kecamatan,
                'infos' => $infos,
                'damon' => $damon,
            ]);
        }
        if ($subindikator == 'ibadah') {
            $ibadah = ['mesjid', 'mushola', 'gereja', 'pura', 'vihara', 'klenteng'];
            $damon = Datum::where([
                'jenis' => 'sarpras',
                'asal_id' => $id_desa,
                'tahun' => $tahun
            ])->whereIn('nama_data', $ibadah)->get();

            return view('irban.monografi.ibadah',  [
                'kecamatans' => $kecamatan,
                'infos' => $infos,
                'damon' => $damon,
            ]);
        }
        if ($subindikator == 'pendidikan') {
            $pendidikan = ['tk', 'sd', 'smp', 'sma', 'ponpes'];
            $damon = Datum::where([
                'jenis' => 'sarpras',
                'asal_id' => $id_desa,
                'tahun' => $tahun
            ])->whereIn('nama_data', $pendidikan)->get();

            return view('irban.monografi.pendidikan',  [
                'kecamatans' => $kecamatan,
                'infos' => $infos,
                'damon' => $damon,
            ]);
        }
        if ($subindikator == 'kesehatan') {
            $kesehatan = ['puskesmas', 'pustu', 'poskesdes', 'posyandu', 'polindes'];
            $damon = Datum::where([
                'jenis' => 'sarpras',
                'asal_id' => $id_desa,
                'tahun' => $tahun
            ])->whereIn('nama_data', $kesehatan)->get();

            return view('irban.monografi.kesehatan',  [
                'kecamatans' => $kecamatan,
                'infos' => $infos,
                'damon' => $damon,
            ]);
        }
    }

    public function kewilayahan(Request $request)
    {
        $id_desa = session('id');
        $tahun = session('tahun');
        $infos = Admin::where('id', session('loggedIrban'))->first();
        $kecamatan = Asal::where('kecamatan', '!=', '')
            ->orderBy('kecamatan')
            ->distinct()
            ->get('kecamatan');

        $subdasar = ['Dasar Hukum Pembentukan Desa'];
        $hasdasarhukum = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $subdasar)->get();

        $subpatok = ['Patok Batas Desa'];
        $haspatokbatas = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $subpatok)->get();

        $subpeta = ['Peta Batas Desa'];
        $haspetabatas = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $subpeta)->get();

        return view('irban.kewilayahan.index', [
            'infos' => $infos,
            'kecamatans' => $kecamatan,
            'hasdasarhukum' => $hasdasarhukum,
            'haspatokbatas' => $haspatokbatas,
            'haspetabatas' => $haspetabatas
        ]);
    }

    public function monografi(Request $request)
    {
        $id_desa = session('id');
        $tahun = session('tahun');
        $infos = Admin::where('id', session('loggedIrban'))->first();
        $kecamatan = Asal::where('kecamatan', '!=', '')
            ->orderBy('kecamatan')
            ->distinct()
            ->get('kecamatan');

        $nilai_pddusia = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Jumlah Penduduk Per Kelompok Usia'
        ])->get();

        $nilai_pddmiskin = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Jumlah Penduduk Miskin'
        ])->get();

        $nilai_pddpekerjaan = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Jumlah Penduduk Berdasarkan Pekerjaan'
        ])->get();

        $nilai_saribadah = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Jumlah Sarana Ibadah'
        ])->get();

        $nilai_sarpendidikan = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Jumlah Sarana Pendidikan'
        ])->get();

        $nilai_sarkesehatan = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Jumlah Sarana Kesehatan'
        ])->get();

        $nilai_jumumkm = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Jumlah UMKM'
        ])->get();

        $nilai_pjgjalan = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Panjang Jalan Desa'
        ])->get();

        $nilai_jmljembatan = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Jumlah Jembatan'
        ])->get();

        return view('/irban.monografi.index', [
            'infos' => $infos,
            'kecamatans' => $kecamatan,
            'nilai_pddusias' => $nilai_pddusia,
            'nilai_pddmiskins' => $nilai_pddmiskin,
            'nilai_pddpekerjaans' => $nilai_pddpekerjaan,
            'nilai_saribadahs' => $nilai_saribadah,
            'nilai_sarpendidikans' => $nilai_sarpendidikan,
            'nilai_sarkesehatans' => $nilai_sarkesehatan,
            'nilai_jumumkms' => $nilai_jumumkm,
            'nilai_pjgjalans' => $nilai_pjgjalan,
            'nilai_jmljembatans' => $nilai_jmljembatan,
        ]);
    }

    public function perencanaan(Request $request)
    {
        $id_desa = session('id');
        $tahun = session('tahun');
        $infos = Admin::where('id', session('loggedIrban'))->first();
        $kecamatan = Asal::where('kecamatan', '!=', '')
            ->orderBy('kecamatan')
            ->distinct()
            ->get('kecamatan');

        $nilai_rpjmdes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'RPJMDes'
        ])->get();

        $nilai_bacmusdes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'BAC MUSDES'
        ])->get();

        $nilai_bacmusren = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'BAC MUSRENBANGDES'
        ])->get();

        $nilai_sktimrkpdes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'SK Tim Penyusunan RKPDes'
        ])->get();

        $nilai_rkpdes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Peraturan Desa dan Dokumen RKPDes'
        ])->get();

        $nilai_wakturkpdes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Ketepatan Waktu Penetapan Peraturan Desa RKPDes'
        ])->get();

        $nilai_rapbdes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Dokumen RAPBDes'
        ])->get();

        $nilai_bacrapbdes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'BAC Pembahasan RAPBDes'
        ])->get();

        $nilai_kepbpd = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Keputusan BPD tentang Persetujuan APBDes'
        ])->get();

        $nilai_evaluasiapbdes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Hasil Evaluasi APBDes oleh Kecamatan'
        ])->get();

        $nilai_printapbdes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Dokumen APBDes Print Out Siskeudes'
        ])->get();

        $nilai_rab = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Desain Gambar dan RAB'
        ])->get();

        $nilai_waktuapbdes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Ketepatan Waktu Penetapan Peraturan Desa APBDes'
        ])->get();

        $nilai_penjabaranapbdes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun'   => $tahun,
            'subindikator' => 'Peraturan Kepala Desa tentang Penjabaran APBDes'
        ])->get();

        return view('/irban.perencanaan.index', [
            'infos' => $infos,
            'kecamatans' => $kecamatan,
            'nilai_rpjmdes' => $nilai_rpjmdes,
            'nilai_bacmusdes' => $nilai_bacmusdes,
            'nilai_bacmusrens' => $nilai_bacmusren,
            'nilai_sktimrkpdes' => $nilai_sktimrkpdes,
            'nilai_rkpdes' => $nilai_rkpdes,
            'nilai_wakturkpdes' => $nilai_wakturkpdes,
            'nilai_rapbdes' => $nilai_rapbdes,
            'nilai_bacrapbdes' => $nilai_bacrapbdes,
            'nilai_kepbpds' => $nilai_kepbpd,
            'nilai_evaluasiapbdes' => $nilai_evaluasiapbdes,
            'nilai_printapbdes' => $nilai_printapbdes,
            'nilai_rabs' => $nilai_rab,
            'nilai_waktuapbdes' => $nilai_waktuapbdes,
            'nilai_penjabaranapbdes' => $nilai_penjabaranapbdes,

        ]);
    }

    public function lihat_perencanaan($subindikator, $id_desa, $tahun)
    {
        $kecamatan = Asal::where('id', $id_desa)->get('kecamatan');
        $infos = Admin::where('id', session('loggedIrban'))->first();

        if ($subindikator == 'rpjmdes') {
            $data = ['dokumen', 'visi_misi'];
            $rpjmdes = Rpjmd::where([
                'asal_id' => $id_desa,
                'tahun' => $tahun
            ])->whereIn('jenis', $data)->get();

            $misi = Rpjmd::where([
                'jenis' => 'visi_misi',
                'asal_id' => $id_desa,
                'tahun' => $tahun
            ])->where('nama_data', '!=', 'visi')->get();
        }

        return view('irban.perencanaan.rpjmdes',  [
            'kecamatans' => $kecamatan,
            'infos' => $infos,
            'rpjmdes' => $rpjmdes,
            'misis' => $misi,
        ]);
    }

    public function kelembagaan(Request $request)
    {
        $id_desa = session('id');
        $tahun = session('tahun');
        $infos = Admin::where('id', session('loggedIrban'))->first();
        $kecamatan = Asal::where('kecamatan', '!=', '')
            ->orderBy('kecamatan')
            ->distinct()
            ->get('kecamatan');

        $subsotk = ['Perdes SOTK'];
        $hassotk = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $subsotk)->get();

        $subperangkat = ['SK Kades dan Perangkat Desa'];
        $hasperangkat = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $subperangkat)->get();

        $subrt = ['SK RT'];
        $hasrt = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $subrt)->get();

        $sublpm = ['SK LPM'];
        $haslpm = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $sublpm)->get();

        $subkarang = ['SK Karang Taruna'];
        $haskarang = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $subkarang)->get();

        $sublinmas = ['SK Linmas'];
        $haslinmas = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $sublinmas)->get();

        $subbpd = ['SK BPD'];
        $hasbpd = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $subbpd)->get();

        $subpkk = ['SK PKK'];
        $haspkk = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $subpkk)->get();

        $subkandes = ['Keberadaan dan Keberfungsian Kantor Desa'];
        $haskandes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $subkandes)->get();

        $subkanbpd = ['Sekretariat/Kantor BPD'];
        $haskanbpd = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $subkanbpd)->get();

        $subkanbumdes = ['Sekretariat/Kantor BUM Desa'];
        $haskanbumdes = Nilai_pemdes::where([
            'asal_id' => $id_desa,
            'tahun' => $tahun,
        ])->whereIn('subindikator_id', $subkanbumdes)->get();

        return view('irban.kelembagaan.index', [
            'infos' => $infos,
            'kecamatans' => $kecamatan,
            'hassotk' => $hassotk,
            'hasperangkat' => $hasperangkat,
            'hasrt' => $hasrt,
            'haslpm' => $haslpm,
            'haskarang' => $haskarang,
            'haslinmas' => $haslinmas,
            'hasbpd' => $hasbpd,
            'haspkk' => $haspkk,
            'haskandes' => $haskandes,
            'haskanbpd' => $haskanbpd,
            'haskanbumdes' => $haskanbumdes
        ]);
    }

    public function lihatkelembagaan($subindikator, $id_desa, $tahun)
    {
        $kecamatan = Asal::where('id', $id_desa)->get('kecamatan');
        $infos = Admin::where('id', session('loggedIrban'))->first();

        if ($subindikator == 'sotk') {
            $sotk = ['sotk'];
            $perdessotk = Akunkel::where([
                'asal_id' => $id_desa,
                'tahun' => $tahun
            ])->whereIn('nama_data', $sotk)->get();

            return view('irban.kelembagaan.sotk',  [
                'kecamatans' => $kecamatan,
                'infos' => $infos,
                'perdessotk' => $perdessotk
            ]);
        } elseif ($subindikator == 'perangkat') {

            $kades = Datum_perangkat::where([
                'asal_id' => $id_desa,
                'tahun' => $tahun,
                'jabatan' => 'Kepala Desa'
            ])->get();

            $sekdes = Datum_perangkat::where([
                'asal_id' => $id_desa,
                'tahun' => $tahun,
                'jabatan' => 'Sekretaris Desa'
            ])->get();

            $kaurum = Datum_perangkat::where([
                'asal_id' => $id_desa,
                'tahun' => $tahun,
                'jabatan' => 'Kaur Umum'
            ])->get();

            $kaurpr = Datum_perangkat::where([
                'asal_id' => $id_desa,
                'tahun' => $tahun,
                'jabatan' => 'Kaur Perencanaan'
            ])->get();

            $kaurkeu = Datum_perangkat::where([
                'asal_id' => $id_desa,
                'tahun' => $tahun,
                'jabatan' => 'Kaur Keuangan'
            ])->get();

            $kasipem = Datum_perangkat::where([
                'asal_id' => $id_desa,
                'tahun' => $tahun,
                'jabatan' => 'Kasi Pemerintahan'
            ])->get();

            $kasikes = Datum_perangkat::where([
                'asal_id' => $id_desa,
                'tahun' => $tahun,
                'jabatan' => 'Kasi Kesra'
            ])->get();

            $kasipel = Datum_perangkat::where([
                'asal_id' => $id_desa,
                'tahun' => $tahun,
                'jabatan' => 'Kasi Pelayanan'
            ])->get();

            return view('irban.kelembagaan.perangkat',  [
                'kecamatans' => $kecamatan,
                'infos' => $infos,
                'kades' => $kades,
                'sekdes' => $sekdes,
                'kaurum' => $kaurum,
                'kaurpr' => $kaurpr,
                'kaurkeu' => $kaurkeu,
                'kasipem' => $kasipem,
                'kasikes' => $kasikes,
                'kasipel' => $kasipel
            ]);
        }
    }

    public function nilaipemdes(Request $request)
    {
        $data = [
            'asal_id'           => $request->asal_id,
            'tahun'             => $request->tahun,
            'indikator'      => $request->indikator,
            'subindikator'   => $request->subindikator,
            'catatan'           => $request->catatan,
            'rekomendasi'       => $request->rekomendasi,
            'status'            => $request->status,
            'nilai'             => $request->nilai,
            'verifikator'       => $request->verifikator,
            'obrik'             => $request->verval,
            'verval'            => 1
        ];

        Nilai_pemdes::create($data);
        return back()->with('success', 'Validasi dan Verifikasi Berhasil');
    }

    public function updatenilaipemdes(Request $request)
    {
        $nilaipemdes = Nilai_pemdes::findOrFail($request->id);
        $nilaipemdes->update([
            'catatan'        => $request->catatan,
            'rekomendasi'    => $request->rekomendasi,
            'status'         => $request->status,
            'nilai'          => $request->nilai,
            'verifikator'   => $request->verifikator,
            'obrik'         => $request->verval,
            'updated_at'     => $request->waktu_penilaian,
        ]);
        return back()->with('update', 'Ubah Validasi dan Verifikasi Berhasil');
    }


    public function getKecamatan(Request $request)
    {
        $tahuns = $request->post('tahun');
        $obrik = substr($tahuns, 4);
        $tahun = substr($tahuns, 0, 4);
        $kecamatan = Kecamatan::where([
            'obrik' => $obrik,
            'tahun' => $tahun,
        ])->orderBy('kecamatan')->get();
        $html = '<option value="">Pilih Kecamatan</option>';
        foreach ($kecamatan as $kc) {
            $html .= '<option value="' . $kc->kecamatan . '">' . $kc->kecamatan . '</option>';
        }
        echo $html;
    }

    public function getDesa(Request $request)
    {
        $kecamatan = $request->post('kecamatan');
        $desa = Asal::where('kecamatan', $kecamatan)->orderBy('asal')->get();
        $html = '<option value="">Pilih Desa</option>';
        foreach ($desa as $ds) {
            $html .= '<option value="' . $ds->id . '">' . $ds->asal . '</option>';
        }
        echo $html;
    }

    public function sesi(Request $request)
    {
        $tahun = substr($request->tahun, 0, 4);
        $request->validate([
            'kecamatan' => 'not_in:0',
            'desa' => 'not_in:0',
            'tahun' => 'not_in:0',
        ]);
        $desa = Asal::where(['id' => $request->desa])->get('asal');
        session()->forget('id');
        session()->forget('desa');
        session()->forget('kecamatan');
        session()->forget('tahun');
        $request->session()->put('id', $request->desa);
        $request->session()->put('desa', $desa[0]->asal);
        $request->session()->put('kecamatan', $request->kecamatan);
        $request->session()->put('tahun', $tahun);

        return back();
    }

    public function logoutIrban(Request $request)
    {
        if (session()->has('loggedIrban')) {
            session()->pull('loggedIrban');
            session()->pull('loggedAdmin');
            $request->session()->invalidate();
            $request->session()->regenerateToken();
            return redirect('/');
        }
    }
}
