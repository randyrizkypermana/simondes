<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Penataanbelanja_spp;
use App\Models\Apbdes_kegiatan;
use App\Models\Total_apbd;
use App\Models\Penataanbelanja_bkp;

use Illuminate\Support\Facades\Storage;

class PenataanBelanjaController extends Controller
{
    public function formPenataanBelanja(Request $request)
    {

        $tahun = now()->format('Y');
        if ($request->tahun) {
            $tahun = $request->tahun;
        }
        $infos = Admin::with('asal')->where('id', session('loggedAdminDesa'))->first();

        $spp = Penataanbelanja_spp::where([
            'asal_id' => $infos->asal_id,
            'tahun' => $tahun,
        ])->first();
        $bkp = Penataanbelanja_bkp::where([
            'asal_id' => $infos->asal_id,
            'tahun' => $tahun,
        ])->first();

        if (isset($request->jenis)) {
            if ($request->jenis == 'spp' || $request->jenis == 'spp_kegiatan') {
                $datum = Penataanbelanja_spp::where([
                    'asal_id' => $infos->asal_id,
                    'tahun' => $tahun
                ])->get();
            } elseif ($request->jenis == 'tbpu' || $request->jenis == 'cek_tbpu' || $request->jenis == 'bukti_belanja') {
                $datum = Penataanbelanja_bkp::where([
                    'asal_id' => $infos->asal_id,
                    'tahun' => $tahun
                ])->get();
            }

            $data = [
                'infos' => $infos,
                'jenis' => $request->jenis,
                'tahun' => $tahun,
                'spp' => $spp,
                'bkp' => $bkp
            ];

            if (count($datum) == 0) {


                $data['apbdes_kegiatans'] = Apbdes_kegiatan::where([
                    'asal_id' => $infos->asal_id,
                    'tahun' => $tahun
                ])->get();

                $data['total_p'] = Total_apbd::where([
                    'asal_id' => $infos->asal_id,
                    'tahun' => $tahun
                ])->first();
                if (!$data['total_p']) {
                    return back()->with('dok', 'Silahkan Isi Data APBDes Terlebih Dahulu!');
                }
                if ($data['total_p']->belanja_perubahan) {
                    $data['anggaran'] = 'perubahan';
                } else {
                    $data['anggaran'] = 'murni';
                }

                if ($request->jenis == 'spp_kegiatan' && $request->kegiatan) {
                    $data['kegiatan_spp'] = Apbdes_kegiatan::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun,
                        'id' => $request->kegiatan
                    ])->first();
                }
                if ($request->jenis == 'cek_tbpu' && $request->kegiatan) {
                    $data['kegiatan_tbpu'] = Apbdes_kegiatan::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun,
                        'id' => $request->kegiatan
                    ])->first();
                }

                return view('adminDesa.akunBelanja.formPenataanBelanja_t', $data);
            } else {
                if ($request->jenis == 'spp' || $request->jenis == 'spp_kegiatan') {

                    $data['apbdes_kegiatans'] = Apbdes_kegiatan::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun
                    ])->get();


                    $data['total_p'] = Total_apbd::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun
                    ])->first();
                    if ($data['total_p']->belanja_perubahan) {
                        $data['anggaran'] = 'perubahan';
                    } else {
                        $data['anggaran'] = 'murni';
                    }
                }

                if ($request->jenis == 'spp_kegiatan' && $request->kegiatan) {
                    $data['kegiatan_spp'] = Apbdes_kegiatan::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun,
                        'id' => $request->kegiatan
                    ])->first();
                }

                if ($request->jenis == 'tbpu' || $request->jenis == 'cek_tbpu') {
                    $data['apbdes_kegiatans'] = Apbdes_kegiatan::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun
                    ])->get();

                    $data['total_p'] = Total_apbd::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun
                    ])->first();
                    if ($data['total_p']->belanja_perubahan) {
                        $data['anggaran'] = 'perubahan';
                    } else {
                        $data['anggaran'] = 'murni';
                    }

                    $data['tbpus'] = Penataanbelanja_bkp::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun
                    ])->get();
                }

                if ($request->jenis == 'cek_tbpu' && $request->kegiatan) {
                    $data['kegiatan_tbpu'] = Apbdes_kegiatan::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun,
                        'id' => $request->kegiatan
                    ])->first();
                }

                if ($request->jenis == 'bukti_belanja') {
                    $data['total_p'] = Total_apbd::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun
                    ])->first();
                    if ($data['total_p']->belanja_perubahan) {
                        $data['anggaran'] = 'perubahan';
                    } else {
                        $data['anggaran'] = 'murni';
                    }

                    $data['tbpus'] = Penataanbelanja_bkp::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun,
                        'file_lampiran' => NULL
                    ])->get();
                }


                // return $data['tbpus']->where('kegiatan_id', 1)->where('belanja_id', 2);
                // return $data['apbdes_kegiatans'];
                // return $data['apbdes_kegiatans'][0]->penataanbelanja_spp[0]->jumlah;


                return view('adminDesa.akunBelanja.formPenataanBelanja_e', $data);
            }
        } else {
            $data = [
                'infos' => $infos,
                'jenis' => $request->jenis,
                'tahun' => $tahun,
                'spp' => $spp,
                'bkp' => $bkp
            ];
            return view('adminDesa.akunBelanja.formPenataanBelanja', $data);
        }
    }

    public function tambahSPP(Request $request)
    {
        $request->validate([
            'nomor' => 'required|max:50',
            'tanggal' => 'required',
            'jumlah' => 'required',
            'file_spp' => 'required|file|mimes:pdf|max:1024'
        ]);
        $data = [
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'kegiatan_id' => $request->kegiatan_id,
            'apbdes_kegiatan_id' => $request->apbdes_kegiatan_id,
            'nomor' => strip_tags($request->nomor),
            'tanggal' => strip_tags($request->tanggal),
            'jumlah' => strip_tags($request->jumlah)

        ];
        $ext = $request->file_spp->extension();
        $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_belanja";
        $file1 = $request->file('file_spp')->storeAs($folder, "file_spp_$request->apbdes_kegiatan_id" . "-" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

        $data['file_spp'] = $file1;

        Penataanbelanja_spp::create($data);
        return back()->with('success', 'berhasil tambah SPP');
    }

    public function tambahTBPU(Request $request)
    {

        $request->validate([
            'nomor' => 'required|max:50',
            'tanggal' => 'required',
            'jumlah' => 'required',
            'sebagai' => 'required',
            'file_bkp' => 'required|file|mimes:pdf|max:1024',
            'file_lampiran' => 'file|mimes:pdf|max:2048'
        ]);
        $data = [
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'apbdes_kegiatan_id' => $request->apbdes_kegiatan_id,
            'belanja_id' => $request->belanja_id,
            'nomor' => strip_tags($request->nomor),
            'tanggal' => strip_tags($request->tanggal),
            'jumlah' => strip_tags($request->jumlah),
            'sebagai' => strip_tags($request->sebagai),
            'pph' =>  strip_tags($request->pph),
            'ppn' =>  strip_tags($request->ppn),

        ];
        $ext = $request->file_bkp->extension();
        $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_belanja";
        $file1 = $request->file('file_bkp')->storeAs($folder, "file_bkp_$request->apbdes_kegiatan_id" . "-" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

        $data['file_bkp'] = $file1;



        if ($request->file_lampiran) {
            $ext = $request->file_lampiran->extension();
            $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_belanja";
            $file2 = $request->file('file_lampiran')->storeAs($folder, "file_lampiran_$request->apbdes_kegiatan_id" . "-" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

            $data['file_lampiran'] = $file2;
        }

        Penataanbelanja_bkp::create($data);

        return back()->with('success', 'berhasil tambah TBPU');
    }

    public function updateTBPU(Request $request)
    {

        $request->validate([
            'nomor' => 'required|max:50',
            'tanggal' => 'required',
            'jumlah' => 'required',
            'sebagai' => 'required',
            'file_bkp' => 'file|mimes:pdf|max:1024',
            'file_lampiran' => 'file|mimes:pdf|max:2048'
        ]);

        $update = Penataanbelanja_bkp::where([
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'id' => $request->id
        ])->update([
            'nomor' => strip_tags($request->nomor),
            'tanggal' => strip_tags($request->tanggal),
            'jumlah' => strip_tags($request->jumlah),
            'sebagai' => strip_tags($request->sebagai),
            'pph' =>  strip_tags($request->pph),
            'ppn' =>  strip_tags($request->ppn)
        ]);


        if ($request->file_bkp) {
            if ($request->old_1 && strpos($request->old_1, $request->tahun)) {
                Storage::delete($request->old_1);
            }
            $ext = $request->file_bkp->extension();
            $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_belanja";
            $file1 = $request->file('file_bkp')->storeAs($folder, "file_bkp_$request->apbdes_kegiatan_id" . "-" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

            Penataanbelanja_bkp::where([
                'asal_id' => $request->asal_id,
                'tahun' => $request->tahun,
                'id' => $request->id
            ])->update([
                'file_bkp' => $file1
            ]);
        }



        if ($request->file_lampiran) {
            if ($request->old_2 && strpos($request->old_2, $request->tahun)) {
                Storage::delete($request->old_2);
            }
            $ext = $request->file_lampiran->extension();
            $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_belanja";
            $file2 = $request->file('file_lampiran')->storeAs($folder, "file_lampiran_$request->apbdes_kegiatan_id" . "-" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

            Penataanbelanja_bkp::where([
                'asal_id' => $request->asal_id,
                'tahun' => $request->tahun,
                'id' => $request->id
            ])->update([
                'file_lampiran' => $file2
            ]);
        }


        return back()->with('success', 'berhasil update TBPU');
    }


    function cekDokTbpu(Request $request)
    {
        $databkp = Penataanbelanja_bkp::where([
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'apbdes_kegiatan_id' => $request->keg
        ])->get();
        $infos = Admin::with('asal')->where('id', session('loggedAdminDesa'))->first();
        $bkp = Penataanbelanja_bkp::where([
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun
        ])->first();
        //$kegiatan = $databkp->apbdes_kegiatan->kegiatan->kegiatan;
        if (count($databkp)) {
            $kegiatan = $databkp[0]->apbdes_kegiatan->kegiatan->kegiatan;
        } else {
            return "belum ada input BKP";
        }


        $spp = Penataanbelanja_spp::where([
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun
        ])->first();

        $data = [
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'spp' => $spp,
            'bkp' => $bkp,
            'databkp' => $databkp,
            'kegiatan' => $kegiatan,
            'infos' => $infos
        ];


        return view('adminDesa.akunBelanja.cekBKP', $data);
    }

    function cekDokSPP(Request $request)
    {

        $dataspp = Penataanbelanja_spp::where([
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'apbdes_kegiatan_id' => $request->keg
        ])->get();

        $infos = Admin::with('asal')->where('id', session('loggedAdminDesa'))->first();
        $spp = Penataanbelanja_spp::where([
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun
        ])->first();
        //$kegiatan = $dataspp->apbdes_kegiatan->kegiatan->kegiatan;
        if (count($dataspp)) {
            $kegiatan = $dataspp[0]->apbdes_kegiatan->kegiatan->kegiatan;
        } else {
            return "belum ada input spp";
        }


        $data = [
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'spp' => $spp,
            'spp' => $spp,
            'dataspp' => $dataspp,
            'kegiatan' => $kegiatan,
            'infos' => $infos
        ];


        return view('adminDesa.akunBelanja.cekSPP', $data);
    }

    public function updateSPP(Request $request)
    {

        $request->validate([
            'nomor' => 'required|max:50',
            'tanggal' => 'required',
            'jumlah' => 'required',
            'file_spp' => 'file|mimes:pdf|max:1024'
        ]);

        $update = Penataanbelanja_spp::where([
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'id' => $request->id
        ])->update([
            'nomor' => strip_tags($request->nomor),
            'tanggal' => strip_tags($request->tanggal),
            'jumlah' => strip_tags($request->jumlah)

        ]);


        if ($request->file_spp) {
            if ($request->old_1 && strpos($request->old_1, $request->tahun)) {
                Storage::delete($request->old_1);
            }
            $ext = $request->file_spp->extension();
            $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_belanja";
            $file1 = $request->file('file_spp')->storeAs($folder, "file_spp_$request->apbdes_kegiatan_id" . "-" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

            Penataanbelanja_spp::where([
                'asal_id' => $request->asal_id,
                'tahun' => $request->tahun,
                'id' => $request->id
            ])->update([
                'file_spp' => $file1
            ]);
        }



        return back()->with('success', 'berhasil update SPP');
    }

    public function hapusSPP($id)
    {
        Penataanbelanja_spp::destroy($id);
        return back()->with('success', 'data berhasil dihapus');
    }

    public function hapusTBPU($id)
    {
        Penataanbelanja_bkp::destroy($id);
        return back()->with('success', 'data berhasil dihapus');
    }

    public function cariSPP(Request $request)
    {
        $data = [
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'apbdes_kegiatan_id' => $request->apbdes_kegiatan_id,
            'dataspp' => Penataanbelanja_spp::where([
                'asal_id' => $request->asal_id,
                'tahun' => $request->tahun,
                'apbdes_kegiatan_id' => $request->apbdes_kegiatan_id
            ])->get()
        ];
        if (count($data['dataspp']) == 0) {
            return "data kosong";
        } else {
            return view('adminDesa.akunBelanja.cekSPPKeg', $data);
        }
    }
}
