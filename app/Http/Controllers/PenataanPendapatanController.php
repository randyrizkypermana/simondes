<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Penataan_pendapatan;
use App\Models\Apbdes_pendapatan;
use App\Models\Buku_pembantu_bank;
use App\Models\Total_apbd;

use Illuminate\Support\Facades\Storage;


class PenataanPendapatanController extends Controller
{
    public function formPenataanPendapatan(Request $request)
    {
        $tahun = now()->format('Y');
        if ($request->tahun) {
            $tahun = $request->tahun;
        }
        $infos = Admin::with('asal')->where('id', session('loggedAdminDesa'))->first();

        $pengajuan = Penataan_pendapatan::where([
            'asal_id' => $infos->asal_id,
            'tahun' => $tahun,
        ])->first();
        $bukuBank = Buku_pembantu_bank::where([
            'asal_id' => $infos->asal_id,
            'tahun' => $tahun,
        ])->first();


        if (isset($request->jenis)) {
            if ($request->jenis == 'pengajuan' || $request->jenis == 'cek_pengajuan') {
                $datum = Penataan_pendapatan::where([
                    'asal_id' => $infos->asal_id,
                    'tahun' => $tahun
                ])->get();
            } elseif ($request->jenis == 'buku_pembantu_bank') {
                $datum = Buku_pembantu_bank::where([
                    'asal_id' => $infos->asal_id,
                    'tahun' => $tahun
                ])->get();
            }

            $data = [
                'infos' => $infos,
                'jenis' => $request->jenis,
                'tahun' => $tahun,
                'pengajuan' => $pengajuan,
                'bukuBank' => $bukuBank
            ];

            if (count($datum) == 0) {
                if ($request->jenis == 'pengajuan') {

                    $data['pendapatans'] = Apbdes_pendapatan::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun,
                    ])->get();

                    $data['total_p'] = Total_apbd::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun
                    ])->first();
                    if (!$data['total_p']) {
                        return back()->with('dok', 'Silahkan Isi Data APBDes Terlebih Dahulu!');
                    }
                    if ($data['total_p']->belanja_perubahan) {
                        $data['anggaran'] = 'perubahan';
                    } else {
                        $data['anggaran'] = 'murni';
                    }
                }


                return view('adminDesa.akunPendapatan.formAkunPendapatan_t', $data);
            } else {

                if ($request->jenis == 'pengajuan' || $request->jenis == 'cek_pengajuan') {

                    $data['pendapatans'] = Apbdes_pendapatan::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun,
                    ])->get();
                    $data['total_p'] = Total_apbd::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun
                    ])->first();
                    if (!$data['total_p']) {
                        return back()->with('dok', 'Silahkan Isi Data APBDes Terlebih Dahulu!');
                    }
                    if ($data['total_p']->belanja_perubahan) {
                        $data['anggaran'] = 'perubahan';
                    } else {
                        $data['anggaran'] = 'murni';
                    }
                } elseif ($request->jenis == 'buku_pembantu_bank') {
                    $data['bukubanks'] = Buku_pembantu_bank::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun
                    ])->first();
                }
                if ($request->pendapatan) {
                    $jenpend = Apbdes_pendapatan::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun,
                        'id' => $request->pendapatan
                    ])->first();

                    $data['jenpend'] = $jenpend;
                    $data['nampend'] = $jenpend->jenis_pendapatan;
                    $data['pendapatan_id'] = $jenpend->pendapatan_id;
                }

                return view('adminDesa.akunPendapatan.formAkunPendapatan_e', $data);
            }
        } else {
            return view('adminDesa.akunPendapatan.formAkunPendapatan', [
                'tahun' => $tahun,
                'infos' => $infos,
                'pengajuan' => $pengajuan,
                'bukuBank' => $bukuBank

            ]);
        }
    }

    public function tambahPengajuan(Request $request)
    {

        $request->validate([
            'file_data' => 'required|file|mimes:pdf|max:512'
        ]);

        if ($request->pendapatan_id == 5) {
            $nama_data = $request->nama_data;
            $bulan = $request->bulan;
            foreach ($bulan as $bln) {
                $nama_data .= "_" . $bln . "_";
            }
            $nama_data = substr($nama_data, 0, -1);
        } else {
            $nama_data = $request->nama_data;
        }


        $data = [
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'jenis' => $request->jenis,
            'nama_data' => $nama_data,
            'jumlah' => strip_tags($request->jumlah),
            'apbdes_pendapatan_id' => $request->apbdes_pendapatan_id,
            'pendapatan_id' => $request->pendapatan_id,
            'tgl_pengajuan' => strip_tags($request->tgl_pengajuan),
            'tgl_saldo' => strip_tags($request->tgl_saldo),
        ];


        if ($request->file('file_data')) {
            $ext = $request->file_data->extension();
            $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_pendapatan";
            $file1 = $request->file('file_data')->storeAs($folder, "file_data_$request->pendapatan_id" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

            $data['file_data'] = $file1;
        }


        Penataan_pendapatan::create($data);
        return back()->with('success', 'data berhasil dikirim');
    }

    public function updatePengajuan(Request $request)
    {


        $request->validate([
            'file_data' => 'file|mimes:pdf|max:512'
        ]);


        $data = [
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'jenis' => $request->jenis,
            'nama_data' => $request->nama_data,
            'jumlah' => strip_tags($request->jumlah),
            'apbdes_pendapatan_id' => $request->apbdes_pendapatan_id,
            'pendapatan_id' => $request->pendapatan_id,
            'tgl_pengajuan' => strip_tags($request->tgl_pengajuan),
            'tgl_saldo' => strip_tags($request->tgl_saldo),
        ];
        Penataan_pendapatan::where([
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'id' => $request->id,
            'apbdes_pendapatan_id' => $request->apbdes_pendapatan_id
        ])->update([
            'nama_data' => $request->nama_data,
            'jumlah' => strip_tags($request->jumlah),
            'tgl_pengajuan' => strip_tags($request->tgl_pengajuan),
            'tgl_saldo' => strip_tags($request->tgl_saldo),
        ]);

        if ($request->file('file_data')) {
            if (strpos($request->tahun, $request->old_1)) {
                Storage::delete($request->old_1);
            }
            $ext = $request->file_data->extension();
            $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_pendapatan";
            $file1 = $request->file('file_data')->storeAs($folder, "file_data_$request->pendapatan_id" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

            Penataan_pendapatan::where([
                'asal_id' => $request->asal_id,
                'tahun' => $request->tahun,
                'id' => $request->id,
                'apbdes_pendapatan_id' => $request->apbdes_pendapatan_id
            ])->update([
                'file_data' => $file1
            ]);
        }

        return back()->with('success', 'data berhasil diupdate');
    }

    public function deletePengajuan($id)
    {
        $file_data = Penataan_pendapatan::where('id', $id)->pluck('file_data')->first();
        Storage::delete($file_data);

        Penataan_pendapatan::destroy($id);
        return back()->with('success', 'data berhasil dihapus');
    }

    public function tambahBukuBank(Request $request)
    {

        $request->validate([
            'semester_1' => 'file|mimes:pdf|max:1024',
            'semester_2' => 'file|mimes:pdf|max:1024'
        ]);

        $data = [
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun
        ];
        if ($request->semester_1) {
            $ext = $request->semester_1->extension();
            $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_pendapatan";
            $file1 = $request->file('semester_1')->storeAs($folder, "buku_pembantu_bankSemester_1_" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

            $data['semester_1'] = $file1;
            Buku_pembantu_bank::create($data);
        }
        if ($request->semester_2) {
            $ext = $request->semester_2->extension();
            $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_pendapatan";
            $file2 = $request->file('semester_2')->storeAs($folder, "buku_pembantu_bankSemester_2_" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

            $data['semester_2'] = $file2;
            Buku_pembantu_bank::create($data);
        }

        return back()->with('success', 'Data Berhasil dikirim');
    }

    public function updateBukuBank(Request $request)
    {

        $request->validate([
            'semester_1' => 'file|mimes:pdf|max:1024',
            'semester_2' => 'file|mimes:pdf|max:1024'
        ]);

        $data = [
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun
        ];
        if ($request->semester_1) {
            if ($request->old_1) {
                Storage::delete($request->old_1);
            }
            $ext = $request->semester_1->extension();
            $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_pendapatan";
            $file1 = $request->file('semester_1')->storeAs($folder, "buku_pembantu_bankSemester_1_" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

            Buku_pembantu_bank::where([
                'asal_id' => $request->asal_id,
                'tahun' => $request->tahun
            ])->update([
                'semester_1' => $file1
            ]);
        }
        if ($request->semester_2) {
            if ($request->old_2) {
                Storage::delete($request->old_2);
            }
            $ext = $request->semester_2->extension();
            $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_pendapatan";
            $file2 = $request->file('semester_2')->storeAs($folder, "buku_pembantu_bankSemester_2_" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

            Buku_pembantu_bank::where([
                'asal_id' => $request->asal_id,
                'tahun' => $request->tahun
            ])->update([
                'semester_2' => $file2
            ]);
        }

        return back()->with('success', 'Data Berhasil dikirim');
    }
}
