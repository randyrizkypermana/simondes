<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Admin;
use App\Models\Post;
use App\Models\Berita;
use App\Models\Pengumuman;


class EditorController extends Controller
{

    public function index()
    {
        $infos = Admin::where('id', session('loggedEditor'))->first();

        return view('editor.index', [
            'infos' => $infos,

        ]);
    }

    public function logoutEditor(Request $request)
    {
        if (session()->has('loggedEditor')) {
            session()->pull('loggedEditor');
            session()->pull('loggedAdmin');
            $request->session()->invalidate();

            $request->session()->regenerateToken();
            return redirect('/');
        }
    }


    public function berita()
    {
        $infos = Admin::where('id', session('loggedEditor'))->first();
        $berita = Post::select('*')->get();
        return view('editor.berita.index', [
            'infos' => $infos,
            'beritas' => $berita

        ]);
    }


    public function create()
    {
        return view('berita.create',);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'image'     => 'required|image|mimes:png,jpg,jpeg',
            'title'     => 'required',
            'content'   => 'required'
        ]);

        //upload image
        $image = $request->file('image');
        $image->storeAs('public/beritas', $image->hashName());

        $berita = Berita::create([
            'image'     => $image->hashName(),
            'title'     => $request->title,
            'content'   => $request->content
        ]);

        if ($berita) {
            //redirect dengan pesan sukses
            return redirect()->route('berita.index')->with(['success' => 'Data Berhasil Disimpan!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('berita.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }

    /**
     * edit
     *
     * @param  mixed $berita
     * @return void
     */
    public function edit(berita $berita)
    {
        return view('berita.edit', compact('berita'));
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $berita
     * @return void
     */
    public function update(Request $request, berita $berita)
    {
        $this->validate($request, [
            'title'     => 'required',
            'content'   => 'required'
        ]);

        //get data berita by ID
        $berita = Berita::findOrFail($berita->id);

        if ($request->file('image') == "") {

            $berita->update([
                'title'     => $request->title,
                'content'   => $request->content
            ]);
        } else {

            //hapus old image
            Storage::disk('local')->delete('public/beritas/' . $berita->image);

            //upload new image
            $image = $request->file('image');
            $image->storeAs('public/beritas', $image->hashName());

            $berita->update([
                'image'     => $image->hashName(),
                'title'     => $request->title,
                'content'   => $request->content
            ]);
        }

        if ($berita) {
            //redirect dengan pesan sukses
            return redirect()->route('berita.index')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('berita.index')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $berita = Berita::findOrFail($id);
        $berita->delete();

        if ($berita) {
            //redirect dengan pesan sukses
            return redirect()->route('berita.index')->with(['success' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('berita.index')->with(['error' => 'Data Gagal Dihapus!']);
        }
    }
}
