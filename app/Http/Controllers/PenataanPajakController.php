<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Penataanbelanja_spp;
use App\Models\Apbdes_kegiatan;
use App\Models\Total_apbd;
use App\Models\Penataanbelanja_bkp;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Report\Xml\Totals;

class PenataanPajakController extends Controller
{
    public function formPenataanPajak(Request $request)
    {

        $tahun = now()->format('Y');
        if ($request->tahun) {
            $tahun = $request->tahun;
        }
        $infos = Admin::with('asal')->where('id', session('loggedAdminDesa'))->first();


        if ($request->jenis) {
            if ($request->jenis == 'billing') {
                $datum = Penataanbelanja_bkp::where([
                    'asal_id' => $infos->asal_id,
                    'tahun' => $tahun
                ])->where('billing_pph', '!=', Null)->orWhere('billing_ppn', '!=', Null)->get();
            }
            if (count($datum) == 0) {
                $data = [
                    'asal_id' => $infos->asal_id,
                    'tahun' => $tahun,
                    'infos' => $infos,
                    'jenis' => $request->jenis,
                    'databkp' => Penataanbelanja_bkp::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun
                    ])->get(),
                ];

                return view('adminDesa.akunPajak.formPenataanPajak_t', $data);
            } else {
                $data = [
                    'asal_id' => $infos->asal_id,
                    'tahun' => $tahun,
                    'infos' => $infos,
                    'jenis' => $request->jenis,
                    'databkp' => Penataanbelanja_bkp::where([
                        'asal_id' => $infos->asal_id,
                        'tahun' => $tahun
                    ])->get()
                ];
                $apbdes = Total_apbd::where([
                    'asal_id' => $infos->asal_id,
                    'tahun' => $tahun
                ])->first();
                if (!$apbdes) {
                    return "Belum Ada Input APBDes TA $tahun";
                }
                if ($apbdes->akunbelanja_perubahan) {
                    $data['belanja'] = $apbdes->akunbelanja_perubahan;
                } else {
                    $data['belanja'] = $apbdes->akunbelanja_murni;
                }

                $totalbkp = 0;
                $totalpph = 0;
                $totalppn = 0;
                $pphterbayar = 0;
                $ppnterbayar = 0;
                foreach ($data['databkp'] as $bkp) {
                    $nominal = intval(str_replace('.', '', $bkp->jumlah));
                    $totalbkp += $nominal;
                    $pph = intval(str_replace('.', '', $bkp->pph));
                    $ppn = intval(str_replace('.', '', $bkp->ppn));
                    $totalpph += $pph;
                    $totalppn += $ppn;
                    if ($bkp->billing_pph) {
                        $pph_t = intval(str_replace('.', '', $bkp->pph));
                        $pphterbayar += $pph_t;
                    }
                    if ($bkp->billing_ppn) {
                        $ppn_t = intval(str_replace('.', '', $bkp->ppn));
                        $ppnterbayar += $ppn_t;
                    }
                }
                $totalpajak = $totalpph + $totalppn;
                $pajakterbayar = $pphterbayar + $ppnterbayar;

                $data['totalbkp'] = $totalbkp;
                $data['totalpajak'] = $totalpajak;
                $data['pajakterbayar'] = $pajakterbayar;

                return view('adminDesa.akunPajak.formPenataanPajak_e', $data);
            }
        } else {
            $data = [
                'asal_id' => $infos->asal_id,
                'tahun' => $tahun,
                'infos' => $infos
            ];
            return view('adminDesa.akunPajak.formPenataanPajak', $data);
        }
    }

    public function tambahBilling(Request $request)
    {
        if ($request->billing_pph) {
            if ($request->old_pph && strpos($request->old_pph, $request->tahun)) {
                Storage::delete($request->old_pph);
            }
            $ext = $request->billing_pph->extension();
            $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_belanja";
            $file1 = $request->file('billing_pph')->storeAs($folder, "billing_pph_$request->id" . "-" . $request->tahun . "-" . mt_rand(1, 100) . "." . $ext);

            Penataanbelanja_bkp::where([
                'asal_id' => $request->asal_id,
                'tahun' => $request->tahun,
                'id' => $request->id
            ])->update([
                'billing_pph' => $file1
            ]);
        }

        if ($request->billing_ppn) {
            if ($request->old_ppn && strpos($request->old_ppn, $request->tahun)) {
                Storage::delete($request->old_ppn);
            }
            $ext = $request->billing_ppn->extension();
            $folder = "adminDesa/desa_" . $request->asal_id . "/" . $request->tahun . "/penataan_belanja";
            $file2 = $request->file('billing_ppn')->storeAs($folder, "billing_ppn_$request->id" . "-" . $request->tahun . "-" . mt_rand(2, 100) . "." . $ext);

            Penataanbelanja_bkp::where([
                'asal_id' => $request->asal_id,
                'tahun' => $request->tahun,
                'id' => $request->id
            ])->update([
                'billing_ppn' => $file2
            ]);
        }
        if ($request->crud == 'tambah') {
            $kata = "Berhasil Tambah Data";
        } else {
            $kata = "Berhasil Update Data";
        }

        return back()->with('success', $kata);
    }

    public function hapusBilling(Request $request)
    {

        if ($request->billing_pph) {
            Storage::delete($request->billing_pph);
        }
        if ($request->billing_ppn) {
            Storage::delete($request->billing_ppn);
        }

        Penataanbelanja_bkp::where([
            'asal_id' => $request->asal_id,
            'tahun' => $request->tahun,
            'id' => $request->id
        ])->update([
            'billing_pph' => Null,
            'billing_ppn' => Null
        ]);

        return back()->with('success', 'Data Billing dan STS Pajak Berhasil dihapus');
    }
}
