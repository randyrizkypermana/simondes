<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Kecamatan;
use App\Models\Akun_wilayah;
use App\Models\Asal;
use App\Models\Nilai_pemdes;

class AdminSimondesController extends Controller
{

    public function index()
    {

        $adminInfo = Admin::with('asal')->where('id', session('loggedAdmin'))->first();
        return view('adminSimondes.index', [
            'infos' => $adminInfo,
        ]);
    }

    public function akun()
    {
        $adminInfo = Admin::with('asal')->where('id', session('loggedAdmin'))->first();
        $listadmin = Admin::with('asal')->where('role', 'irban')->get();

        return view('adminSimondes.akun.index', [
            'infos' => $adminInfo,
            'listadmin' => $listadmin
        ]);
    }

    public function wilayah()
    {
        $tahun = now()->format('Y');
        $adminInfo = Admin::with('asal')->where('id', session('loggedAdmin'))->first();
        $listadmin = Admin::with('asal')->where('role', 'irban')->get();
        $kecamatan = Asal::where('kecamatan', '!=', '')
            ->orderBy('kecamatan')
            ->distinct()
            ->get('kecamatan');

        $obrik1 = Kecamatan::with('asal')->where('obrik', 'irban1')
            ->orderBy('kecamatan')
            ->distinct()
            ->get();

        $obrik2 = Kecamatan::with('asal')->where('obrik', 'irban2')
            ->orderBy('kecamatan')
            ->distinct()
            ->get();

        $obrik3 = Kecamatan::with('asal')->where('obrik', 'irban3')
            ->orderBy('kecamatan')
            ->distinct()
            ->get();

        $obrik4 = Kecamatan::with('asal')->where('obrik', 'irban4')
            ->orderBy('kecamatan')
            ->distinct()
            ->get();

        return view('adminSimondes.wilayah.index', [
            'infos' => $adminInfo,
            'tahuns' => $tahun,
            'listadmin' => $listadmin,
            'kecamatans' => $kecamatan,
            'obrik1' => $obrik1,
            'obrik2' => $obrik2,
            'obrik3' => $obrik3,
            'obrik4' => $obrik4
        ]);
    }

    public function wilayah_update(Request $request)
    {
        $kecamatan = Nilai_pemdes::findOrFail($request->id);
        $kecamatan->update([
            'obrik' => $request->obrik
        ]);

        return back();
    }

    public function addobrik(Request $request)
    {
        $id_kecamatan = Asal::where([
            'kecamatan' => $request->kecamatan
        ])->get('kecamatan_id')->first();

        $data = [
            'tahun' => $request->tahun,
            'kecamatan' => $request->kecamatan,
            'obrik' => $request->obrik,
            'kecamatan_id' => $id_kecamatan->kecamatan_id
        ];

        Kecamatan::create($data);

        return back();
    }

    public function deleteobrik($id)
    {
        $data = Kecamatan::findOrFail($id);
        $data->delete();
        return back();
    }

    public function pilihtahun(Request $request)
    {
        $adminInfo = Admin::with('asal')->where('id', session('loggedAdmin'))->first();
        if ($request->tahun) {
            $tahun = $request->tahun;
        } else {
            $tahun = Kecamatan::where([
                'tahun' => $request->tahun
            ])->get();
        }
        $kecamatan = Asal::where('kecamatan', '!=', '')
            ->orderBy('kecamatan')
            ->distinct()
            ->get('kecamatan');


        $whereob1 = ['tahun' => $request->tahun, 'obrik' => 'irban1'];
        $obrik1 = Kecamatan::with('asal')->where($whereob1)
            ->orderBy('kecamatan')
            ->distinct()
            ->get();

        $whereob2 = ['tahun' => $request->tahun, 'obrik' => 'irban2'];
        $obrik2 = Kecamatan::with('asal')->where($whereob2)
            ->orderBy('kecamatan')
            ->distinct()
            ->get();

        $whereob3 = ['tahun' => $request->tahun, 'obrik' => 'irban3'];
        $obrik3 = Kecamatan::with('asal')->where($whereob3)
            ->orderBy('kecamatan')
            ->distinct()
            ->get();

        $whereob4 = ['tahun' => $request->tahun, 'obrik' => 'irban4'];
        $obrik4 = Kecamatan::with('asal')->where($whereob4)
            ->orderBy('kecamatan')
            ->distinct()
            ->get();

        return view('adminSimondes.wilayah.index', [
            'tahuns' => $tahun,
            'infos' => $adminInfo,
            'kecamatans' => $kecamatan,
            'obrik1' => $obrik1,
            'obrik2' => $obrik2,
            'obrik3' => $obrik3,
            'obrik4' => $obrik4
        ]);
    }

    public function aspek()
    {
        $adminInfo = Admin::with('asal')->where('id', session('loggedAdmin'))->first();
        $listadmin = Admin::with('asal')->where('role', 'irban')->get();

        return view('adminSimondes.aspek.index', [
            'infos' => $adminInfo,
            'listadmin' => $listadmin
        ]);
    }

    // public function tambahirban(Request $request)
    // {
    //     $data = $request->validate([
    //         'nama' => 'required',
    //         'obrik' => 'required',
    //         'username' => 'required',
    //         'password' => 'required'
    //     ]);
    //     $data = [
    //         'asal_id' => 0,
    //         'nama' => $request->nama,
    //         'username' => $request->username,
    //         'password' => $request->password,
    //         'obrik' => $request->obrik,
    //         'role' => 'irban'
    //     ];

    //     $tambahadmin = Admin::create($data);
    //     if ($tambahadmin) {
    //         return redirect('/admin/akun')->with('tambah', 'Berhasil Tambah Data Akun Irban');
    //     }
    // }


    public function tambahirban(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'obrik' => 'required',
            'username' => 'required',
            'password' => 'required'
        ]);

        $data = [
            'asal_id' => 0,
            'name' => $request->name,
            'username' => $request->username,
            'password' => $request->password,
            'obrik' => $request->obrik,
            'role' => 'irban'
        ];

        $tambahadmin = Admin::create($data);

        if ($tambahadmin) {
            return back()->with('success', 'Data Berhasil Disimpan');
        }
    }

    public function ubahirban(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'obrik' => 'required',
            'username' => 'required',
            'password' => 'required'
        ]);

        $data = [
            'asal_id' => 0,
            'name' => $request->name,
            'username' => $request->username,
            'password' => $request->password,
            'obrik' => $request->obrik,
            'role' => 'irban'
        ];

        $listadmin = Admin::with('asal')->findOrFail($id);

        $listadmin->update($data);

        if ($listadmin) {
            return back()->with('ubah', 'Data Berhasil Diubah');
        }
    }

    public function logoutAdmin(Request $request)
    {
        if (session()->has('loggedAdmin')) {
            session()->pull('loggedIrban');
            session()->pull('loggedAdmin');
            $request->session()->invalidate();

            $request->session()->regenerateToken();
            return redirect('/');
        }
    }

    public function delete($id)
    {
        $admin = Admin::findOrFail($id);
        $admin->delete();
        return back();
    }
}
