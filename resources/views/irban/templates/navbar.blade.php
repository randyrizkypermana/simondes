<div class="inner-wrapper">
	<!-- start: sidebar -->
	<aside id="sidebar-left" class="sidebar-left">

		<div class="sidebar-header">
			<div class="sidebar-title">
				Navigation
			</div>
			<div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
				<i class="fas fa-bars" aria-label="Toggle sidebar"></i>
			</div>
		</div>

		<div class="nano">
			<div class="nano-content">
				<nav id="menu" class="nav-main" role="navigation">

					<ul class="nav nav-main">
						<li>
							<a class="nav-link" href="/irban">
								<i class="bx bx-home-alt" aria-hidden="true"></i>
								<span>Dashboard</span>
							</a>                        
						</li>
						<li class="nav-parent">
							<a class="nav-link" href="#">
								<i class="icons icon-notebook"></i>
								<span>Pemerintahan Desa</span>
							</a>   
							<ul class="nav nav-children">
								<li>
									<a class="nav-link" href="/irban/kewilayahan">
										Kewilayahan
									</a>
								</li>
								<li>
									<a class="nav-link" href="/irban/monografi">
										Monografi
									</a>
								</li>
								<li>
									<a class="nav-link" href="/irban/perencanaan">
										Dokumen Perancanaan
									</a>
								</li>
								<li>
									<a class="nav-link" href="/irban/kelembagaan">
										Kelembagaan
									</a>
								</li>
							</ul>                     
						</li>
						<li class="nav-parent">
							<a class="nav-link" href="#">
								<i class="bx bx-cart-alt" aria-hidden="true"></i>
								<span>eCommerce</span>
							</a>
							<ul class="nav nav-children">
								<li>
									<a class="nav-link" href="/irban">
										Dashboard
									</a>
								</li>
								<li>
									<a class="nav-link" href="ecommerce-products-list.html">
										Products List
									</a>
								</li>
								<li>
									<a class="nav-link" href="ecommerce-products-form.html">
										Products Form
									</a>
								</li>
					</ul>
				</nav>
			</div>
		</div>

	</aside>

	<script>
		// Maintain Scroll Position
		if (typeof localStorage !== 'undefined') {
			if (localStorage.getItem('sidebar-left-position') !== null) {
				var initialPosition = localStorage.getItem('sidebar-left-position'),
					sidebarLeft = document.querySelector('#sidebar-left .nano-content');

				sidebarLeft.scrollTop = initialPosition;
			}
		}
	</script>