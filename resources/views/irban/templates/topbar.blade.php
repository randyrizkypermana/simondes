<?php 
$desa = Session::get('desa');
$kecamatan = Session::get('kecamatan');
$tahun = Session::get('tahun');
$id_desa = Session::get('id');
?>

<header class="header">
    <div class="logo-container">
        <a href="/irban" class="logo">
            <img src="/storage/image/logo.png" width="35" height="35" alt="SIMONDes" />
            <span style="font-weight: bold; vertical-align: middle;" class="text-dark text-5">SIMONDes</span>
        </a>
        
        <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
        
    </div>
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" style="position: absolute; right: 650px;" role="alert">
            <strong>Harap Periksa Pemilihan Desa</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-hidden="true" aria-label="Close"></button>
        </div>
    @endif
    <!-- start: search & user box -->
    <div class="header-right">  
        <span class="separator"></span>
        <ul class="notifications">
            <li>
                @if(Session()->has('desa'))
                    <button type="button" class="mb-1 mt-1 me-1 btn btn-primary" data-bs-toggle="dropdown"><i class="el el-map-marker-alt"></i>  
                        {{ Session::get('desa') }} | {{ Session::get('kecamatan') }} | {{ Session::get('tahun') }} | <i class="el el-chevron-down"></i>
                    </button>
                @else
                    <button type="button" class="mb-1 mt-1 me-1 btn btn-danger" data-bs-toggle="dropdown"><i class="el el-map-marker-alt"></i>  
                        Pilih Desa
                    </button>
                @endif
                </button>
                <div class="dropdown-menu notification-menu">
                    <div class="notification-title">
                        <span class="float-end badge badge-default"></span>
                    </div>
                    
                    <div class="content">
                        <form class="form-horizontal form-bordered" action="/irban/sesi" method="POST">
                            @csrf
                            <ul>
                                <li>
                                    <a href="#" class="clearfix">
                                        <div class="image">
                                            <i class="el el-calendar bg-success text-light"></i>
                                        </div>
                                        <div class="form-group row">
                                            <select class="form-control" name="tahun" id="tahuns" required>
                                                <option value="0">Pilih Tahun</option>
                                                <option value="2022{{ $infos->obrik }}">2022</option>
                                                <option value="2023{{ $infos->obrik }}">2023</option>
                                            </select>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="clearfix">
                                        <div class="image">
                                            <i class="el el-map-marker bg-info text-light"></i>
                                        </div>
                                        <div class="form-group row">
                                            <select class="form-control" name="kecamatan" id="kecamatanss" required>
                                                <option value="0" selected>Pilih Kecamatan</option>
                                                {{-- @foreach ($kecamatans as $kec) { ?>
                                                <option value="{{$kec->kecamatan}}">{{ $kec->kecamatan }}</option>
                                                @endforeach --}}
                                            </select>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="clearfix">
                                        <div class="image">
                                            <i class="el el-map-marker bg-warning text-light"></i>
                                        </div>
                                        <div class="form-group row">
                                            <select class="form-control" name="desa" id="desas" required>                                
                                                <option selected value="0">Pilih Desa</option>
                                            </select>
                                        </div>
                                    </a>
                                </li>
                                
                                <li>
                                    <div class="col" style="text-align: center;">
                                        <button class="btn btn-primary btn-rounded btn-px-4 btn-py-1 font-weight-bold" type="submit">Pilih</button>                                
                                    </div>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </li>
        </ul>

        <span class="separator"></span>
        <ul class="notifications">
            
            <li>
                <a href="#" class="dropdown-toggle notification-icon" data-bs-toggle="dropdown">
                    <i class="bx bx-bell"></i>
                    <span class="badge">3</span>
                </a>
                <div class="dropdown-menu notification-menu">
                    <div class="notification-title">
                        <span class="float-end badge badge-default">3</span>
                        Alerts
                    </div>
                    
                    <div class="content">
                        <ul>
                            <li>
                                <a href="#" class="clearfix">
                                    <div class="image">
                                        <i class="fas fa-thumbs-down bg-danger text-light"></i>
                                    </div>
                                    <span class="title">Server is Down!</span>
                                    <span class="message">Just now</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="clearfix">
                                    <div class="image">
                                        <i class="bx bx-lock bg-warning text-light"></i>
                                    </div>
                                    <span class="title">User Locked</span>
                                    <span class="message">15 minutes ago</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="clearfix">
                                    <div class="image">
                                        <i class="fas fa-signal bg-success text-light"></i>
                                    </div>
                                    <span class="title">Connection Restaured</span>
                                    <span class="message">10/10/2021</span>
                                </a>
                            </li>
                        </ul>

                        <hr />

                        <div class="text-end">
                            <a href="#" class="view-more">View All</a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>

        <span class="separator"></span>

        <div id="userbox" class="userbox">
            <a href="#" data-bs-toggle="dropdown">
                <figure class="profile-picture">
                    <img src="/assets/irban/img/!logged-user.jpg" alt="Joseph Doe" class="rounded-circle" data-lock-picture="/assets/irban/img/!logged-user.jpg" />
                </figure>
                <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
                    <span class="name">{{ $infos->name }}</span>
                    <span class="role">{{ $infos->obrik }}</span>
                </div>

                <i class="fa custom-caret"></i>
            </a>

            <div class="dropdown-menu">
                <ul class="list-unstyled mb-2">
                    <li class="divider"></li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="/"><i class="bx bx-home-circle"></i> Website</a>
                    </li>                    
                    <li>
                        <a role="menuitem" tabindex="-1" href="/logoutIrban"><i class="bx bx-power-off"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end: search & user box -->
    
</header>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>

<script>
jQuery(document).ready(function(){
jQuery('#tahuns').change(function(){
    let tahun=jQuery(this).val();
    jQuery.ajax({
        url:'/irban/getKecamatan',
        type:'post',
        data:'tahun='+tahun+'&_token={{ csrf_token() }}',
        success:function(result){            
            jQuery('#kecamatanss').html(result);
        }
    })
})
});
</script>
<script>
jQuery(document).ready(function(){
jQuery('#kecamatanss').change(function(){
    let kecamatan=jQuery(this).val();
    jQuery.ajax({
        url:'/irban/getDesa',
        type:'post',
        data:'kecamatan='+kecamatan+'&_token={{ csrf_token() }}',
        success:function(result){            
            jQuery('#desas').html(result);
        }
    })
})
});
</script>