@extends('irban.templates.main')

@section('content')
<?php 
use Carbon\Carbon;
$desa = Session::get('desa');
$kecamatan = Session::get('kecamatan');
$tahun = Session::get('tahun');
$id_desa = Session::get('id');
?>
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Dokumen Perencanaan Desa {{ $desa }}</h2>

        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="/irban">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>

                <li><span>Beranda</span></li>
                
                <li>&nbsp;</li>
            </ol>            
        </div>
    </header>

    <div class="row">
        <div class="col">
                <p class="mb-0 text-2 fw-bold">1. Peraturan Desa dan Dokumen RPJMDes : 
                    @forelse ($nilai_rpjmdes as $nilmon)
                        <span class="text-primary">dinilai</span>
                    @empty
                        <span class="text-warning">belum dinilai</span>
                    @endforelse
                </p>
                <p class="mb-0 text-2 fw-bold">2. Berita Acara Musdes / Musdus : 
                    @forelse ($nilai_bacmusdes as $nilmon)
                        <span class="text-primary">dinilai</span>
                    @empty
                        <span class="text-warning">belum dinilai</span>
                    @endforelse
                </p>
                <p class="mb-0 text-2 fw-bold">3. Berita Acara Musrenbangdes : 
                    @forelse ($nilai_bacmusrens as $nilmon)
                        <span class="text-primary">dinilai</span>
                    @empty
                        <span class="text-warning">belum dinilai</span>
                    @endforelse
                </p>
                <p class="mb-0 text-2 fw-bold">4. SK Tim Penyusunan RKPDes : 
                    @forelse ($nilai_sktimrkpdes as $nilmon)
                        <span class="text-primary">dinilai</span>
                    @empty
                        <span class="text-warning">belum dinilai</span>
                    @endforelse
                </p>
        </div>
        <div class="col">
                <p class="mb-0 text-2 fw-bold">5. Peraturan Desa dan Dokumen RKPDes : 
                    @forelse ($nilai_rkpdes as $nilmon)
                        <span class="text-primary">dinilai</span>
                    @empty
                        <span class="text-warning">belum dinilai</span>
                    @endforelse
                </p>
                <p class="mb-0 text-2 fw-bold">6. Ketepatan Waktu Penetapan Perdes RKPDes : 
                    @forelse ($nilai_wakturkpdes as $nilmon)
                        <span class="text-primary">dinilai</span>
                    @empty
                        <span class="text-warning">belum dinilai</span>
                    @endforelse
                </p>	
                <p class="mb-0 text-2 fw-bold">7. Dokumen RAPBDes : 
                    @forelse ($nilai_rapbdes as $nilmon)
                        <span class="text-primary">dinilai</span>
                    @empty
                        <span class="text-warning">belum dinilai</span>
                    @endforelse
                </p>		
                <p class="mb-0 text-2 fw-bold">8. BAC Pembahasan RAPBDes dengan BPD : 
                    @forelse ($nilai_bacrapbdes as $nilmon)
                        <span class="text-primary">dinilai</span>
                    @empty
                        <span class="text-warning">belum dinilai</span>
                    @endforelse
                </p>			
        </div>
        <div class="col">
                <p class="mb-0 text-2 fw-bold">9. Keputusan BPD tentang Persetujuan ABPDes : 
                    @forelse ($nilai_kepbpds as $nilmon)
                        <span class="text-primary">dinilai</span>
                    @empty
                        <span class="text-warning">belum dinilai</span>
                    @endforelse   
                </p>		
                <p class="mb-0 text-2 fw-bold">10. Hasil Evaluasi APBDes oleh Kecamatan : 
                    @forelse ($nilai_evaluasiapbdes as $nilmon)
                        <span class="text-primary">dinilai</span>
                    @empty
                        <span class="text-warning">belum dinilai</span>
                    @endforelse
                </p>
                <p class="mb-0 text-2 fw-bold">11. Dokumen APBDes Print Out Siskeudes : 
                    @forelse ($nilai_printapbdes as $nilmon)
                        <span class="text-primary">dinilai</span>
                    @empty
                        <span class="text-warning">belum dinilai</span>
                    @endforelse
                </p>		
                <p class="mb-0 text-2 fw-bold">12. Desain Gambar dan RAB : 
                    @forelse ($nilai_rabs as $nilmon)
                        <span class="text-primary">dinilai</span>
                    @empty
                        <span class="text-warning">belum dinilai</span>
                    @endforelse
                </p>		
        </div>
    </div>

    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    </div>
                    <h2 class="card-title">Pilih Subindikator</h2>
                </header>
                <div class="card-body">
                    
                    <table class="table table-bordered table-striped mb-0" id="datatable-default">
                        <thead>
                            <tr>
                                <th style="width: 1%">No.</th>
                                <th>Sub Indikator</th>                                
                                <th>Sumber Data</th>
                                <th>Catatan</th>
                                <th>Rekomendasi</th>
                                <th>Status</th>
                                <th>Nilai</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1.</td>
                                <td>Peraturan Desa dan Dokumen RPJMDes</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat_perencanaan/rpjmdes/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_rpjmdes as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_rpjmdes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_rpjmdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Peraturan Desa dan Dokumen RPJMDes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">  
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                                                                        
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_rpjmdes"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_rpjmdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Peraturan Desa dan Dokumen RPJMDes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">   
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                                                                       
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_rpjmdes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_rpjmdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_rpjmdes"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_rpjmdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Dokumen Perencanaan">
                                                            <input name="subindikator" type="hidden" value="RPJMDes">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Berita Acara Musdes / Musdus</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddusia/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_bacmusdes as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_bacmusdes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_bacmusdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator BAC Musdes/Musdus</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word"> 
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                                                                         
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_bacmusdes"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_bacmusdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Berita Acara Musdes / Musdus</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word"> 
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                                                                         
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_bacmusdes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_bacmusdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_bacmusdes"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_bacmusdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Dokumen Perencanaan">
                                                            <input name="subindikator" type="hidden" value="BAC MUSDES">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Berita Acara Musrenbangdes</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddusia/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_bacmusrens as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_bacmusrem"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_bacmusrem" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Berita Acara Musyawarah Pembangunan Desa</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">    
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                                                                      
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_bacmusdes"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_bacmusdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Berita Acara Musyawarah Pembangunan Desa</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word"> 
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                                                                         
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_bacmusren"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_bacmusren" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_bacmusren"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_bacmusren" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Dokumen Perencanaan">
                                                            <input name="subindikator" type="hidden" value="BAC MUSRENBANGDES">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>SK Tim Penyusunan RKPDes</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddusia/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_sktimrkpdes as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_sktim"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_sktim" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator SK Tim Penyusunan RKPDes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word"> 
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                                                                         
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_sktim"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_sktim" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator SK Tim Penyusunan RKPDes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                                                                          
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_sktim"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_sktim" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_sktim"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_sktim" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Dokumen Perencanaan">
                                                            <input name="subindikator" type="hidden" value="SK Tim Penyusunan RKPDes">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Peraturan Desa dan Dokumen RKPDes</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddusia/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_rkpdes as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_rkpdes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_rkpdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Peraturan Desa dan Dokumen RKPDes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">  
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                            
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_rkpdes"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_rkpdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Peraturan Desa dan Dokumen RKPDes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">           
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                                                               
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_rkpdes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_rkpdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_rkpdes"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_rkpdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Dokumen Perencanaan">
                                                            <input name="subindikator" type="hidden" value="Peraturan Desa dan Dokumen RKPDes">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>Ketepatan Waktu Penetapan Peraturan Desa RKPDes</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddusia/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_wakturkpdes as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_wktrkpdes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_wktrkpdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Ketepatan Waktu Penetapan Peraturan Desa RKPDes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">      
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                      
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_wktrkpdes"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_wktrkpdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Ketepatan Waktu Penetapan Peraturan Desa RKPDes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                               
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_wktrkpdes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_wktrkpdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_wktrkpdes"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_wktrkpdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Dokumen Perencanaan">
                                                            <input name="subindikator" type="hidden" value="Ketepatan Waktu Penetapan Peraturan Desa RKPDes">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>Dokumen RAPBDes</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddusia/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_rapbdes as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_rapbdes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_rapbdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Dokumen RAPBDes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">      
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                      
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_rapbdes"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_rapbdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Dokumen RAPBDes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                               
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_rapbdes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_rapbdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_rapbdes"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_rapbdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Dokumen Perencanaan">
                                                            <input name="subindikator" type="hidden" value="Dokumen RAPBDes">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td>BAC Pembahasan RAPBDes dengan BPD</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddusia/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_bacrapbdes as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_bacrapbdes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_bacrapbdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator BAC Pembahasan RAPBDes dengan BPD</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">      
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                      
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_bacrapbdes"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_bacrapbdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator BAC Pembahasan RAPBDes dengan BPD</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                               
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_bacrapbdes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_bacrapbdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_bacrapbdes"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_bacrapbdes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Dokumen Perencanaan">
                                                            <input name="subindikator" type="hidden" value="BAC Pembahasan RAPBDes">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td>Keputusan BPD tentang Persetujuan ABPDes</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddusia/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_kepbpds as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_kepbpd"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_kepbpd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Keputusan BPD tentang Persetujuan ABPDes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">      
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                      
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_kepbpd"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_kepbpd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Keputusan BPD tentang Persetujuan ABPDes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                               
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_kepbpd"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_kepbpd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_kepbpd"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_kepbpd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Dokumen Perencanaan">
                                                            <input name="subindikator" type="hidden" value="Keputusan BPD tentang Persetujuan APBDes">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>10.</td>
                                <td>Hasil Evaluasi APBDes oleh Kecamatan</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddusia/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_evaluasiapbdes as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_evaluasi"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_evaluasi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Hasil Evaluasi APBDes oleh Kecamatan</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">      
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                      
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_evaluasi"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_evaluasi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Hasil Evaluasi APBDes oleh Kecamatan</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                               
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_evaluasi"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_evaluasi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_evaluasi"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_evaluasi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Dokumen Perencanaan">
                                                            <input name="subindikator" type="hidden" value="Hasil Evaluasi APBDes oleh Kecamatan">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>11.</td>
                                <td>Dokumen APBDes Print Out Siskeudes</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddusia/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_printapbdes as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_siskeudes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_siskeudes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Dokumen APBDes Print Out Siskeudes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">      
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                      
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_siskeudes"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_siskeudes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Dokumen APBDes Print Out Siskeudes</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                               
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_siskeudes"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_siskeudes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_siskeudes"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_siskeudes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Dokumen Perencanaan">
                                                            <input name="subindikator" type="hidden" value="Dokumen APBDes Print Out Siskeudes">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>12.</td>
                                <td>Desain Gambar dan RAB</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddusia/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_rabs as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_rab"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_rab" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Desain Gambar dan RAB</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">      
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                      
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_rab"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_rab" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Desain Gambar dan RAB</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $nilmon->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $nilmon->updated_at }}" readonly>
                                                    </div>
                                                </div>                                                                                               
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_rab"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_rab" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_rab"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_rab" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Dokumen Perencanaan">
                                                            <input name="subindikator" type="hidden" value="Desain Gambar dan RAB">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
</section>




@endsection