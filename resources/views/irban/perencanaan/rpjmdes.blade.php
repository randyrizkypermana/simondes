@extends('irban.templates.main')
@section('content')

<?php 
use Carbon\Carbon;
$desa = Session::get('desa');
$kecamatan = Session::get('kecamatan');
$tahun = Session::get('tahun');
$id_desa = Session::get('id');
?>

<section role="main" class="content-body card-margin">
    <header class="page-header">
        <h2>RPJMDes {{ $desa }}</h2>

        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="/irban">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>

                <li><span>Beranda</span></li>
                
                <li>&nbsp;</li>
            </ol>            
        </div>
    </header>

<div class="row">
    <div class="col">
        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>                    
                </div>

                <h2 class="card-title">RPJMDes {{ $desa }}</h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="tabs">
                            <ul class="nav nav-tabs">
                                <li class="nav-item active">
                                    <a class="nav-link" data-bs-target="#rpjmd" href="#rpjmd" data-bs-toggle="tab">Dokumen RPJMDes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-target="#visi" href="#visi" data-bs-toggle="tab">Visi-Misi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-target="#potensi" href="#potensi" data-bs-toggle="tab">Potensi Desa</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="rpjmd" class="tab-pane active">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="30%">
                                                <div class="form-group">
                                                    <label style="font-weight: 650">Peraturan Desa RPJMDes</label>
                                                </div>
                                            </th>
                                            <th>
                                                @forelse ($rpjmdes as $data)
                                                    @if ($data->nama_data == 'nomor_rpjmd')
                                                        <label style="font-weight: 450">Nomor : {{$data->uraian}} </label>
                                                    @endif
                                                @empty
                                                    <label style="font-weight: 450">Nomor : - </label>
                                                @endforelse
                                            </th>
                                        </tr>
                                        <tr>
                                            <th width="30%">
                                                <div class="form-group">
                                                    <label style="font-weight: 650">Tanggal Penetapan Peraturan Desa</label>
                                                </div>
                                            </th>
                                            <th>
                                                @forelse ($rpjmdes as $data)
                                                    @if ($data->nama_data == 'tanggal_penetapan_rpjmd')
                                                        <label style="font-weight: 450">{{$data->uraian}} </label>
                                                    @endif
                                                @empty
                                                    <label style="font-weight: 450"> - </label>
                                                @endforelse
                                            </th>
                                        </tr>
                                        <tr>
                                            <th width="30%">
                                                <div class="form-group">
                                                    <label style="font-weight: 650">Masa Berlaku</label>
                                                </div>
                                            </th>
                                            <th>
                                                @forelse ($rpjmdes as $data)
                                                    @if ($data->nama_data == 'sejak')
                                                        <label style="font-weight: 450">
                                                            {{$data->sejak}} s.d {{ $data->sampai}}
                                                        </label>
                                                    @endif
                                                @empty
                                                    <label style="font-weight: 450"> - </label>
                                                @endforelse
                                            </th>
                                        </tr>
                                        <tr>
                                            <th width="30%">
                                                <div class="form-group">
                                                    <label style="font-weight: 650">File Dokumen RPJMDes</label>
                                                </div>
                                            </th>
                                            <th>
                                                @forelse ($rpjmdes as $data)
                                                    @if ($data->nama_data == 'dokumen_rpjmd')
                                                        <label style="font-weight: 450">
                                                            <a href="{{ asset('storage/'.$data->file_data) }}" target="_blank">
                                                                <img src="/img/logo-pdf.webp" width="50px"><br>
                                                                <small>(klik untuk lihat)</small>
                                                            </a>
                                                        </label>
                                                    @endif
                                                @empty
                                                    <label style="font-weight: 450"> - </label>
                                                @endforelse
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                                <div id="visi" class="tab-pane">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="30%">
                                                <div class="form-group">
                                                    <label style="font-weight: 650">Visi</label>
                                                </div>
                                            </th>
                                            <th>
                                                @forelse ($rpjmdes as $data)
                                                    @if ($data->nama_data == 'visi')
                                                        <label style="font-weight: 450">{{$data->uraian}} </label>
                                                    @endif
                                                @empty
                                                    <label style="font-weight: 450">Nomor : - </label>
                                                @endforelse
                                            </th>
                                        </tr>
                                        <tr>
                                            <th width="30%">
                                                <div class="form-group">
                                                    <label style="font-weight: 650">Misi</label>
                                                </div>
                                            </th>
                                            <th>
                                                @forelse ($misis as $data)
                                                    <label style="font-weight: 450">{{$data->uraian}} </label>
                                                @empty
                                                    <label style="font-weight: 450">Nomor : - </label>
                                                @endforelse
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                                <div id="potensi" class="tab-pane">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </section>
    </div>
</div>
</section>

@endsection