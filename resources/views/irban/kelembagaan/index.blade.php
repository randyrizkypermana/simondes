@extends('irban.templates.main')

@section('content')
<?php 
use Carbon\Carbon;
$desa = Session::get('desa');
$kecamatan = Session::get('kecamatan');
$tahun = Session::get('tahun');
$id_desa = Session::get('id');
?>

<section role="main" class="content-body">
    <header class="page-header">
        <h2>Kelembagaan {{ $desa }}</h2>

        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="/irban">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>

                <li><span>Beranda</span></li>
                
                <li>&nbsp;</li>
            </ol>            
        </div>
    </header>

    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    </div>
                    <h2 class="card-title">Desa {{ $desa }}</h2>
                </header>
                <div class="card-body">
                    
                    <table class="table table-bordered table-striped mb-0" id="datatable-default">
                        <thead>
                            <tr>
                                <th style="width: 1%">No.</th>
                                <th>Sub Indikator</th>                                
                                <th>Sumber Data</th>
                                <th>Catatan</th>
                                <th>Rekomendasi</th>
                                <th>Status</th>
                                <th>Nilai</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1.</td>
                                <td>Perdes SOTK</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihatkelembagaan/sotk/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>
                                @forelse($hassotk as $hsot)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" title="Lihat Catatan" data-bs-toggle="modal" data-bs-target="#ctt_sotk"><i class="el el-edit"></i></a></td>                                                            
                                <div class="modal fade" id="ctt_sotk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Perdes SOTK</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $hsot->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsot->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsot->updated_at }}" readonly>
                                                    </div>
                                                </div>
                                                <textarea class="form-control" rows="10" readonly>{{ $hsot->catatan }}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkn_sotk"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="rkn_sotk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Perdes SOTK</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $hsot->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsot->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsot->updated_at }}" readonly>
                                                    </div>
                                                </div>
                                                <textarea class="form-control" rows="10" readonly>{{ $hsot->rekomendasi }}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                                <td style="text-align: center">
                                    @if($hsot->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $hsot->status }}</span>
                                    @elseif($hsot->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $hsot->status }}</span>
                                    @elseif($hsot->status == 'Valid')
                                        <span class="badge badge-primary">{{ $hsot->status }}</span>
                                    @elseif($hsot->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $hsot->status }}</span>
                                    @endif
                                </td>
                                <td style="text-align: center">@if($hsot->nilai){{ $hsot->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" title="Ubah Validasi & Verifikasi" data-bs-toggle="modal" data-bs-target="#modal-upsotk"><i class="el el-edit"></i></a></td>                                
                                
                                <div class="modal fade" id="modal-upsotk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Ubah Validasi & Verifikasi (Perdes SOTK)</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $hsot->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $hsot->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $hsot->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $hsot->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $hsot->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $hsot->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $hsot->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $hsot->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" title="Tambah Validasi & Verifikasi" data-bs-toggle="modal" data-bs-target="#modal-sotk"><i class="el el-plus"></i></a></td>                                    
                                        <div class="modal fade" id="modal-sotk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi (Perdes SOTK)</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>                                                        
                                                    <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                        @csrf                                                             
                                                        <div class="modal-body">
                                                            <div class="form-group row">
                                                                <div class="col">
                                                                    <label for="pemeriksa">Pemeriksa</label>
                                                                    <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="wilayah">Wilayah</label>
                                                                    <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                                <input name="tahun" type="hidden" value="{{ $tahun }}">                                                                
                                                                <input name="indikator" type="hidden" value="Kewilayahan">
                                                                <input name="subindikator" type="hidden" value="Perdes SOTK">
                                                                <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                                <label>Catatan</label>
                                                                <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                                <label>Rekomendasi</label>
                                                                <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                            </div>                                                                
                                                            <div class="form-group row">
                                                                <div class="col">
                                                                    <label for="nilai">Status</label>
                                                                    <select class="form-control" name="status" id="status" required>
                                                                        <option value="" disabled selected>Pilih Status</option>
                                                                        <option value="Lengkap">Lengkap</option>
                                                                        <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                        <option value="Valid">Valid</option>
                                                                        <option value="Tidak Valid">Tidak Valid</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="nilai">Nilai</label>
                                                                    <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                                </div>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                            <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>                                                                            
                                @endforelse
                            </tr>
                            
                            <tr>
                                <td>2.</td>
                                <td>SK Kades dan Perangkat Desa</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihatkelembagaan/perangkat/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>
                                @forelse($hassotk as $hsot)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" title="Lihat Catatan" data-bs-toggle="modal" data-bs-target="#ctt_sotk"><i class="el el-edit"></i></a></td>                                                            
                                <div class="modal fade" id="ctt_sotk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Perdes SOTK</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $hsot->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsot->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsot->updated_at }}" readonly>
                                                    </div>
                                                </div>
                                                <textarea class="form-control" rows="10" readonly>{{ $hsot->catatan }}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkn_sotk"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="rkn_sotk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Perdes SOTK</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $hsot->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsot->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsot->updated_at }}" readonly>
                                                    </div>
                                                </div>
                                                <textarea class="form-control" rows="10" readonly>{{ $hsot->rekomendasi }}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                                <td style="text-align: center">
                                    @if($hsot->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $hsot->status }}</span>
                                    @elseif($hsot->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $hsot->status }}</span>
                                    @elseif($hsot->status == 'Valid')
                                        <span class="badge badge-primary">{{ $hsot->status }}</span>
                                    @elseif($hsot->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $hsot->status }}</span>
                                    @endif
                                </td>
                                <td style="text-align: center">@if($hsot->nilai){{ $hsot->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" title="Ubah Validasi & Verifikasi" data-bs-toggle="modal" data-bs-target="#modal-upsotk"><i class="el el-edit"></i></a></td>                                
                                
                                <div class="modal fade" id="modal-upsotk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Ubah Validasi & Verifikasi (Perdes SOTK)</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $hsot->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $hsot->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $hsot->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $hsot->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $hsot->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $hsot->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $hsot->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $hsot->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" title="Tambah Validasi & Verifikasi" data-bs-toggle="modal" data-bs-target="#modal-sotk"><i class="el el-plus"></i></a></td>                                    
                                        <div class="modal fade" id="modal-sotk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi (Perdes SOTK)</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>                                                        
                                                    <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                        @csrf                                                             
                                                        <div class="modal-body">
                                                            <div class="form-group row">
                                                                <div class="col">
                                                                    <label for="pemeriksa">Pemeriksa</label>
                                                                    <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="wilayah">Wilayah</label>
                                                                    <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                                <input name="tahun" type="hidden" value="{{ $tahun }}">                                                                
                                                                <input name="indikator" type="hidden" value="Kewilayahan">
                                                                <input name="subindikator" type="hidden" value="Perdes SOTK">
                                                                <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                                <label>Catatan</label>
                                                                <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                                <label>Rekomendasi</label>
                                                                <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                            </div>                                                                
                                                            <div class="form-group row">
                                                                <div class="col">
                                                                    <label for="nilai">Status</label>
                                                                    <select class="form-control" name="status" id="status" required>
                                                                        <option value="" disabled selected>Pilih Status</option>
                                                                        <option value="Lengkap">Lengkap</option>
                                                                        <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                        <option value="Valid">Valid</option>
                                                                        <option value="Tidak Valid">Tidak Valid</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="nilai">Nilai</label>
                                                                    <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                                </div>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                            <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>                                                                            
                                @endforelse
                            </tr>

                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
</section>


@endsection