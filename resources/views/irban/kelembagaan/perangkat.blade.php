@extends('irban.templates.main')
@section('content')
<?php 
use Carbon\Carbon;
$desa = Session::get('desa');
$kecamatan = Session::get('kecamatan');
$tahun = Session::get('tahun');
$id_desa = Session::get('id');
?>

<section role="main" class="content-body card-margin">
    <header class="page-header">
        <h2>Kelembagaan {{ $desa }}</h2>

        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="/irban">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>

                <li><span>Beranda</span></li>                
                <li>&nbsp;</li>
            </ol>            
        </div>
    </header>

<div class="row">
    <div class="col">
        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>                    
                </div>

                <h2 class="card-title">SK Kades dan Perangkat Desa Desa {{ $desa }}</h2>
            </header>
            <div class="card-body">
                <div class="row akunwil">
                    <div class="col-md-12">
                        
                        <div class="tabs">
                            <ul class="nav nav-tabs">
                                <li class="nav-item active">
                                    <a class="nav-link" data-bs-target="#kades" href="#kades" data-bs-toggle="tab"> Kepala Desa</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-target="#sekdes" href="#sekdes" data-bs-toggle="tab"> Sekretaris Desa</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-target="#kaurum" href="#kaurum" data-bs-toggle="tab"> Kaur Umum</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-target="#kaurpr" href="#kaurpr" data-bs-toggle="tab"> Kaur Perencanaan</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-target="#kaurkeu" href="#kaurkeu" data-bs-toggle="tab"> Kaur Keuangan</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-target="#kasipem" href="#kasipem" data-bs-toggle="tab"> Kasi Pemerintahan</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-target="#kasikes" href="#kasikes" data-bs-toggle="tab"> Kasi Kesra</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-target="#kasipel" href="#kasipel" data-bs-toggle="tab"> Kasi Pelayanan</a>
                                </li>
                            </ul>
                            <div class="tab-content">

                                <div id="kades" class="tab-pane active">
                                    <h3 class="font-weight-bold text-center mb-5" style="text-decoration: underline 2px;">Kepala Desa</h3>
                                    
                                    @forelse($kades as $kd)
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kd->jabatan }}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Status Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kd->status_jab }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Nama</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kd->nama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tempat Lahir</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kd->tempat_lahir }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tanggal Lahir</label>            
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kd->tgl_lahir }}</label>
                                                </div>                                               
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jenis Kelamin</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kd->jenkel == 'L' ? 'Laki-laki' : 'Perempuan'}}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Agama</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kd->agama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Telepon</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kd->hp }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">SK Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                           
                                                    <label>: No: {{ $kd->nomor_sk }}</label>
                                                </div>                                       
                                            </div>

                                        </div>

                                        <div class="col-md-6">                                                                        
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Masa Berlaku SK Jabatan</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $kd->sejak }}</label> s.d <label>{{ $kd->sampai }}</label>            
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">SK Jabatan </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$kd->file_sk) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                        
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Pendidikan Terakhir</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $kd->pendidikan }}</label>
                                                </div>                                       
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Ijazah Terakhir </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$kd->file_ijazah) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%" title="Klik untuk lihat file ijazah"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                                      
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Foto {{ $kd->jabatan }}</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <div class="foto_p">
                                                        @if($kd->foto_perangkat)                    
                                                        <img src="{{ asset('storage/'.$kd->foto_perangkat) }}"
                                                            class="gb_perangkat img-fluid" width="150">
                                                        @else
                                                        <p class="nfile">no_image</p>
                                                        <img src="/img/no_image.png" class="gb_perangkat" width="150">
                                                        @endif
                                                    </div>
                                                </div>                    
                                            </div>
                                        </div>

                                    </div>

                                    @empty

                                    <div class="col-md-12 text-center">
                                        <h4 class="text-center mb-5 text-warning">Desa Belum mengisi data Kepala Desa</h4>
                                    </div>
                                        
                                    @endforelse                                                                        

                                </div>
                                
                                <div id="sekdes" class="tab-pane">
                                    <h3 class="font-weight-bold text-center mb-5" style="text-decoration: underline 2px;">Sekretaris Desa</h3>
                                    
                                    @forelse($sekdes as $sd)
                                    
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $sd->jabatan }}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Status Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $sd->status_jab }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Nama</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $sd->nama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tempat Lahir</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $sd->tempat_lahir }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tanggal Lahir</label>            
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $sd->tgl_lahir }}</label>
                                                </div>                                               
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jenis Kelamin</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $sd->jenkel == 'L' ? 'Laki-laki' : 'Perempuan'}}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Agama</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $sd->agama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Telepon</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $sd->hp }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">SK Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                           
                                                    <label>: No: {{ $sd->nomor_sk }}</label>
                                                </div>                                       
                                            </div>

                                        </div>

                                        <div class="col-md-6">                                                                        
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Masa Berlaku SK Jabatan</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $sd->sejak }}</label> s.d <label>{{ $kd->sampai }}</label>            
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">SK Jabatan </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$sd->file_sk) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                        
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Pendidikan Terakhir</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $sd->pendidikan }}</label>
                                                </div>                                       
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Ijazah Terakhir </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$sd->file_ijazah) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%" title="Klik untuk lihat file ijazah"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                                      
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Foto {{ $sd->jabatan }}</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <div class="foto_p">
                                                        @if($sd->foto_perangkat)                    
                                                        <img src="{{ asset('storage/'.$sd->foto_perangkat) }}"
                                                            class="gb_perangkat img-fluid" width="150">
                                                        @else
                                                        <p class="nfile">no_image</p>
                                                        <img src="/img/no_image.png" class="gb_perangkat" width="150">
                                                        @endif
                                                    </div>
                                                </div>                    
                                            </div>
                                        </div>

                                    </div>

                                    @empty
                                    
                                    <div class="col-md-12 text-center">
                                        <h4 class="text-center mb-5 text-warning">Desa Belum mengisi data Sekretaris Desa</h4>
                                    </div>

                                    @endforelse                                

                                </div>

                                <div id="kaurum" class="tab-pane">
                                    <h3 class="font-weight-bold text-center mb-5" style="text-decoration: underline 2px;">Kaur Umum</h3>
                                    
                                    @forelse($kaurum as $ku)
                                    
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $ku->jabatan }}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Status Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $ku->status_jab }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Nama</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $ku->nama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tempat Lahir</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $ku->tempat_lahir }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tanggal Lahir</label>            
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $ku->tgl_lahir }}</label>
                                                </div>                                               
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jenis Kelamin</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $ku->jenkel == 'L' ? 'Laki-laki' : 'Perempuan'}}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Agama</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $ku->agama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Telepon</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $ku->hp }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">SK Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                           
                                                    <label>: No: {{ $ku->nomor_sk }}</label>
                                                </div>                                       
                                            </div>

                                        </div>

                                        <div class="col-md-6">                                                                        
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Masa Berlaku SK Jabatan</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $ku->sejak }}</label> s.d <label>{{ $ku->sampai }}</label>            
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">SK Jabatan </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$ku->file_sk) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                        
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Pendidikan Terakhir</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $ku->pendidikan }}</label>
                                                </div>                                       
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Ijazah Terakhir </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$ku->file_ijazah) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%" title="Klik untuk lihat file ijazah"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                                      
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Foto {{ $ku->jabatan }}</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <div class="foto_p">
                                                        @if($ku->foto_perangkat)                    
                                                        <img src="{{ asset('storage/'.$ku->foto_perangkat) }}"
                                                            class="gb_perangkat img-fluid" width="150">
                                                        @else
                                                        <p class="nfile">no_image</p>
                                                        <img src="/img/no_image.png" class="gb_perangkat" width="150">
                                                        @endif
                                                    </div>
                                                </div>                    
                                            </div>
                                        </div>

                                    </div>

                                    @empty
                                    
                                    <div class="col-md-12 text-center">
                                        <h4 class="text-center mb-5 text-warning">Desa Belum mengisi data Kaur Umum</h4>
                                    </div>

                                    @endforelse                                

                                </div>

                                <div id="kaurpr" class="tab-pane">
                                    <h3 class="font-weight-bold text-center mb-5" style="text-decoration: underline 2px;">Kaur Perencanaan</h3>
                                    
                                    @forelse($kaurpr as $kp)
                                    
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kp->jabatan }}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Status Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kp->status_jab }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Nama</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kp->nama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tempat Lahir</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kp->tempat_lahir }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tanggal Lahir</label>            
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kp->tgl_lahir }}</label>
                                                </div>                                               
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jenis Kelamin</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kp->jenkel == 'L' ? 'Laki-laki' : 'Perempuan'}}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Agama</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kp->agama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Telepon</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kp->hp }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">SK Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                           
                                                    <label>: No: {{ $kp->nomor_sk }}</label>
                                                </div>                                       
                                            </div>

                                        </div>

                                        <div class="col-md-6">                                                                        
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Masa Berlaku SK Jabatan</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $kp->sejak }}</label> s.d <label>{{ $kp->sampai }}</label>            
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">SK Jabatan </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$kp->file_sk) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                        
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Pendidikan Terakhir</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $kp->pendidikan }}</label>
                                                </div>                                       
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Ijazah Terakhir </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$kp->file_ijazah) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%" title="Klik untuk lihat file ijazah"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                                      
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Foto {{ $kp->jabatan }}</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <div class="foto_p">
                                                        @if($kp->foto_perangkat)                    
                                                        <img src="{{ asset('storage/'.$kp->foto_perangkat) }}"
                                                            class="gb_perangkat img-fluid" width="150">
                                                        @else
                                                        <p class="nfile">no_image</p>
                                                        <img src="/img/no_image.png" class="gb_perangkat" width="150">
                                                        @endif
                                                    </div>
                                                </div>                    
                                            </div>
                                        </div>

                                    </div>

                                    @empty
                                    
                                    <div class="col-md-12 text-center">
                                        <h4 class="text-center mb-5 text-warning">Desa Belum mengisi data Kaur Perencanaan</h4>
                                    </div>

                                    @endforelse                                

                                </div>

                                <div id="kaurkeu" class="tab-pane">
                                    <h3 class="font-weight-bold text-center mb-5" style="text-decoration: underline 2px;">Kaur Keuangan</h3>
                                    
                                    @forelse($kaurkeu as $kke)
                                    
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kke->jabatan }}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Status Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kke->status_jab }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Nama</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kke->nama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tempat Lahir</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kke->tempat_lahir }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tanggal Lahir</label>            
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kke->tgl_lahir }}</label>
                                                </div>                                               
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jenis Kelamin</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kke->jenkel == 'L' ? 'Laki-laki' : 'Perempuan'}}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Agama</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kke->agama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Telepon</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kke->hp }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">SK Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                           
                                                    <label>: No: {{ $kke->nomor_sk }}</label>
                                                </div>                                       
                                            </div>

                                        </div>

                                        <div class="col-md-6">                                                                        
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Masa Berlaku SK Jabatan</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $kke->sejak }}</label> s.d <label>{{ $kke->sampai }}</label>            
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">SK Jabatan </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$kke->file_sk) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                        
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Pendidikan Terakhir</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $kke->pendidikan }}</label>
                                                </div>                                       
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Ijazah Terakhir </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$kke->file_ijazah) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%" title="Klik untuk lihat file ijazah"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                                      
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Foto {{ $kke->jabatan }}</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <div class="foto_p">
                                                        @if($kke->foto_perangkat)                    
                                                        <img src="{{ asset('storage/'.$kke->foto_perangkat) }}"
                                                            class="gb_perangkat img-fluid" width="150">
                                                        @else
                                                        <p class="nfile">no_image</p>
                                                        <img src="/img/no_image.png" class="gb_perangkat" width="150">
                                                        @endif
                                                    </div>
                                                </div>                    
                                            </div>
                                        </div>

                                    </div>

                                    @empty
                                    
                                    <div class="col-md-12 text-center">
                                        <h4 class="text-center mb-5 text-warning">Desa Belum mengisi data Kaur Keuangan</h4>
                                    </div>

                                    @endforelse                                

                                </div>

                                <div id="kasipem" class="tab-pane">
                                    <h3 class="font-weight-bold text-center mb-5" style="text-decoration: underline 2px;">Kasi Pemerintahan</h3>
                                    
                                    @forelse($kasipem as $kpm)
                                    
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kpm->jabatan }}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Status Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kpm->status_jab }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Nama</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kpm->nama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tempat Lahir</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kpm->tempat_lahir }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tanggal Lahir</label>            
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kpm->tgl_lahir }}</label>
                                                </div>                                               
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jenis Kelamin</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kpm->jenkel == 'L' ? 'Laki-laki' : 'Perempuan'}}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Agama</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kpm->agama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Telepon</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kpm->hp }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">SK Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                           
                                                    <label>: No: {{ $kpm->nomor_sk }}</label>
                                                </div>                                       
                                            </div>

                                        </div>

                                        <div class="col-md-6">                                                                        
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Masa Berlaku SK Jabatan</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $kke->sejak }}</label> s.d <label>{{ $kpm->sampai }}</label>            
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">SK Jabatan </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$kpm->file_sk) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                        
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Pendidikan Terakhir</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $kpm->pendidikan }}</label>
                                                </div>                                       
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Ijazah Terakhir </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$kpm->file_ijazah) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%" title="Klik untuk lihat file ijazah"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                                      
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Foto {{ $kpm->jabatan }}</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <div class="foto_p">
                                                        @if($kpm->foto_perangkat)                    
                                                        <img src="{{ asset('storage/'.$kpm->foto_perangkat) }}"
                                                            class="gb_perangkat img-fluid" width="150">
                                                        @else
                                                        <p class="nfile">no_image</p>
                                                        <img src="/img/no_image.png" class="gb_perangkat" width="150">
                                                        @endif
                                                    </div>
                                                </div>                    
                                            </div>
                                        </div>

                                    </div>

                                    @empty
                                    
                                    <div class="col-md-12 text-center">
                                        <h4 class="text-center mb-5 text-warning">Desa Belum mengisi data Kasi Pemerintahan</h4>
                                    </div>

                                    @endforelse                                

                                </div>

                                <div id="kasikes" class="tab-pane">
                                    <h3 class="font-weight-bold text-center mb-5" style="text-decoration: underline 2px;">Kasi Kesra</h3>
                                    
                                    @forelse($kasikes as $kks)
                                    
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kks->jabatan }}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Status Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kks->status_jab }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Nama</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kks->nama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tempat Lahir</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kks->tempat_lahir }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tanggal Lahir</label>            
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kks->tgl_lahir }}</label>
                                                </div>                                               
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jenis Kelamin</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kks->jenkel == 'L' ? 'Laki-laki' : 'Perempuan'}}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Agama</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kks->agama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Telepon</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kks->hp }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">SK Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                           
                                                    <label>: No: {{ $kks->nomor_sk }}</label>
                                                </div>                                       
                                            </div>

                                        </div>

                                        <div class="col-md-6">                                                                        
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Masa Berlaku SK Jabatan</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $kks->sejak }}</label> s.d <label>{{ $kks->sampai }}</label>            
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">SK Jabatan </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$kks->file_sk) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                        
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Pendidikan Terakhir</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $kks->pendidikan }}</label>
                                                </div>                                       
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Ijazah Terakhir </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$kks->file_ijazah) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%" title="Klik untuk lihat file ijazah"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                                      
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Foto {{ $kks->jabatan }}</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <div class="foto_p">
                                                        @if($kks->foto_perangkat)                    
                                                        <img src="{{ asset('storage/'.$kks->foto_perangkat) }}"
                                                            class="gb_perangkat img-fluid" width="150">
                                                        @else
                                                        <p class="nfile">no_image</p>
                                                        <img src="/img/no_image.png" class="gb_perangkat" width="150">
                                                        @endif
                                                    </div>
                                                </div>                    
                                            </div>
                                        </div>

                                    </div>

                                    @empty
                                    
                                    <div class="col-md-12 text-center">
                                        <h4 class="text-center mb-5 text-warning">Desa Belum mengisi data Kasi Kesra</h4>
                                    </div>

                                    @endforelse                                

                                </div>

                                <div id="kasipel" class="tab-pane">
                                    <h3 class="font-weight-bold text-center mb-5" style="text-decoration: underline 2px;">Kasi Pelayanan</h3>
                                    
                                    @forelse($kasipel as $kpl)
                                    
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kpl->jabatan }}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Status Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kpl->status_jab }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Nama</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kpl->nama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tempat Lahir</label>
                                                <div class="col-md-9 col-sm-9">                                            
                                                    <label>: {{ $kpl->tempat_lahir }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Tanggal Lahir</label>            
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kpl->tgl_lahir }}</label>
                                                </div>                                               
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Jenis Kelamin</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kpl->jenkel == 'L' ? 'Laki-laki' : 'Perempuan'}}</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Agama</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kpl->agama }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">Telepon</label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label>: {{ $kpl->hp }}</label>
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3 col-sm-3">SK Jabatan</label>
                                                <div class="col-md-9 col-sm-9">                                           
                                                    <label>: No: {{ $kpl->nomor_sk }}</label>
                                                </div>                                       
                                            </div>

                                        </div>

                                        <div class="col-md-6">                                                                        
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Masa Berlaku SK Jabatan</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $kpl->sejak }}</label> s.d <label>{{ $kpl->sampai }}</label>            
                                                </div>                                        
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">SK Jabatan </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$kpl->file_sk) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                        
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Pendidikan Terakhir</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <label>: {{ $kpl->pendidikan }}</label>
                                                </div>                                       
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Ijazah Terakhir </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <a href="{{ asset('storage/'.$kpl->file_ijazah) }}" target="_blank"><img src="/img/logo-pdf.webp" width="100%" title="Klik untuk lihat file ijazah"></a>
                                                    <label class="text-2">SK Jabatan</label>
                                                </div>                                                      
                                            </div>
                    
                                            <div class="form-group row">
                                                <label class="control-label col-md-4 col-sm-4">Foto {{ $kpl->jabatan }}</label>
                                                <div class="col-md-8 col-sm-8">                                            
                                                    <div class="foto_p">
                                                        @if($kpl->foto_perangkat)                    
                                                        <img src="{{ asset('storage/'.$kpl->foto_perangkat) }}"
                                                            class="gb_perangkat img-fluid" width="150">
                                                        @else
                                                        <p class="nfile">no_image</p>
                                                        <img src="/img/no_image.png" class="gb_perangkat" width="150">
                                                        @endif
                                                    </div>
                                                </div>                    
                                            </div>
                                        </div>

                                    </div>

                                    @empty
                                    
                                    <div class="col-md-12 text-center">
                                        <h4 class="text-center mb-5 text-warning">Desa Belum mengisi data Kasi Pelayanan</h4>
                                    </div>

                                    @endforelse                                

                                </div>

                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
</section>

@endsection