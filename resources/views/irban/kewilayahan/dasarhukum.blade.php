@extends('irban.templates.main')
@section('content')
<?php 
use Carbon\Carbon;
$desa = Session::get('desa');
$kecamatan = Session::get('kecamatan');
$tahun = Session::get('tahun');
$id_desa = Session::get('id');
?>

<section role="main" class="content-body card-margin">
    <header class="page-header">
        <h2>Kewilayahan {{ $desa }}</h2>

        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="/irban">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>

                <li><span>Beranda</span></li>
                
                <li>&nbsp;</li>
            </ol>            
        </div>
    </header>

<div class="row">
    <div class="col">
        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>                    
                </div>

                <h2 class="card-title">Dasar Hukum Pembentukan Desa {{ $desa }}</h2>
            </header>
            <div class="card-body">
                <div class="row akunwil">
                    <div class="col-md-10">
                        <table class="table table-bordered">
                            <tr>
                                <th width="50%">
                                    <div class="form-group">
                                        <label for="dasarhukum">Dasar Hukum Pembentukan Desa </label>
                                        <input type="text" class="form-control" id="dasarhukum"
                                            value="{{ $dasarhukum[0]->isidata }}" style="font-size: .85rem" readonly>
                                        <input type="hidden" name="nama_data[]" value="dasar_hukum">
                                    </div>
                                </th>
                                <th width="10%" class="text-center" style="vertical-align: middle">
                                    @if($dataAkun[0]->file_data)
                                    <input type="hidden" name="old_0" value="{{ $dataAkun[0]->file_data }}">
                                    <a href="{{ asset('storage/'.$dataAkun[0]->file_data) }}" target="_blank">
                                        <img src="/img/logo-pdf.webp" width="50px"><br>
                                        <small>(klik untuk lihat)</small>
                                    </a>
                                    @else
                                    <span class="text-danger">(kosong)</span>
                                    @endif
                                </th>
                                
                            </tr>                            
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
</section>

@endsection