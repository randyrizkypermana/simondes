@extends('irban.templates.main')
@section('content')
<?php 
use Carbon\Carbon;
$desa = Session::get('desa');
$kecamatan = Session::get('kecamatan');
$tahun = Session::get('tahun');
$id_desa = Session::get('id');
?>

<section role="main" class="content-body card-margin">
    <header class="page-header">
        <h2>Kewilayahan {{ $desa }}</h2>

        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="/irban">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>

                <li><span>Beranda</span></li>
                
                <li>&nbsp;</li>
            </ol>            
        </div>
    </header>

<div class="row">
    <div class="col">
        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>                    
                </div>

                <h2 class="card-title">Peta Batas Desa {{ $desa }}</h2>
            </header>
            <div class="card-body">
                <div class="row akunwil">
                    <div class="col-md-10">
                        <table class="table table-bordered">
                            @foreach ($peta as $pta)
                            @if ($pta->nama_data == 'luas_wilayah')
                            <tr>
                                <th>
                                    <div class="form-group">
                                        <label>Peta/Sket Desa</label>
                                    </div>
                                </th>                                
                                <th colspan="2" class="text-center" style="vertical-align: middle">
                                    @if($dataAkun[5]->file_data)
                                    <input type="hidden" name="old_5" value="{{ $dataAkun[5]->file_data }}">
                                    <div>Peta/Sket Desa {{ $infos->asal->asal }}</div>
                                    <a href="{{ asset('storage/'.$dataAkun[5]->file_data) }}" target="_blank">
                                        <img src="{{ asset('storage/'.$dataAkun[5]->file_data) }}" alt=""
                                            width="300px"><br>
                                        <small>(klik untuk lihat)</small>

                                    </a>
                                    @else
                                    <span class="text-danger">(kosong)</span>
                                    @endif
                                </th>                                
                            </tr>
                            @endif
                            @endforeach                  
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
</section>

@endsection