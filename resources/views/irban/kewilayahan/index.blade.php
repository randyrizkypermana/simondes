@extends('irban.templates.main')

@section('content')
<?php 
use Carbon\Carbon;
$desa = Session::get('desa');
$kecamatan = Session::get('kecamatan');
$tahun = Session::get('tahun');
$id_desa = Session::get('id');
?>
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Kewilayahan {{ $desa }}</h2>

        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="/irban">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>

                <li><span>Beranda</span></li>
                
                <li>&nbsp;</li>
            </ol>            
        </div>
    </header>

    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    </div>
                    <h2 class="card-title">Desa {{ $desa }}</h2>
                </header>
                <div class="card-body">
                    
                    <table class="table table-bordered table-striped mb-0" id="datatable-default">
                        <thead>
                            <tr>
                                <th style="width: 1%">No.</th>
                                <th>Sub Indikator</th>                                
                                <th>Sumber Data</th>
                                <th>Catatan</th>
                                <th>Rekomendasi</th>
                                <th>Status</th>
                                <th>Nilai</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1.</td>
                                <td>Dasar Hukum Pembentukan Desa</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/dasarhukum/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>
                                @forelse($hasdasarhukum as $hsdsr)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" title="Lihat Catatan" data-bs-toggle="modal" data-bs-target="#ctt_dsr"><i class="el el-edit"></i></a></td>                                                              
                                <div class="modal fade" id="ctt_dsr" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Dasar Hukum Pembentukan Desa</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $hsdsr->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsdsr->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsdsr->updated_at }}" readonly>
                                                    </div>
                                                </div>
                                                <textarea class="form-control" rows="10" readonly>{{ $hsdsr->catatan }}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkn_dsr"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkn_dsr" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Dasar Hukum Pembentukan Desa</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $hsdsr->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsdsr->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsdsr->updated_at }}" readonly>
                                                    </div>
                                                </div>
                                                <textarea class="form-control" rows="10" readonly>{{ $hsdsr->rekomendasi }}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($hsdsr->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $hsdsr->status }}</span>
                                    @elseif($hsdsr->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $hsdsr->status }}</span>
                                    @elseif($hsdsr->status == 'Valid')
                                        <span class="badge badge-primary">{{ $hsdsr->status }}</span>
                                    @elseif($hsdsr->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $hsdsr->status }}</span>
                                    @endif
                                </td>
                                <td style="text-align: center">@if($hsdsr->nilai){{ $hsdsr->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" title="Ubah Validasi & Verifikasi" data-bs-toggle="modal" data-bs-target="#modal-updsr"><i class="el el-edit"></i></a></td>                                
                                
                                <div class="modal fade" id="modal-updsr" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Ubah Validasi & Verifikasi (Dasar Hukum Pembentukan Desa)</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $hsdsr->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $hsdsr->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $hsdsr->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $hsdsr->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $hsdsr->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $hsdsr->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $hsdsr->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $hsdsr->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" title="Tambah Validasi & Verifikasi" data-bs-toggle="modal" data-bs-target="#modal-dsr"><i class="el el-plus"></i></a></td>                                    
                                    <div class="modal fade" id="modal-dsr" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi (Dasar Hukum Pembentukan Desa)</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">                                                                
                                                            <input name="indikator" type="hidden" value="Kewilayahan">
                                                            <input name="subindikator" type="hidden" value="Dasar Hukum Pembentukan Desa">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                                                            
                                @endforelse
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Patok Batas Desa</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/patokbatas/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                            
                                @forelse($haspatokbatas as $hsptk)

                                <td style="text-align: center"><a class="btn btn-sm btn-warning" title="Lihat Catatan" data-bs-toggle="modal" data-bs-target="#ctt_patok"><i class="el el-edit"></i></a></td>                                                            
  
                                <div class="modal fade" id="ctt_patok" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Patok Batas Desa</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $hsptk->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsptk->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsptk->updated_at }}" readonly>
                                                    </div>
                                                </div>
                                                <textarea class="form-control" rows="10" readonly>{{ $hsptk->catatan }}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkn_patok"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkn_patok" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Patok Batas Desa</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $hsptk->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsptk->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hsptk->updated_at }}" readonly>
                                                    </div>
                                                </div>
                                                <textarea class="form-control" rows="10" readonly>{{ $hsptk->rekomendasi }}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <td style="text-align: center">
                                    @if($hsptk->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $hsptk->status }}</span>
                                    @elseif($hsptk->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $hsptk->status }}</span>
                                    @elseif($hsptk->status == 'Valid')
                                        <span class="badge badge-primary">{{ $hsptk->status }}</span>
                                    @elseif($hsptk->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $hsptk->status }}</span>
                                    @endif
                                </td>
                                <td style="text-align: center">@if($hsptk->nilai){{ $hsptk->nilai }}@else Belum Dinilai @endif</td>                                
                                <td style="text-align: center"><a class="btn btn-sm btn-success" title="Ubah Validasi & Verifikasi" data-bs-toggle="modal" data-bs-target="#modal-uppatok"><i class="el el-edit"></i></a></td>                                
                                
                                <div class="modal fade" id="modal-uppatok" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Ubah Validasi & Verifikasi (Patok Batas Desa)</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id_data" type="hidden" value="{{ $hsptk->id_data }}">                                                        
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $hsptk->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $hsptk->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $hsptk->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $hsptk->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $hsptk->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $hsptk->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $hsptk->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                <td style="text-align: center">-</td>
                                <td style="text-align: center">-</td>
                                <td style="text-align: center">-</td>
                                <td style="text-align: center">-</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Tambah Validasi & Verifikasi" data-bs-toggle="modal" data-bs-target="#modal-patok"><i class="el el-plus"></i></a></td>                                         
                                    
                                        <div class="modal fade" id="modal-patok" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <form action="/irban/nilaipemdes" method="post">
                                                        @csrf
                                                        <div class="modal-header">
                                                        <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi (Patok Batas Desa)</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>  
                                                        <div class="modal-body">
                                                            <div class="form-group row">
                                                                <div class="col">
                                                                    <label for="pemeriksa">Pemeriksa</label>
                                                                    <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="wilayah">Wilayah</label>
                                                                    <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                                <input name="tahun" type="hidden" value="{{ $tahun }}">                                                                
                                                                <input name="indikator" type="hidden" value="Kewilayahan">
                                                                <input name="subindikator" type="hidden" value="Patok Batas Desa">
                                                                <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                                <label>Catatan</label>
                                                                <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                                <label>Rekomendasi</label>
                                                                <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                            </div>                                                                
                                                            <div class="form-group row">
                                                                <div class="col">
                                                                    <label for="nilai">Status</label>
                                                                    <select class="form-control" name="status" id="status" required>
                                                                        <option value="" disabled selected>Pilih Status</option>
                                                                        <option value="Lengkap">Lengkap</option>
                                                                        <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                        <option value="Valid">Valid</option>
                                                                        <option value="Tidak Valid">Tidak Valid</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="nilai">Nilai</label>
                                                                    <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                                </div>
                                                            </div>
                                                        
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                            <button type="submit" class="btn btn-primary modal-submit">Kirim Data</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>                                                                    
                                @endforelse
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Peta Batas Desa</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/petabatas/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                
                                @forelse($haspetabatas as $hspta)

                                <td style="text-align: center"><a class="btn btn-sm btn-warning" title="Lihat Catatan" data-bs-toggle="modal" data-bs-target="#ctt_peta"><i class="el el-edit"></i></a></td>                                                            
  
                                <div class="modal fade" id="ctt_peta" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Peta Batas Desa</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $hspta->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hspta->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hspta->updated_at }}" readonly>
                                                    </div>
                                                </div>
                                                <textarea class="form-control" rows="10" readonly>{{ $hspta->catatan }}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkn_peta"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkn_peta" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Peta Batas Desa</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <div class="form-group row pb-3">
                                                    <div class="col">
                                                        <label for="pemeriksa">Pemeriksa</label>
                                                        <input type="text" name="verifikator" class="form-control" value="{{ $hspta->verifikator }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Wilayah</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hspta->obrik }}" readonly>
                                                    </div>
                                                    <div class="col">
                                                        <label for="wilayah">Waktu Penilaian</label>
                                                        <input type="text" name="verval" class="form-control" value="{{ $hspta->updated_at }}" readonly>
                                                    </div>
                                                </div>
                                                <textarea class="form-control" rows="10" readonly>{{ $hspta->rekomendasi }}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($hspta->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $hspta->status }}</span>
                                    @elseif($hspta->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $hspta->status }}</span>
                                    @elseif($hspta->status == 'Valid')
                                        <span class="badge badge-primary">{{ $hspta->status }}</span>
                                    @elseif($hspta->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $hspta->status }}</span>
                                    @endif
                                </td>
                                <td style="text-align: center">@if($hspta->nilai){{ $hspta->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" title="Ubah Validasi & Verifikasi" data-bs-toggle="modal" data-bs-target="#modal-uppeta"><i class="el el-edit"></i></a></td>                                
                                
                                <div class="modal fade" id="modal-uppeta" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Ubah Validasi & Verifikasi (Peta Batas Desa)</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id_data" type="hidden" value="{{ $hspta->id_data }}">                                                        
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $hspta->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $hspta->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $hspta->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $hspta->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $hspta->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $hspta->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $hspta->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                <td style="text-align: center">-</td>
                                <td style="text-align: center">-</td>
                                <td style="text-align: center">-</td>
                                <td style="text-align: center">-</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Tambah Validasi & Verifikasi" data-bs-toggle="modal" data-bs-target="#modal-peta"><i class="el el-plus"></i></a></td>                                    
                                        <div class="modal fade" id="modal-peta" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <form action="/irban/nilaipemdes" method="post">
                                                        @csrf
                                                        <div class="modal-header">
                                                        <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi (Peta Batas Desa)</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>  
                                                        <div class="modal-body">
                                                            <div class="form-group row">
                                                                <div class="col">
                                                                    <label for="pemeriksa">Pemeriksa</label>
                                                                    <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="wilayah">Wilayah</label>
                                                                    <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                                <input name="tahun" type="hidden" value="{{ $tahun }}">                                                                
                                                                <input name="indikator" type="hidden" value="Kewilayahan">
                                                                <input name="subindikator" type="hidden" value="Peta Batas Desa">
                                                                <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                                <label>Catatan</label>
                                                                <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                                <label>Rekomendasi</label>
                                                                <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                            </div>                                                                
                                                            <div class="form-group row">
                                                                <div class="col">
                                                                    <label for="nilai">Status</label>
                                                                    <select class="form-control" name="status" id="status" required>
                                                                        <option value="" disabled selected>Pilih Status</option>
                                                                        <option value="Lengkap">Lengkap</option>
                                                                        <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                        <option value="Valid">Valid</option>
                                                                        <option value="Tidak Valid">Tidak Valid</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="nilai">Nilai</label>
                                                                    <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                                </div>
                                                            </div>                                                        
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                            <button type="submit" class="btn btn-primary modal-submit">Kirim Data</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>                                                                    
                            @endforelse                        
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
</section>




@endsection