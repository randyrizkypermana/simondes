@extends('irban.templates.main')
@section('content')
<?php 
use Carbon\Carbon;
$desa = Session::get('desa');
$kecamatan = Session::get('kecamatan');
$tahun = Session::get('tahun');
$id_desa = Session::get('id');
?>

<section role="main" class="content-body card-margin">
    <header class="page-header">
        <h2>Kewilayahan {{ $desa }}</h2>

        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="/irban">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>

                <li><span>Beranda</span></li>
                
                <li>&nbsp;</li>
            </ol>            
        </div>
    </header>

<div class="row">
    <div class="col">
        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>                    
                </div>

                <h2 class="card-title">Patok Batas Desa {{ $desa }}</h2>
            </header>
            <div class="card-body">
                <div class="row akunwil">
                    <div class="col-md-10">
                        <table class="table table-bordered">
                            @foreach ($patok as $ptk)
                            @if ($ptk->nama_data == 'batas_utara')
                            <tr>
                                <th>
                                    <div class="form-group">
                                        <label for="batas_utara">Batas Utara</label>
                                        <input type="text" class="form-control" id="batas_utara"
                                            value="{{ $ptk->isidata }}" style="font-size: .85rem" readonly>
                                        <input type="hidden" name="nama_data[]" value="patok_batas_utara">
                                    </div>
                                </th>
                                <th width="10%" class="text-center" style="vertical-align: middle">
                                    @if($dataAkun[1]->file_data)
                                    <input type="hidden" name="old_1" value="{{ $dataAkun[1]->file_data }}">
                                    <a href="{{ asset('storage/'.$dataAkun[1]->file_data) }}" target="_blank">
                                        <img src="{{ asset('storage/'.$dataAkun[1]->file_data) }}"
                                            width="50px"><br>
                                        <small>(klik untuk lihat)</small>
                                    </a>
                                    @else
                                    <span class="text-danger">(kosong)</span>
                                    @endif
                                </th>                                
                            </tr>
                            @elseif ($ptk->nama_data == 'batas_selatan')
                            <tr>
                                <th>
                                    <div class="form-group">
                                        <label for="batas_selatan">Batas Selatan</label>
                                        <input type="text" class="form-control" id="batas_selatan"
                                            value="{{ $ptk->isidata }}" style="font-size: .85rem" readonly>
                                        <input type="hidden" name="nama_data[]" value="patok_batas_selatan">
                                    </div>
                                </th>
                                <th width="10%" class="text-center" style="vertical-align: middle">
                                    @if($dataAkun[2]->file_data)
                                    <input type="hidden" name="old_2" value="{{ $dataAkun[2]->file_data }}">
                                    <a href="{{ asset('storage/'.$dataAkun[2]->file_data) }}" target="_blank">
                                        <img src="{{ asset('storage/'.$dataAkun[2]->file_data) }}"
                                            width="50px"><br>
                                        <small>(klik untuk lihat)</small>
                                    </a>
                                    @else
                                    <span class="text-danger">(kosong)</span>
                                    @endif
                                </th>                                
                            </tr>
                            @elseif ($ptk->nama_data == 'batas_barat')
                            <tr>
                                <th>
                                    <div class="form-group">
                                        <label for="batas_barat">Batas Barat</label>
                                        <input type="text" class="form-control" id="batas_barat"
                                            value="{{ $ptk->isidata }}" style="font-size: .85rem" readonly>
                                        <input type="hidden" name="nama_data[]" value="patok_batas_barat">
                                    </div>
                                </th>
                                <th width="10%" class="text-center" style="vertical-align: middle">
                                    @if($dataAkun[3]->file_data)
                                    <input type="hidden" name="old_3" value="{{ $dataAkun[3]->file_data }}">
                                    <a href="{{ asset('storage/'.$dataAkun[3]->file_data) }}" target="_blank">
                                        <img src="{{ asset('storage/'.$dataAkun[3]->file_data) }}"
                                            width="50px"><br>
                                        <small>(klik untuk lihat)</small>
                                    </a>
                                    @else
                                    <span class="text-danger">(kosong)</span>
                                    @endif
                                </th>                               
                            </tr>
                            @elseif ($ptk->nama_data == 'batas_timur')
                            <tr>
                                <th>
                                    <div class="form-group">
                                        <label for="batas_timur">Batas Timur</label>
                                        <input type="text" class="form-control" id="batas_timur"
                                            value="{{ $ptk->isidata }}" style="font-size: .85rem" readonly>
                                        <input type="hidden" name="nama_data[]" value="patok_batas_timur">
                                    </div>
                                </th>
                                <th width="10%" class="text-center" style="vertical-align: middle">
                                    @if($dataAkun[4]->file_data)
                                    <input type="hidden" name="old_4" value="{{ $dataAkun[4]->file_data }}">
                                    <a href="{{ asset('storage/'.$dataAkun[4]->file_data) }}" target="_blank">
                                        <img src="{{ asset('storage/'.$dataAkun[4]->file_data) }}"
                                            width="50px"><br>
                                        <small>(klik untuk lihat)</small>
                                    </a>
                                    @else
                                    <span class="text-danger">(kosong)</span>
                                    @endif
                                </th>                                
                            </tr>
                            @endif
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
</section>

@endsection