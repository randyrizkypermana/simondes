@extends('irban.templates.main')

@section('content')
<?php 
use Carbon\Carbon;
$desa = Session::get('desa');
$kecamatan = Session::get('kecamatan');
$tahun = Session::get('tahun');
$id_desa = Session::get('id');
?>
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Monografi {{ $desa }}</h2>

        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="/irban">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>

                <li><span>Beranda</span></li>
                
                <li>&nbsp;</li>
            </ol>            
        </div>
    </header>

    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    </div>
                    <h2 class="card-title">Desa {{ $desa }}</h2>
                </header>
                <div class="card-body">
                    
                    <table class="table table-bordered table-striped mb-0" id="datatable-default">
                        <thead>
                            <tr>
                                <th style="width: 1%">No.</th>
                                <th>Sub Indikator</th>                                
                                <th>Sumber Data</th>
                                <th>Catatan</th>
                                <th>Rekomendasi</th>
                                <th>Status</th>
                                <th>Nilai</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1.</td>
                                <td>Jumlah Penduduk Per Kelompok Usia</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddusia/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_pddusias as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_pddusia"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_pddusia" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Jumlah Per Kelompok Usia</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_pddusia"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_pddusia" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Jumlah Penduduk Perkelompok Usia</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_pddusia"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_pddusia" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_pddusia"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_pddusia" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Monografi">
                                                            <input name="subindikator" type="hidden" value="Jumlah Penduduk Per Kelompok Usia">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Jumlah Penduduk Miskin</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pddmiskin/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_pddmiskins as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_pddmiskin"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_pddmiskin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Jumlah Penduduk Miskin</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_pddmiskin"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_pddmiskin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Jumlah Penduduk Miskin</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_pddmiskin"><i class="el el-edit"></i></a></td>                                
                                
                                <div class="modal fade" id="update_pddmiskin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_pddmiskin"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_pddmiskin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Monografi">
                                                            <input name="subindikator" type="hidden" value="Jumlah Penduduk Miskin">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                                
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Jumlah Penduduk Berdasarkan Pekerjaan</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="#" ><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_pddpekerjaans as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_pddpekerjaan"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_pddpekerjaan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Jumlah Penduduk Berdasarkan Pekerjaan</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_pddpekerjaan"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_pddpekerjaan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Jumlah Penduduk Berdasarkan Pekerjaan</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_pddpekerjaan"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_pddpekerjaan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_pddpekerjaan"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_pddpekerjaan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Monografi">
                                                            <input name="subindikator" type="hidden" value="Jumlah Penduduk Berdasarkan Pekerjaan">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Jumlah Sarana Ibadah</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/ibadah/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_saribadahs as $nilmon)
                                <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_saribadah"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="ctt_saribadah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Jumlah Sarana Ibadah</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            

                                <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_saribadah"><i class="el el-edit"></i></a></td>

                                <div class="modal fade" id="rkm_saribadah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">                                                                                                
                                                <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Jumlah Sarana Ibadah</h4>                                                
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <td style="text-align: center">
                                    @if($nilmon->status == 'Lengkap')
                                        <span class="badge badge-success">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Lengkap')
                                        <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Valid')
                                        <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                    @elseif($nilmon->status == 'Tidak Valid')
                                        <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                    @endif
                                </td>
                                <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_saribadah"><i class="el el-edit"></i></a></td>
                                <div class="modal fade" id="update_saribadah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>                                                        
                                            <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                @csrf                                                             
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="pemeriksa">Pemeriksa</label>
                                                            <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                        </div>
                                                        <div class="col">
                                                            <label for="wilayah">Wilayah</label>
                                                            <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                        <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                        <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                        <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                        <label>Catatan</label>
                                                        <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                        <label>Rekomendasi</label>
                                                        <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                    </div>                                                                
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label for="nilai">Status</label>
                                                            <select class="form-control" name="status" id="status" required>
                                                                <option value="" disabled selected>Pilih Status</option>
                                                                <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <label for="nilai">Nilai</label>
                                                            <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_saribadah"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_saribadah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Monografi">
                                                            <input name="subindikator" type="hidden" value="Jumlah Sarana Ibadah">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                                </tr>
                                <tr>
                                    <td>5.</td>
                                    <td>Jumlah Sarana Pendidikan</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/pendidikan/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                    @forelse ($nilai_sarpendidikans as $nilmon)
                                    <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_sarpendidikan"><i class="el el-edit"></i></a></td>
                                    <div class="modal fade" id="ctt_sarpendidikan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                            <div class="modal-content">
                                                <div class="modal-header text-center">                                                                                                
                                                    <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Jumlah Sarana Pendidikan</h4>                                                
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                    <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-end">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                            

                                    <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_sarpendidikan"><i class="el el-edit"></i></a></td>

                                    <div class="modal fade" id="rkm_sarpendidikan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                            <div class="modal-content">
                                                <div class="modal-header text-center">                                                                                                
                                                    <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Jumlah Sarana Pendidikan</h4>                                                
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                    <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-end">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <td style="text-align: center">
                                        @if($nilmon->status == 'Lengkap')
                                            <span class="badge badge-success">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Tidak Lengkap')
                                            <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Valid')
                                            <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Tidak Valid')
                                            <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                        @endif
                                    </td>
                                    <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_sarpendidikan"><i class="el el-edit"></i></a></td>
                                    <div class="modal fade" id="update_sarpendidikan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                    <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                    <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                    <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_sarpendidikan"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_sarpendidikan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Monografi">
                                                            <input name="subindikator" type="hidden" value="Jumlah Sarana Pendidikan">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>Jumlah Sarana Kesehatan</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="/lihat/kesehatan/{{ $id_desa }}/{{ $tahun }}"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_sarkesehatans as $nilmon)
                                    <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_sarkesehatan"><i class="el el-edit"></i></a></td>
                                    <div class="modal fade" id="ctt_sarkesehatan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                            <div class="modal-content">
                                                <div class="modal-header text-center">                                                                                                
                                                    <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Jumlah Sarana Kesehatan</h4>                                                
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                    <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-end">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                            

                                    <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_sarkesehatan"><i class="el el-edit"></i></a></td>

                                    <div class="modal fade" id="rkm_sarkesehatan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                            <div class="modal-content">
                                                <div class="modal-header text-center">                                                                                                
                                                    <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Jumlah Sarana Kesehatan</h4>                                                
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                    <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-end">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <td style="text-align: center">
                                        @if($nilmon->status == 'Lengkap')
                                            <span class="badge badge-success">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Tidak Lengkap')
                                            <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Valid')
                                            <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Tidak Valid')
                                            <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                        @endif
                                    </td>
                                    <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_sarkesehatan"><i class="el el-edit"></i></a></td>
                                    <div class="modal fade" id="update_sarkesehatan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                    <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                    <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                    <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_sarkesehatan"><i class="el el-plus"></i></a></td>
                                        <div class="modal fade" id="nilai_sarkesehatan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>                                                        
                                                    <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                        @csrf                                                             
                                                        <div class="modal-body">
                                                            <div class="form-group row">
                                                                <div class="col">
                                                                    <label for="pemeriksa">Pemeriksa</label>
                                                                    <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="wilayah">Wilayah</label>
                                                                    <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                                <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                                <input name="indikator" type="hidden" value="Monografi">
                                                                <input name="subindikator" type="hidden" value="Jumlah Sarana Kesehatan">
                                                                <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                                <label>Catatan</label>
                                                                <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                                <label>Rekomendasi</label>
                                                                <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                            </div>                                                                
                                                            <div class="form-group row">
                                                                <div class="col">
                                                                    <label for="nilai">Status</label>
                                                                    <select class="form-control" name="status" id="status" required>
                                                                        <option value="" disabled selected>Pilih Status</option>
                                                                        <option value="Lengkap">Lengkap</option>
                                                                        <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                        <option value="Valid">Valid</option>
                                                                        <option value="Tidak Valid">Tidak Valid</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="nilai">Nilai</label>
                                                                    <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                                </div>
                                                            </div>                                                            
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                            <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>Jumlah UMKM</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="#"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_jumumkms as $nilmon)
                                    <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_umkm"><i class="el el-edit"></i></a></td>
                                    <div class="modal fade" id="ctt_umkm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                            <div class="modal-content">
                                                <div class="modal-header text-center">                                                                                                
                                                    <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Jumlah UMKM</h4>                                                
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                    <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-end">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                            

                                    <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_umkm"><i class="el el-edit"></i></a></td>

                                    <div class="modal fade" id="rkm_umkm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                            <div class="modal-content">
                                                <div class="modal-header text-center">                                                                                                
                                                    <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Jumlah UMKM</h4>                                                
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                    <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-end">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <td style="text-align: center">
                                        @if($nilmon->status == 'Lengkap')
                                            <span class="badge badge-success">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Tidak Lengkap')
                                            <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Valid')
                                            <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Tidak Valid')
                                            <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                        @endif
                                    </td>
                                    <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_umkm"><i class="el el-edit"></i></a></td>
                                    <div class="modal fade" id="update_umkm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                    <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                    <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                    <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_umkm"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_umkm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Monografi">
                                                            <input name="subindikator" type="hidden" value="Jumlah UMKM">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td>Panjang Jalan Desa</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="#"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_pjgjalans as $nilmon)
                                    <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_jalan"><i class="el el-edit"></i></a></td>
                                    <div class="modal fade" id="ctt_jalan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                            <div class="modal-content">
                                                <div class="modal-header text-center">                                                                                                
                                                    <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Panjang Jalan Desa</h4>                                                
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                    <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-end">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                            

                                    <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_jalan"><i class="el el-edit"></i></a></td>

                                    <div class="modal fade" id="rkm_jalan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                            <div class="modal-content">
                                                <div class="modal-header text-center">                                                                                                
                                                    <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Panjang Jalan Desa</h4>                                                
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                    <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-end">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <td style="text-align: center">
                                        @if($nilmon->status == 'Lengkap')
                                            <span class="badge badge-success">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Tidak Lengkap')
                                            <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Valid')
                                            <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Tidak Valid')
                                            <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                        @endif
                                    </td>
                                    <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_jalan"><i class="el el-edit"></i></a></td>
                                    <div class="modal fade" id="update_jalan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                    <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                    <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                    <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_jalan"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_jalan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Monografi">
                                                            <input name="subindikator" type="hidden" value="Panjang Jalan Desa">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td>Jumlah Jembatan</td>
                                <td style="text-align: center"><a class="btn btn-sm btn-primary" target="_blank" rel="noopener"  href="#"><i class="el el-search"></i> Lihat Data</a></td>                                                                                               
                                @forelse ($nilai_jmljembatans as $nilmon)
                                    <td style="text-align: center"><a class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ctt_jembatan"><i class="el el-edit"></i></a></td>
                                    <div class="modal fade" id="ctt_jembatan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                            <div class="modal-content">
                                                <div class="modal-header text-center">                                                                                                
                                                    <h4 class="font-weight-bold text-dark"><i class="el el-info-circle"></i> Catatan Subindikator Jumlah Jembatan</h4>                                                
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                    <textarea class="form-control" rows="15" readonly>{{ $nilmon->catatan }}</textarea>                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-end">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                            

                                    <td style="text-align: center"><a class="btn btn-sm btn-info" title="Lihat Rekomendasi" data-bs-toggle="modal" data-bs-target="#rkm_jembatan"><i class="el el-edit"></i></a></td>

                                    <div class="modal fade" id="rkm_jembatan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-lg modal-dialog-scrollable">
                                            <div class="modal-content">
                                                <div class="modal-header text-center">                                                                                                
                                                    <h4 class="font-weight-bold text-dark"><i class="el el-check"></i> Rekomendasi Subindikator Jumlah Jembatan</h4>                                                
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body" style="text-align: justify; text-justify: inter-word">                                                                                            
                                                    <textarea class="form-control" rows="15" readonly>{{ $nilmon->rekomendasi }}</textarea>                                            
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-end">
                                                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OKE</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <td style="text-align: center">
                                        @if($nilmon->status == 'Lengkap')
                                            <span class="badge badge-success">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Tidak Lengkap')
                                            <span class="badge badge-warning">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Valid')
                                            <span class="badge badge-primary">{{ $nilmon->status }}</span>
                                        @elseif($nilmon->status == 'Tidak Valid')
                                            <span class="badge badge-danger">{{ $nilmon->status }}</span>
                                        @endif
                                    </td>
                                    <td>@if($nilmon->nilai){{ $nilmon->nilai }}@else Belum Dinilai @endif</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#update_jembatan"><i class="el el-edit"></i></a></td>
                                    <div class="modal fade" id="update_jembatan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Update Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/updatenilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="id" type="hidden" value="{{ $nilmon->id }}">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control">{{ $nilmon->catatan }}</textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control">{{ $nilmon->rekomendasi }}</textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap" {{ $nilmon->status == 'Lengkap' ? 'selected' : '' }}>Lengkap</option>
                                                                    <option value="Tidak Lengkap" {{ $nilmon->status == 'Tidak Lengkap' ? 'selected' : '' }}>Tidak Lengkap</option>
                                                                    <option value="Valid" {{ $nilmon->status == 'Valid' ? 'selected' : '' }}>Valid</option>
                                                                    <option value="Tidak Valid" {{ $nilmon->status == 'Tidak Valid' ? 'selected' : '' }}>Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" value="{{ $nilmon->nilai }}" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                @empty
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center">-</td>
                                    <td style="text-align: center"><a class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#nilai_jembatan"><i class="el el-plus"></i></a></td>
                                    <div class="modal fade" id="nilai_jembatan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel">Validasi & Verifikasi</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>                                                        
                                                <form action="/irban/nilaipemdes" method="POST" class="form-horizontal mb-lg">
                                                    @csrf                                                             
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="pemeriksa">Pemeriksa</label>
                                                                <input type="text" name="verifikator" class="form-control" value="{{ $infos->name }}" readonly>
                                                            </div>
                                                            <div class="col">
                                                                <label for="wilayah">Wilayah</label>
                                                                <input type="text" name="verval" class="form-control" value="{{ $infos->obrik }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input name="asal_id" type="hidden" value="{{ $id_desa }}">
                                                            <input name="tahun" type="hidden" value="{{ $tahun }}">
                                                            <input name="indikator" type="hidden" value="Monografi">
                                                            <input name="subindikator" type="hidden" value="Jumlah Jembatan">
                                                            <input name="waktu_penilaian" type="hidden" value="{{ Carbon::now()->format('d-m-Y H:i:s') }}">
                                                            <label>Catatan</label>
                                                            <textarea rows="4" type="text" name="catatan" class="form-control"></textarea>
                                                            <label>Rekomendasi</label>
                                                            <textarea rows="4" type="text" name="rekomendasi" class="form-control"></textarea>
                                                        </div>                                                                
                                                        <div class="form-group row">
                                                            <div class="col">
                                                                <label for="nilai">Status</label>
                                                                <select class="form-control" name="status" id="status" required>
                                                                    <option value="" disabled selected>Pilih Status</option>
                                                                    <option value="Lengkap">Lengkap</option>
                                                                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                                                                    <option value="Valid">Valid</option>
                                                                    <option value="Tidak Valid">Tidak Valid</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="nilai">Nilai</label>
                                                                <input type="number" name="nilai" class="form-control" placeholder="0 - 100">
                                                            </div>
                                                        </div>                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Kirim Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                                        
                                @endforelse
                            </tr>

                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
</section>




@endsection