@extends('irban.templates.main')
@section('content')

<?php 
use Carbon\Carbon;
$desa = Session::get('desa');
$kecamatan = Session::get('kecamatan');
$tahun = Session::get('tahun');
$id_desa = Session::get('id');
?>

<section role="main" class="content-body card-margin">
    <header class="page-header">
        <h2>Monografi Desa {{ $desa }}</h2>

        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="/irban">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>

                <li><span>Beranda</span></li>
                
                <li>&nbsp;</li>
            </ol>            
        </div>
    </header>

<div class="row">
    <div class="col">
        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>                    
                </div>

                <h2 class="card-title">Jumlah Penduduk Per Kelompok Usia Desa {{ $desa }}</h2>
            </header>
            <div class="card-body">
                <div class="row akunwil">
                    <div class="col-md-10">
                        <table class="table table-bordered">
                            @foreach ($damon as $mon)
                            <tr>
                                <th width="50%">
                                    <div class="form-group">
                                        @if ($mon->nama_data == 'usia_0_15')
                                            <label for="dasarhukum">Usia 0 - 15 Tahun</label>   
                                        @elseif ($mon->nama_data == 'usia_15_65')
                                            <label for="dasarhukum">Usia 15 - 65 Tahun</label>
                                        @else
                                            <label for="dasarhukum">Usia 65 Keatas</label>
                                        @endif
                                    </div>
                                </th>
                                <th>
                                    @if ($mon->nama_data == 'usia_0_15')
                                        <label style="font-weight: 450">{{ $mon->isidata }} Orang</label>   
                                    @elseif ($mon->nama_data == 'usia_15_65')
                                        <label style="font-weight: 450">{{ $mon->isidata }} Orang</label>
                                    @else
                                        <label style="font-weight: 450">{{ $mon->isidata }} Orang</label>
                                    @endif
                                </th>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
</section>

@endsection