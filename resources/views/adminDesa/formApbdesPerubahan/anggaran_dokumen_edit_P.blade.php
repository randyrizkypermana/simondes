<div class="mt-2">
    <p class="text-info">Form Update Data (Dokumen) APBDes TA {{ $tahun }} Perubahan</p>
</div>
<div class="row">
    <div class="col-md-8">
        <form action="/adminDesa/tambahDokumenP" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
            <input type="hidden" name="tahun" value="{{ $tahun }}">

            <table class="table table-bordered">
                <thead>
                    <tr style="background-color: rgb(183, 197, 197)">
                        <th width="5%" style="vertical-align: middle">#</th>
                        <th style="vertical-align: middle">
                            <h4>Jenis Data</h4>
                        </th>
                        <th width="40%" class="text-center">
                            <h4>Data</h4>
                        </th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th style="vertical-align: middle">1.</th>
                        <th style="vertical-align: middle">Nomor dan Tahun Perdes tentang APBDes TA {{ $tahun }}
                            Perubahan</th>
                        <th width="50%">
                            @error('nomor_perdes_perubahan')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                            <input type="text" name="nomor_perdes_perubahan" class="form-control"
                                value="{{ old('nomor_perdes_perubahan', $apbdes_doks[0]->nomor_perdes_perubahan) }}"
                                autofocus style="font-size: .85rem">
                        </th>
                    </tr>
                    <tr>
                        <th style="vertical-align: middle">2.</th>
                        <th style="vertical-align: middle">Tanggal Penetapan Perdes APBDes TA {{ $tahun }} Perubahan
                        </th>
                        <th>
                            @error('tanggal_perubahan')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                            <input type="text" name="tanggal_perubahan" class="form-control"
                                data-inputmask="'mask': '99/99/9999'"
                                value="{{ old('tanggal_perubahan', $apbdes_doks[0]->tanggal_perubahan) }}"
                                style="font-size: .85rem">
                        </th>
                    </tr>
                    <tr>
                        <th style="vertical-align: middle">3.</th>
                        <th style="vertical-align: middle">Dokumen APBDes TA {{ $tahun }} Perubahan</th>
                        <th class="d-flex">
                            <div class="pr-2">
                                @if($apbdes_doks[0]->dokumen_perubahan)
                                <a href="{{ asset('storage/'.$apbdes_doks[0]->dokumen_perubahan) }}" target="_blank">
                                    <img src="/img/logo-pdf.webp" width="100px">
                                    <input type="hidden" name="old_1" value="{{ $apbdes_doks[0]->dokumen_perubahan }}">
                                </a>
                                @else
                                <div class="text-danger">(data kosong)</div>
                                @endif
                            </div>
                            @error('dokumen_perubahan')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="dokumen_perubahan" class="custom-file-input"
                                        id="dokumen_perubahan">
                                    <label class="custom-file-label text-muted dokumen_perubahan"
                                        for="dokumen_perubahan">Ganti
                                        file PDF
                                        (max-size: 20MB)</label>
                                </div>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th style="vertical-align: middle">4.</th>
                        <th style="vertical-align: middle">Dokumen Penjabaran APBDes TA {{ $tahun }} Perubahan</th>
                        <th class="d-flex">
                            <div class="pr-2">
                                @if($apbdes_doks[0]->dokumen_penjabaran_perubahan)
                                <a href="{{ asset('storage/'.$apbdes_doks[0]->dokumen_penjabaran_perubahan) }}"
                                    target="_blank">
                                    <img src="/img/logo-pdf.webp" width="100px">
                                    <input type="hidden" name="old_2"
                                        value="{{ $apbdes_doks[0]->dokumen_penjabaran_perubahan }}">
                                </a>
                                @else
                                <div class="text-danger">(data kosong)</div>
                                @endif
                            </div>
                            @error('dokumen_penjabaran_perubahan')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="dokumen_penjabaran_perubahan" class="custom-file-input"
                                        id="dokumen_penjabaran_perubahan">
                                    <label class="custom-file-label text-muted dokumen_penjabaran_perubahan"
                                        for="dokumen_penjabaran_perubahan">Ganti
                                        file PDF
                                        (max-size: 20MB)</label>
                                </div>
                            </div>
                        </th>
                    </tr>

                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="2" class="text-info">Catatan : Dokumen APBDes Perubahan yg diupload adalah print
                            out
                            Siskeudes</th>
                        <th class="text-center"><button class="btn btn-primary" type="submit">KIRIM
                                DATA</button></th>
                    </tr>
                </tfoot>

            </table>
        </form>
    </div>
</div>
@if(session()->has('success'))
<script>
    Swal.fire({
  position: 'center',
  icon: 'success',
  title: 'Data Berhasil di Update',
  showConfirmButton: false,
  timer: 1500
})
</script>

@endif

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>

<script>
    $('#dokumen_penjabaran_perubahan').on('change', function(){
        getURL(this);
    })

    function getURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var filename = $("#dokumen_penjabaran_perubahan").val();
            
            filename = filename.substring(filename.lastIndexOf('\\') + 1);
            var cekgb = filename.substring(filename.lastIndexOf('.') + 1);
            if (cekgb == 'pdf' || cekgb == 'PDF') {
                if(input.files[0]['size'] > 20480000){
                    alert('ukuran file tidak boleh > 20 Mb !');
                    $('#dokumen_penjabaran_perubahan').val("");
                    $('.dokumen_penjabaran_perubahan').html("Choose file PDF (max-size: 20MB)");
                }else{
                    
                }
                
            }else {
                alert ("file harus berjenis 'pdf' ");
                $('#dokumen_penjabaran_perubahan').val("");
                $('.dokumen_penjabaran_perubahan').html("Choose file PDF (max-size: 20MB)");
                
            }
            
            
        }

    }

    $('#dokumen_perubahan').on('change', function(){
        getURL2(this);
    })

    function getURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var filename = $("#dokumen_perubahan").val();
            
            filename = filename.substring(filename.lastIndexOf('\\') + 1);
            var cekgb = filename.substring(filename.lastIndexOf('.') + 1);
            if (cekgb == 'pdf' || cekgb == 'PDF') {
                if(input.files[0]['size'] > 20480000){
                    alert('ukuran file tidak boleh > 20 Mb !');
                    $('#dokumen_perubahan').val("");
                    $('.dokumen_perubahan').html("Choose file PDF (max-size: 20MB)");
                }else{
                    
                }
                
            }else {
                alert ("file harus berjenis 'pdf' ");
                $('#dokumen_perubahan').val("");
                $('.dokumen_perubahan').html("Choose file PDF (max-size: 20MB)");
                
            }
            
            
        }

    }
</script>
@endpush