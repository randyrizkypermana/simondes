@extends('templates.desa.main')

@section('content')
<div class="row justify-content-center mt-2 mb-4">
    <div class="col-md-12 col-sm-12  ">
        <h5 class="alert alert-info">Form Input/Update Data Akuntabilitas Kelembagaan</h5>
        <div class="x_panel">

            <div class="x_title">
                <div class="d-flex">
                    <form class="form-inline" action="/barjas" method="get">
                        @csrf
                        <div class="form-group mx-sm-3 mb-2">
                            <h6>Masukkan tahun data :</h6>
                            <input type="text" name="tahun" class="form-control ml-3" placeholder="{{ $tahun }}"
                                data-inputmask="'mask': '9999'" style="font-size: .85rem">
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm mb-2">Cek Data</button>
                    </form>
                    <button type="button" class="btn btn-info ml-auto btn-sm" data-toggle="modal" data-target="#copybarjas">
                        Copy Dokumen Pengadaan Barang dan Jasa
                    </button>
                </div>

                <hr>
                @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Error !</strong> Harap periksa inputan dan file berupa pdf dengan maksimal yang ditentukan 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>

                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div>Tahun Data : {{ $tahun }} &emsp; &emsp; <span class="text-info">(Form Input Data Pengadaan Barang dan Jasa)</span></div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                    @if($tpks->isEmpty())
                        <li class="nav-item">
                            <a class="nav-link active" id="profile-tab" href="#tpk" data-toggle="tab" role="tab" aria-controls="tpk" aria-selected="true">Tim Pelaksana Kegiatan (TPK)</a>
                        </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link active" id="profile-tab" href="#edittpk" data-toggle="tab" role="tab" aria-controls="tpk" aria-selected="true">Tim Pelaksana Kegiatan (TPK)</a>
                    </li>
                    @endif
                    @if($surveys->isEmpty())
                    <li class="nav-item">
                        <a class="nav-link" id="home-tab" href="#survey" data-toggle="tab" role="tab" aria-controls="survey" aria-selected="false">Survey Harga</a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link" id="home-tab" href="#editsurvey" data-toggle="tab" role="tab" aria-controls="survey" aria-selected="false">Survey Harga</a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" id="home-tab" href="#dokumen" data-toggle="tab" role="tab" aria-controls="survey" aria-selected="false">Kelengkapan Dokumen</a>
                    </li>
                </ul>

                <div class=" tab-content" id="myTabContent">
                    @if($tpks->isEmpty())
                        <div class="tab-pane fade show active" id="tpk" role="tabpanel" aria-labelledby="home-tab">
                            <form action="/barjas/tambahtpk" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                                <input type="hidden" name="tahun" value="{{ $tahun }}">

                                <div class="row akunwil">
                                    <div class="col-md-8">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="60%">
                                                    <div class="form-group">
                                                        <label for="sotk">Berita Acara Musrenbang yang menunjukan susunan keanggotaan Tim Pelaksana Kegiatan(TPK)</label>   
                                                        <input type="hidden" name="nama_data[]" value="bac">                                                 
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="form-group">
                                                        <label for="upload_bac">Upload Berita Acara</label>
                                                        <div class="custom-file">
                                                            <input type="file" name="upload_bac" class="custom-file-input" id="upload_bac">
                                                            <label class="custom-file-label text-muted bac"for="upload_bac">Choose file PDF (max-size: 1MB)</label>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th width="60%">
                                                    <div class="form-group">
                                                        <label for="sotk">Surat Keputusan (SK) Penetapan Tim Pelaksana Kegiatan(TPK)</label>  
                                                        <input type="hidden" name="nama_data[]" value="sk">                                                  
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="form-group">
                                                        <label for="upload_sk">Upload SK Penetapan</label>
                                                        <div class="custom-file">
                                                            <input type="file" name="upload_sk"
                                                                class="custom-file-input" id="upload_sk">
                                                            <label class="custom-file-label text-muted upload_sk"
                                                                for="upload_sk">Choose file PDF (max-size: 1MB)</label>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group row justify-content-center">
                                    <div class="col-md-7 col-sm-7">
                                        <button type="button" class="btn btn-primary">Cancel</button>
                                        <button type="reset" class="btn btn-primary">Reset</button>
                                        <button type="submit" class="btn btn-success">Kirim Data</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @else
                        <div class="tab-pane fade show active" id="edittpk" role="tabpanel" aria-labelledby="home-tab">
                            <form action="/barjas/edittpk" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                                <input type="hidden" name="tahun" value="{{ $tahun }}">
        
                                <div class="row Akunkel">
                                    <div class="col-md-10">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="50%">
                                                    <div class="form-group">
                                                        <label for="sotk">Berita Acara Musrenbang yang menunjukan susunan keanggotaan Tim Pelaksana Kegiatan(TPK)</label>
                                                        <input type="hidden" name="nama_data[]" value="bac">
                                                    </div>
                                                </th>
                                                <th width="10%" class="text-center" style="vertical-align: middle">
                                                    @if($tpks[0]->file_data)
                                                    <input type="hidden" name="old_0" value="{{ $tpks[0]->file_data }}">
                                                    <a href="{{ asset('storage/'.$tpks[0]->file_data) }}" target="_blank">
                                                        <img src="/img/logo-pdf.webp" width="50px"><br>
                                                        <small>(klik untuk lihat)</small>
                                                    </a>
                                                    @else
                                                    <span class="text-danger">(kosong)</span>
                                                    @endif
                                                </th>
                                                <th>
                                                    <div class="form-group">
                                                        <label for="upload_bac">Ganti Berita Acara</label>
                                                        <div class="custom-file">
                                                            <input type="file" name="upload_bac"
                                                                class="custom-file-input" id="upload_bac">
                                                            <label class="custom-file-label text-muted upload_bac"
                                                                for="upload_bac">Choose
                                                                file PDF
                                                                (max-size: 1MB)</label>
                                                        </div>
                                                    </div>
                                                </th>  
                                            </tr>
                                            <tr>
                                                <th width="50%">
                                                    <div class="form-group">
                                                        <label for="sotk">Surat Keputusan (SK) Penetapan Tim Pelaksana Kegiatan(TPK)</label>
                                                        <input type="hidden" name="nama_data[]" value="sk">
                                                    </div>
                                                </th>
                                                <th width="10%" class="text-center" style="vertical-align: middle">
                                                    @if($tpks[1]->file_data)
                                                    <input type="hidden" name="old_1" value="{{ $tpks[1]->file_data }}">
                                                    <a href="{{ asset('storage/'.$tpks[1]->file_data) }}" target="_blank">
                                                        <img src="/img/logo-pdf.webp" width="50px"><br>
                                                        <small>(klik untuk lihat)</small>
                                                    </a>
                                                    @else
                                                    <span class="text-danger">(kosong)</span>
                                                    @endif
                                                </th>
                                                <th>
                                                    <div class="form-group">
                                                        <label for="upload_sk">Ganti Surat Keputusan</label>
                                                        <div class="custom-file">
                                                            <input type="file" name="upload_sk"
                                                                class="custom-file-input" id="upload_sk">
                                                            <label class="custom-file-label text-muted upload_sk"
                                                                for="upload_sk">Choose
                                                                file PDF
                                                                (max-size: 1MB)</label>
                                                        </div>
                                                    </div>
                                                </th>  
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group row justify-content-center">
                                    <div class="col-md-7 col-sm-7">
                                        <button type="button" class="btn btn-primary">Cancel</button>
                                        <button type="reset" class="btn btn-primary">Reset</button>
                                        <button type="submit" class="btn btn-success">Update Data</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endif
                    @if($surveys->isEmpty())
                        <div class="tab-pane fade" id="survey" role="tabpanel" aria-labelledby="home-tab">
                            <form action="/barjas/tambahsurvey" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                                <input type="hidden" name="tahun" value="{{ $tahun }}">

                                <div class="row akunwil">
                                    <div class="col-md-8">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="60%">
                                                    <div class="form-group">
                                                        <label for="survey">Dokumen Seluruh Berita Acara / Surat Keterangan tentang Pelaksanaan Survey Harga untuk pengadaan Barang dan Jasa</label>   
                                                        <input type="hidden" name="nama_data[]" value="survey">                                                 
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="form-group">
                                                        <label for="upload_survey">Upload Berita Acara</label>
                                                        <div class="custom-file">
                                                            <input type="file" name="upload_survey" class="custom-file-input" id="upload_survey">
                                                            <label class="custom-file-label text-muted bac"for="upload_survey">Choose file PDF (max-size: 2MB)</label>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group row justify-content-center">
                                    <div class="col-md-7 col-sm-7">
                                        <button type="button" class="btn btn-primary">Cancel</button>
                                        <button type="reset" class="btn btn-primary">Reset</button>
                                        <button type="submit" class="btn btn-success">Kirim Data</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @else
                        <div class="tab-pane fade" id="editsurvey" role="tabpanel" aria-labelledby="home-tab">
                            <form action="/barjas/editsurvey" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                                <input type="hidden" name="tahun" value="{{ $tahun }}">
        
                                <div class="row Akunkel">
                                    <div class="col-md-10">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="50%">
                                                    <div class="form-group">
                                                        <label for="sotk">Dokumen Seluruh Berita Acara / Surat Keterangan tentang Pelaksanaan Survey Harga untuk pengadaan Barang dan Jasa</label>
                                                        <input type="hidden" name="nama_data[]" value="bac">
                                                    </div>
                                                </th>
                                                <th width="10%" class="text-center" style="vertical-align: middle">
                                                    @if($surveys[0]->file_data)
                                                    <input type="hidden" name="old_0" value="{{ $surveys[0]->file_data }}">
                                                    <a href="{{ asset('storage/'.$surveys[0]->file_data) }}" target="_blank">
                                                        <img src="/img/logo-pdf.webp" width="50px"><br>
                                                        <small>(klik untuk lihat)</small>
                                                    </a>
                                                    @else
                                                    <span class="text-danger">(kosong)</span>
                                                    @endif
                                                </th>
                                                <th>
                                                    <div class="form-group">
                                                        <label for="upload_survey">Ganti Berita Acara</label>
                                                        <div class="custom-file">
                                                            <input type="file" name="upload_survey"
                                                                class="custom-file-input" id="upload_survey">
                                                            <label class="custom-file-label text-muted upload_survey"
                                                                for="upload_survey">Choose
                                                                file PDF
                                                                (max-size: 2MB)</label>
                                                        </div>
                                                    </div>
                                                </th>  
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group row justify-content-center">
                                    <div class="col-md-7 col-sm-7">
                                        <button type="button" class="btn btn-primary">Cancel</button>
                                        <button type="reset" class="btn btn-primary">Reset</button>
                                        <button type="submit" class="btn btn-success">Update Data</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endif

                    <div class="tab-pane fade" id="survey" role="tabpanel" aria-labelledby="contact-tab">
                        <form action="/adminDesa/tambahAkunkel" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                            <input type="hidden" name="tahun" value="{{ $tahun }}">

                            <div class="row akunwil">
                                <div class="col-md-8">
                                    <table class="table table-bordered">
                                        <tr>

                                            <th width="60%">
                                                <div class="form-group">
                                                    <label for="sotk">Survey Desa Tentang Susunan Organisasi Tatakerja (SOTK)</label>
                                                    <input type="hidden" name="nama_data[]" value="sotk">
                                                    
                                                </div>

                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_sotk">Upload Dokumen Perdes (SOTK)</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_sotk"
                                                            class="custom-file-input" id="upload_sotk">
                                                        <label class="custom-file-label text-muted upload_sotk"
                                                            for="upload_sotk">Choose
                                                            file PDF
                                                            (max-size: 1MB)</label>
                                                    </div>

                                                </div>

                                            </th>

                                        </tr>

                                        <tr>

                                            <th>
                                                <div class="form-group">
                                                    <label for="sklpm">SK Lembaga Pemberdayaan Masyarakat (LPM)</label>
                                                    <input type="hidden" name="nama_data[]" value="sklpm">
                                                    
                                                </div>

                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_sklpm">Upload SK LPM</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_sklpm"
                                                            class="custom-file-input" id="upload_sklpm">
                                                        <label class="custom-file-label text-muted upload_sklpm"
                                                            for="upload_batas_utara">Choose
                                                            file Image
                                                            (max-size: 1MB)</label>
                                                    </div>

                                                </div>

                                            </th>

                                        </tr>
                                        <tr>

                                            <th>
                                                <div class="form-group">
                                                    <label for="sktaruna">SK Karang Taruna</label>
                                                    <input type="hidden" name="nama_data[]" value="sktaruna">
                                                    
                                                </div>

                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_sktaruna">Upload SK Karang Taruna</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_sktaruna"
                                                            class="custom-file-input" id="upload_sktaruna">
                                                        <label class="custom-file-label text-muted upload_sktaruna"
                                                            for="upload_sktaruna">Choose
                                                            file Image
                                                            (max-size: 1MB)</label>
                                                    </div>

                                                </div>

                                            </th>

                                        </tr>
                                        <tr>

                                            <th>
                                                <div class="form-group">
                                                    <label for="sklinmas">SK Perlindungan Masyarakat (Linmas)</label>
                                                    <input type="hidden" name="nama_data[]" value="sklinmas">
                                                    
                                                </div>

                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_linmas">Upload SK Linmas</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_linmas"
                                                            class="custom-file-input" id="upload_linmas">
                                                        <label class="custom-file-label text-muted upload_linmas"
                                                            for="upload_linmas">Choose
                                                            file Image
                                                            (max-size: 1MB)</label>
                                                    </div>

                                                </div>

                                            </th>

                                        </tr>
                                        <tr>

                                            <th>
                                                <div class="form-group">
                                                    <label for="kantor_desa">Keberadaan & Keberfungsian Kantor Desa</label>
                                                    <input type="hidden" name="nama_data[]" value="kantor_desa">
                                                    
                                                    <input type="hidden" name="nama_data[]" value="kg_kdes">
                                                    
                                                </div>

                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_kantor_desa">Upload Tampak Depan Kantor Desa</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_kantor_desa"
                                                            class="custom-file-input" id="upload_kantor_desa">
                                                        <label class="custom-file-label text-muted upload_kantor_desa"
                                                            for="upload_kantor_desa">Choose
                                                            file Image
                                                            (max-size: 1MB)</label>
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <label for="upload_kg_kdes">Upload Kegiatan di Kantor Desa</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_kg_kdes"
                                                            class="custom-file-input" id="upload_kg_kdes">
                                                        <label class="custom-file-label text-muted upload_kg_kdes"
                                                            for="upload_kg_kdes">Choose
                                                            file Image
                                                            (max-size: 1MB)</label>
                                                    </div>

                                                </div>

                                            </th>

                                        </tr>
                                        <tr>
                                            <th>
                                                <div class="form-group">
                                                    <label for="kantor_bpd">Sekretariat / Kantor BPD</label>
                                                    <input type="hidden" name="nama_data[]" value="kantor_bpd">
                                                    
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_kantor_bpd">Upload Sekretariat / Kantor BPD</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_kantor_bpd"
                                                            class="custom-file-input" id="upload_kantor_bpd">
                                                        <label class="custom-file-label text-muted upload_kantor_bpd"
                                                            for="upload_kantor_bpd">Choose
                                                            file Image
                                                            (max-size: 5MB)</label>
                                                    </div>
                                            </th>

                                        </tr>
                                        <tr>
                                            <th>
                                                <div class="form-group">
                                                    <label for="kantor_lpm">Sekretariat / Kantor LPM</label>
                                                    <input type="hidden" name="nama_data[]" value="kantor_lpm">
                                                    
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_kantor_lpm">Upload Sekretariat / Kantor LPM</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_kantor_lpm"
                                                            class="custom-file-input" id="upload_kantor_lpm">
                                                        <label class="custom-file-label text-muted upload_kantor_lpm"
                                                            for="upload_kantor_lpm">Choose
                                                            file Image
                                                            (max-size: 5MB)</label>
                                                    </div>
                                            </th>

                                        </tr>


                                    </table>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group row justify-content-center">
                                <div class="col-md-7 col-sm-7">
                                    <button type="button" class="btn btn-primary">Cancel</button>
                                    <button type="reset" class="btn btn-primary">Reset</button>
                                    <button type="submit" class="btn btn-success">Kirim Data</button>
                                </div>
                            </div>
                        </form>
                    </div>


                    <div class="modal fade" id="copybarjas" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-info">
                                    <h6 class="modal-title text-light" id="staticBackdropLabel">Copy Dokumen Pengadaan Barang dan Jasa</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @if(session()->has('timpaAll'))
                                    <div class="alert bg-danger text-white">Sudah ada data Pengadaan Barang dan Jasa tahun
                                        {{
                                        session('timpaAll') }}
                                    </div>
                                    <form action="/barjas/copybarjas" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <label for="tahuncopy">Tetap Copy dan timpa Seluruh Data Pengadaan Barang dan Jasa Tahun {{
                                                $tahun }} ke
                                                Tahun {{ session('timpaAll') }}
                                                :</label>
                                            <input type="hidden" name="tahuncopy" value="{{ session('timpaAll') }}">
                                            <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                                            <input type="hidden" name="tahunasal" value="{{ $tahun }}">
                                            <input type="hidden" name="timpadata" value="oke">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Copy Data</button>
                                        </div>
                                    </form>
                                    @else
                                    <form action="/barjas/copybarjas" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <label for="tahuncopy">Copy Seluruh Data Pengadaan Barang dan Jasa Tahun {{ $tahun }} ke Tahun:</label>
                                            <select class="form-control" id="tahuncopy" name="tahuncopy" required>
                                                <option value="">== pilih tahun ==</option>
                                                <option>{{ $tahun+1 }}</option>
                                                <option>{{ $tahun+2 }}</option>
                                                <option>{{ $tahun+3 }}</option>
                                            </select>
                                            <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                                            <input type="hidden" name="tahunasal" value="{{ $tahun }}">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Copy Data</button>
                                        </div>
                                    </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <br><br><br>
        </div>
    </div>
</div>
<br>
<br>
@endsection
@push('script')
<script>
    $(document).ready(function() {
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bs-custom-file-input/1.1.1/bs-custom-file-input.min.js" integrity="sha512-LGq7YhCBCj/oBzHKu2XcPdDdYj6rA0G6KV0tCuCImTOeZOV/2iPOqEe5aSSnwviaxcm750Z8AQcAk9rouKtVSg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script>
    bsCustomFileInput.init();
</script>
<script src="/js/akunwil.js"></script>
@if(session()->has('timpaAll'))
<script>
    $('#copyAkunaset').modal('show');
</script>
@endif
@endpush