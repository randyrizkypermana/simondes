@if($anggaran == 'murni')
<?php 
 $datot = $total_p->belanja_murni;
 $datang = 'anggaran_murni';
 ?>
@else
<?php 
 $datot = $total_p->belanja_perubahan;
 $datang = 'anggaran_perubahan';
 ?>
@endif
<p class="text-info">Form Input Tanda Bukti Pengeluaran Uang (TBPU) atau Kwitansi TA {{ $tahun }}</p>
<div class="row">
    <div class="col-md-2">
        <div class="form-group">
            <label for="totalAnggaran">Jumlah Total Anggaran</label>
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text" style="font-size: .75rem">Rp.</div>
                </div>
                <input type="text" class="form-control angka" id="totalAnggaran" value="{{ $datot }}"
                    style="font-size: .75rem">
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label for="totalSPP">Jumlah Akumulasi TBPU / BKP</label>
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text" style="font-size: .75rem">Rp.</div>
                </div>
                <input type="text" class="form-control" id="totalSPP" style="font-size: .75rem">
            </div>

        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label for="progressSPP">Progress TBPU</label>
            <div class="input-group mb-2">
                <input type="text" class="form-control" id="progressSPP" style="font-size: .75rem">
                <div class="input-group-append">
                    <div class="input-group-text" style="font-size: .75rem">%</div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-12">

        <table class="table table-bordered table-striped" style="font-size: .7rem">
            <thead style="background: linear-gradient(to right, rgb(168, 171, 173), grey,rgb(248, 247, 247))">
                <tr>
                    <th width="4%" style="vertical-align: middle" rowspan="2">Kode_keg</th>
                    <th width='25%' style="vertical-align: middle" rowspan="2">Kegiatan</th>
                    @if($anggaran=='perubahan')
                    <th width="7%" class="text-center" style="vertical-align: middle" rowspan="2">Anggaran Perubahan
                        <br> (Rp)
                    </th>
                    @else
                    <th width="7%" class="text-center" style="vertical-align: middle" rowspan="2">Anggaran <br>(Rp)</th>
                    @endif
                    <th colspan="6" class="text-center">Akumulasi Tanda Bukti Pengeluaran Uang (TBPU / BKP)</th>
                    <th width="7%" rowspan="2" style="vertical-align: middle">Sisa Anggaran</th>

                    <th width="6%" style="vertical-align: middle" class="text-center" rowspan="2">Dokumen TBPU</th>

                    <th width="9%" style="vertical-align: middle" rowspan="2">Upload TBPU</th>
                </tr>
                <tr>
                    <th width="7%" class="text-center" style="vertical-align: middle">Belanja <br>Pegawai <br>(Rp)</th>
                    <th width="7%" class="text-center" style="vertical-align: middle">Belanja <br>Barjas <br>(Rp)</th>
                    <th width="7%" class="text-center" style="vertical-align: middle">Belanja <br>Modal <br>(Rp)</th>
                    <th width="7%" class="text-center" style="vertical-align: middle">Belanja <br>Tak Terduga <br>(Rp)
                    </th>
                    <th width="7%" class="text-center" style="vertical-align: middle">Potongan PPh <br>(Rp)</th>
                    <th width="7%" class="text-center" style="vertical-align: middle">Potongan PPN <br>(Rp)</th>
                </tr>
            </thead>
            <tbody>
                @foreach($apbdes_kegiatans as $keg)
                @if($keg->anggaran_murni || $keg->anggaran_perubahan)
                <tr>
                    <td>{{ $keg->kode_kegiatan }}</td>
                    <td>{{ substr($keg->kegiatan->kegiatan, 0,75)."..." }}</td>
                    <td class="text-right">{{ $keg->$datang }}</td>
                    <td class="text-right ">
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-center" style="vertical-align: middle">

                    </td>
                    <td>

                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                            data-target="#tambahTBPU{{ $keg->id }}" style="font-size: .65rem">
                            + TBPU
                        </button>
                    </td>
                    <!-- Modal -->
                    <div class="modal fade" id="tambahTBPU{{ $keg->id }}" data-backdrop="static" data-keyboard="false"
                        tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <h5 class="modal-title text-dark" id="staticBackdropLabel">Form Tambah Tanda Bukti
                                        Pengeluaran Uang</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="/adminDesa/tambahTBPU" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                                    <input type="hidden" name="tahun" value="{{ $tahun }}">
                                    <input type="hidden" name="apbdes_kegiatan_id" value="{{ $keg->id }}">

                                    <div class="modal-body">
                                        <div class="form-group mb-3">
                                            <label for="kegiatan">Nama Kegiatan</label>
                                            <input type="text" class="form-control" id="kegiatan"
                                                value="{{ $keg->kegiatan->kegiatan }}" style="font-size: .75rem"
                                                readonly>
                                        </div>
                                        <div class="form-group  mb-3">
                                            <label>Nomor Tanda Bukti Pengeluaran Uang</label>
                                            <input type="text" name="nomor" class="form-control"
                                                style="font-size: .75rem" required>
                                        </div>
                                        <div class="form-group  mb-3">
                                            <label>Jumlah Uang</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text" style="font-size: .75rem">Rp.</div>
                                                </div>
                                                <input type="text" name="jumlah" class="form-control jumlah"
                                                    id="inlineFormInputGroup" style="font-size: .75rem" required>
                                            </div>
                                        </div>
                                        <div class="form-group  mb-3">
                                            <label>Sebagai Pembayaran</label>
                                            <div class="input-group mb-2">
                                                <input type="text" name="sebagai" class="form-control sebagai"
                                                    style="font-size: .75rem" required>
                                            </div>
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="jenis_belanja">Jenis Belanja</label>
                                            <select class="form-control" name="belanja_id" id="jenis_belanja"
                                                style="font-size: .75rem" required>
                                                <option value="">==pilih jenis belanja==</option>
                                                <option value="1">5.1 Belanja Pegawai</option>
                                                <option value="2">5.2 Belanja Barang/Jasa</option>
                                                <option value="3">5.3 Belanja Modal</option>
                                                <option value="4">5.4 Belanja Tak Terduga</option>
                                            </select>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6 mb-3">
                                                <label>Jumlah Potongan PPh</label>
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text" style="font-size: .75rem">Rp.
                                                        </div>
                                                    </div>
                                                    <input type="text" name="pph" class="form-control pph angka"
                                                        id="pph" style="font-size: .75rem" placeholder="0">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6 mb-3">
                                                <label>Jumlah Potongan PPN</label>
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text" style="font-size: .75rem">Rp.
                                                        </div>
                                                    </div>
                                                    <input type="text" name="ppn" class="form-control ppn angka"
                                                        id="ppn" style="font-size: .75rem" placeholder="0">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group  mb-3">
                                            <label>Tanggal Tanda Bukti Pengeluaran Uang</label>
                                            <input type="date" name="tanggal" class="form-control"
                                                style="font-size: .75rem" required>
                                        </div>
                                        <div class="form-group  mb-3 mt-4">
                                            <label>Upload Tanda Bukti Pengeluaran Uang (TBPU/BKP/Kwitansi)
                                            </label>
                                            <div class="custom-file">
                                                <input type="file" name="file_bkp" class="custom-file-input file_bkp"
                                                    id="customFile " required>
                                                <label class="custom-file-label nama_file" for="customFile"
                                                    style="font-size: .75rem">Upload file PDF max:
                                                    1MB</label>
                                            </div>
                                        </div>
                                        <div class="form-group  mb-3 mt-4">
                                            <small><label>Upload Lampiran TBPU (Struk/Nota/Tanda Terima
                                                    Siltap,honor/Surat
                                                    Pesanan/SPT/SPPD/Dokumen Pengadaan/bukti belanja lainnya)
                                                </label></small>
                                            <div class="custom-file">
                                                <input type="file" name="file_lampiran"
                                                    class="custom-file-input file_lampiran" id="customFile ">
                                                <label class="custom-file-label nama_file2" for="customFile"
                                                    style="font-size: .75rem">Upload file PDF max:
                                                    2MB</label>
                                            </div>
                                            <small>optional (dapat disusulkan kemudian)</small>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">KIRIM DATA</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </tr>

                @endif
                @endforeach

            </tbody>

        </table>

    </div>
</div>
<hr>
@push('script')
<script src="/bs-custom-file-input/dist/bs-custom-file-input.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script>
    bsCustomFileInput.init();
    $('.jumlah').mask('000.000.000.000.000', {reverse: true});
    $('.angka').mask('000.000.000.000.000', {reverse: true});

    $('.file_bkp').on('change', function(e){
        e.preventDefault();
        var file = $(this).val();
        getURL(this, file);

    })


    function getURL(input, data) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var filename = data;
        filename = filename.substring(filename.lastIndexOf('\\') + 1);
        var cekgb = filename.substring(filename.lastIndexOf('.') + 1);
    
        if (cekgb == 'pdf' || cekgb == 'PDF') {
            if(input.files[0]['size'] > 1024000){
                alert('ukuran file tidak boleh > 1 Mb !');
                $('.file_bkp').val("");
                
                $('.nama_file').html("Choose file PDF (max-size: 1MB)");
            }else{
                
            }
            
        }else {
            alert ("file harus berjenis 'pdf' ");
            $('.file_bkp').val("");
            $('.nama_file').html("Choose PDF (max-size: 1MB)");
            
        }
               
        }

    }

    $('.file_lampiran').on('change', function(e){
        e.preventDefault();
        var file = $(this).val();
        getURL2(this, file);

    })
    function getURL2(input, data) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var filename = data;
        filename = filename.substring(filename.lastIndexOf('\\') + 1);
        var cekgb = filename.substring(filename.lastIndexOf('.') + 1);
    
        if (cekgb == 'pdf' || cekgb == 'PDF') {
            if(input.files[0]['size'] > 1024000){
                alert('ukuran file tidak boleh > 1 Mb !');
                $('.file_lampiran').val("");
                
                $('.nama_file2').html("Choose file PDF (max-size: 1MB)");
            }else{
                
            }
            
        }else {
            alert ("file harus berjenis 'pdf' ");
            $('.file_lampiran').val("");
            $('.nama_file2').html("Choose PDF (max-size: 1MB)");
            
        }
               
        }

    }

var totA = Number($('#totalAnggaran').val().replaceAll('.', ''));

var jum = $('.jumSPP').length;
var spp = 0;
for(let i=0; i<jum; i++){
    var nominal = Number($('.jumSPP').eq(i).val().replaceAll('.',''));
    spp += nominal;
}   
var progress = (spp/totA)*100;
    progress = progress.toFixed(2);
 
$('#totalSPP').val(spp);
$('#progressSPP').val(progress);
$('#totalSPP').mask('000.000.000.000.000', {reverse: true});

</script>


@endpush