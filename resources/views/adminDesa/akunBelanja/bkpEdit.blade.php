@if($anggaran == 'murni')
<?php 
 $datot = $total_p->belanja_murni;
 $datang = 'anggaran_murni';
 ?>
@else
<?php 
 $datot = $total_p->belanja_perubahan;
 $datang = 'anggaran_perubahan';
 ?>
@endif
<?php $totbkp = 0; ?>
@foreach($tbpus as $tb)
<?php 
$jm = str_replace(".",'',$tb->jumlah );
$jm = intval($jm);
$totbkp += $jm;

?>
@endforeach

<p class="text-info">Form Update Tanda Bukti Pengeluaran Uang (TBPU/Kwitansi/BKP) TA {{ $tahun }}
</p>
<div class="row">
    <div class="col-md-2">
        <div class="form-group">
            <label for="totalAnggaran">Jumlah Total Anggaran Kegiatan</label>
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text" style="font-size: .75rem">Rp.</div>
                </div>
                <input type="text" class="form-control angka" id="totalAnggaran" value="{{ $datot }}"
                    style="font-size: .75rem">
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label for="totalSPP">Jumlah Akumulasi TBPU / BKP</label>
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text" style="font-size: .75rem">Rp.</div>
                </div>
                <input type="text" class="form-control angka" id="totalBKP" value="{{ $totbkp }}"
                    style="font-size: .75rem">
            </div>

        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label for="progressSPP">Progress TBPU</label>
            <div class="input-group mb-2">
                <input type="text" class="form-control" id="progress" style="font-size: .75rem">
                <div class="input-group-append">
                    <div class="input-group-text" style="font-size: .75rem">%</div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <table class="table table-bordered table-striped" style="font-size: .7rem">
            <thead style="background: linear-gradient(to right, rgb(168, 171, 173), grey,rgb(248, 247, 247))">
                <tr>
                    <th width="4%" style="vertical-align: middle" rowspan="2">Kode_keg</th>
                    <th width='23%' style="vertical-align: middle" rowspan="2">Kegiatan</th>
                    @if($anggaran=='perubahan')
                    <th width="7%" class="text-center" style="vertical-align: middle" rowspan="2">Anggaran Perubahan
                        <br> (Rp)
                    </th>
                    @else
                    <th width="7%" class="text-center" style="vertical-align: middle" rowspan="2">Anggaran <br>(Rp)
                    </th>
                    @endif
                    <th colspan="6" class="text-center">Akumulasi Tanda Bukti Pengeluaran Uang (TBPU)</th>
                    <th width="7%" rowspan="2" style="vertical-align: middle" class="text-center">Sisa Anggaran <br>(Rp)
                    </th>

                    <th width="6%" style="vertical-align: middle" class="text-center" rowspan="2">Cek Dokumen TBPU
                    </th>

                    <th width="11%" style="vertical-align: middle" rowspan="2">Upload TBPU</th>
                </tr>
                <tr>
                    <th width="7%" class="text-center" style="vertical-align: middle">Belanja <br>Pegawai <br>(Rp)
                    </th>
                    <th width="7%" class="text-center" style="vertical-align: middle">Belanja <br>Barjas <br>(Rp)
                    </th>
                    <th width="7%" class="text-center" style="vertical-align: middle">Belanja <br>Modal <br>(Rp)
                    </th>
                    <th width="7%" class="text-center" style="vertical-align: middle">Belanja <br>Tak Terduga
                        <br>(Rp)
                    </th>
                    <th width="7%" class="text-center" style="vertical-align: middle">Potongan PPh <br>(Rp)</th>
                    <th width="7%" class="text-center" style="vertical-align: middle">Potongan PPN <br>(Rp)</th>
                </tr>
            </thead>
            <tbody>
                @foreach($apbdes_kegiatans as $keg)
                @if($keg->anggaran_murni || $keg->anggaran_perubahan)
                <tr>
                    <td style="vertical-align: middle">{{ $keg->kode_kegiatan }}</td>
                    <td style="vertical-align: middle" style="font-size: .65rem">{{ substr($keg->kegiatan->kegiatan,
                        0,75)."..." }}</td>
                    <td class="text-right datang" style="vertical-align: middle">{{ $keg->$datang }}</td>
                    <td class="text-right pr-1 " style="vertical-align: middle">
                        <?php 
                            $topeg = 0;
                                                         
                            $belpeg = $tbpus->where('apbdes_kegiatan_id', $keg->id)->where('belanja_id', 1);
                           foreach ($belpeg as $p) {
                             $p = intval(str_replace('.', '', $p->jumlah));
                             $topeg += $p;
                           }
                           
                        ?>
                        <input type="text" class="angka text-right p-0 belanjaPegawai" value="{{ $topeg }}" size="10"
                            disabled>
                    </td>
                    <td class="text-right " style="vertical-align: middle">
                        <?php 
                            $tobar = 0;
                                                         
                            $belbar = $tbpus->where('apbdes_kegiatan_id', $keg->id)->where('belanja_id', 2);
                           foreach ($belbar as $bb) {
                             $bb = intval(str_replace('.', '', $bb->jumlah));
                             $tobar += $bb;
                           }
                           
                        ?>
                        <input type="text" class="angka text-right p-0 belanjaBarjas" value="{{ $tobar }}" size="10"
                            disabled>
                    </td>
                    <td class="text-right " style="vertical-align: middle">
                        <?php 
                        $tomod = 0;
                                                     
                        $belmod = $tbpus->where('apbdes_kegiatan_id', $keg->id)->where('belanja_id', 3);
                       foreach ($belmod as $bm) {
                         $bm = intval(str_replace('.', '', $bm->jumlah));
                         $tomod += $bm;
                       }
                       
                    ?>
                        <input type="text" class="angka text-right p-0 belanjaModal" value="{{ $tomod }}" size="10"
                            disabled>
                    </td>
                    <td class="text-right " style="vertical-align: middle">
                        <?php 
                        $toga = 0;
                                                     
                        $belga = $tbpus->where('apbdes_kegiatan_id', $keg->id)->where('belanja_id', 4);
                       foreach ($belga as $bg) {
                         $bg = intval(str_replace('.', '', $bg->jumlah));
                         $toga += $bg;
                       }
                       
                    ?>
                        <input type="text" class="angka text-right p-0 belanjaTakterduga" value="{{ $toga }}" size="10"
                            disabled>
                    </td>
                    <td class="text-right" style="vertical-align: middle">
                        <?php 
                        $pph = 0;
                                                     
                        $belga = $tbpus->where('apbdes_kegiatan_id', $keg->id);
                       foreach ($belga as $p) {
                         $p = intval(str_replace('.', '', $p->pph));
                         $pph += $p;
                       }
                       
                    ?>
                        <input type="text" class="angka text-right p-0 pph" value="{{ $pph }}" size="10" disabled>
                    </td>
                    <td class="text-right" style="vertical-align: middle">
                        <?php 
                        $ppn = 0;
                                                     
                        $belga = $tbpus->where('apbdes_kegiatan_id', $keg->id);
                       foreach ($belga as $pn) {
                         $pn = intval(str_replace('.', '', $pn->ppn));
                         $ppn += $pn;
                       }
                       
                    ?>
                        <input type="text" class="angka text-right p-0 ppn" value="{{ $ppn }}" size="10" disabled>
                    </td>
                    <td class="text-right" style="vertical-align: middle">
                        <input type="text" class="text-right p-0 sisa" size="10" disabled>
                    </td>
                    <td class="text-center" style="vertical-align: middle">

                        @if(count($tbpus->where('apbdes_kegiatan_id', $keg->id)))
                        <a href="/adminDesa/formPenataanBelanja?jenis=cek_tbpu&tahun={{ $tahun }}&asal_id={{ $infos->asal_id }}&kegiatan={{ $keg->id }}"
                            class="btn btn-sm btn-info px-1 py-0" style="font-size: .8rem">link</a>
                        @endif
                    </td>
                    <td style="vertical-align: middle">

                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                            data-target="#tambahTBPU{{ $keg->id }}" style="font-size: .60rem">
                            + TBPU
                        </button>
                    </td>
                    <!-- Modal -->
                    <div class="modal fade" id="tambahTBPU{{ $keg->id }}" data-backdrop="static" data-keyboard="false"
                        tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <h5 class="modal-title text-dark" id="staticBackdropLabel">Form Tambah Tanda
                                        Bukti
                                        Pengeluaran Uang (TBPU)</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="/adminDesa/tambahTBPU" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                                    <input type="hidden" name="tahun" value="{{ $tahun }}">
                                    <input type="hidden" name="kegiatan_id" value="{{ $keg->kegiatan_id }}">
                                    <input type="hidden" name="apbdes_kegiatan_id" value="{{ $keg->id }}">

                                    <div class="modal-body">
                                        <div class="form-group mb-3">
                                            <label for="kegiatan">Nama Kegiatan</label>
                                            <input type="text" class="form-control" id="kegiatan"
                                                value="{{ $keg->kegiatan->kegiatan }}" style="font-size: .75rem"
                                                readonly>
                                        </div>
                                        <div class="form-group  mb-3">
                                            <label>Nomor Tanda Bukti Pengeluaran Uang</label>
                                            <input type="text" name="nomor" class="form-control"
                                                style="font-size: .75rem" required>
                                        </div>
                                        <div class="form-group  mb-3">
                                            <label>Jumlah Uang</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text" style="font-size: .75rem">Rp.
                                                    </div>
                                                </div>
                                                <input type="text" name="jumlah" class="form-control jumlah"
                                                    id="inlineFormInputGroup" style="font-size: .75rem" required>
                                            </div>
                                        </div>
                                        <div class="form-group  mb-3">
                                            <label>Sebagai Pembayaran</label>
                                            <div class="input-group mb-2">
                                                <input type="text" name="sebagai" class="form-control sebagai"
                                                    style="font-size: .75rem" required>
                                            </div>
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="jenis_belanja">Jenis Belanja</label>
                                            <select class="form-control" name="belanja_id" id="jenis_belanja"
                                                style="font-size: .75rem" required>
                                                <option value="">==pilih jenis belanja==</option>
                                                <option value="1">5.1 Belanja Pegawai</option>
                                                <option value="2">5.2 Belanja Barang/Jasa</option>
                                                <option value="3">5.3 Belanja Modal</option>
                                                <option value="4">5.4 Belanja Tak Terduga</option>
                                            </select>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6 mb-3">
                                                <label>Jumlah Potongan PPh</label>
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text" style="font-size: .75rem">Rp.
                                                        </div>
                                                    </div>
                                                    <input type="text" name="pph" class="form-control pph angka"
                                                        id="pph" style="font-size: .75rem" placeholder="0">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6 mb-3">
                                                <label>Jumlah Potongan PPN</label>
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text" style="font-size: .75rem">Rp.
                                                        </div>
                                                    </div>
                                                    <input type="text" name="ppn" class="form-control ppn angka"
                                                        id="ppn" style="font-size: .75rem" placeholder="0">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group  mb-3">
                                            <label>Tanggal Tanda Bukti Pengeluaran Uang</label>
                                            <input type="date" name="tanggal" class="form-control"
                                                style="font-size: .75rem" required>
                                        </div>
                                        <div class="form-group  mb-3 mt-4">
                                            <label>Upload Tanda Bukti Pengeluaran Uang (TBPU/BKP/Kwitansi)
                                            </label>
                                            <div class="custom-file">
                                                <input type="file" name="file_bkp" class="custom-file-input file_bkp"
                                                    id="customFile " required>
                                                <label class="custom-file-label nama_file" for="customFile"
                                                    style="font-size: .75rem">Upload file PDF max:
                                                    1MB</label>
                                            </div>
                                        </div>
                                        <div class="form-group  mb-3 mt-4">
                                            <small><label>Upload Lampiran TBPU (Struk/Nota/Tanda Terima
                                                    Siltap,honor/Surat
                                                    Pesanan/SPT/SPPD/Dokumen Pengadaan/bukti belanja lainnya)
                                                </label></small>
                                            <div class="custom-file">
                                                <input type="file" name="file_lampiran"
                                                    class="custom-file-input file_lampiran" id="customFile ">
                                                <label class="custom-file-label nama_file2" for="customFile"
                                                    style="font-size: .75rem">Upload file PDF max:
                                                    2MB</label>
                                            </div>
                                            <small>optional (dapat disusulkan kemudian)</small>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">KIRIM DATA</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </tr>

                @endif
                @endforeach

            </tbody>

        </table>

    </div>
</div>
<hr>
@push('script')
<script src="/bs-custom-file-input/dist/bs-custom-file-input.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script>
    bsCustomFileInput.init();
   

    Jumsisa();
    function Jumsisa(){
        var totA = Number($('#totalAnggaran').val().replaceAll('.', ''));
        var tbpu = 0;
        var jumlahBelanja = 0; 
        var jumsisa = $('.sisa').length;
        var jumlahSisa = 0;
        for(let i=0; i<jumsisa; i++){
            var datang = Number($('.datang').eq(i).html().replaceAll('.',''));
            var jumpeg = Number($('.belanjaPegawai').eq(i).val().replaceAll('.',''));
            var jumbar =  Number($('.belanjaBarjas').eq(i).val().replaceAll('.',''));
            var jummod = Number($('.belanjaModal').eq(i).val().replaceAll('.',''));
            var jumga = Number($('.belanjaTakterduga').eq(i).val().replaceAll('.',''));
            var jumja = jumpeg+jumbar+jummod+jumga;
            tbpu += jumja;
            jumlahBelanja += jumja;
            jumlahSisa = datang - jumja;
           
            if(jumja){
            $('.sisa').eq(i).val(jumlahSisa);}

           if(jumlahSisa < 0){
            
            $('.sisa').eq(i).addClass('text-danger');
           }
                            
        }
        $('#totalTBPU').val(tbpu);
        var progress = (jumlahBelanja/totA)*100;
            progress = progress.toFixed(2);
        $('#progressTBPU').val(progress);

        var Tpph = 0;
        var Lpph = $('.pph').length;
        for(let i=0; i<Lpph; i++){
            var pph = $('.pph').eq(i).val().replaceAll('.','');
                pph = Number(pph);
            Tpph += pph;
        }
        var Tppn = 0;
        var Lppn = $('.ppn').length;
        for(let i=0; i<Lppn; i++){
            var ppn = $('.ppn').eq(i).val().replaceAll('.','');
                ppn = Number(ppn);
            Tppn += ppn;
        }
        
        $('#totalPajak').val(Tppn+Tpph);

    }

    $('#totalTBPU').mask('000.000.000.000.000', {reverse: true});
    $('.sisa').mask('000.000.000.000.000', {reverse: true});
    $('.jumlah').mask('000.000.000.000.000', {reverse: true});
    $('.angka').mask('000.000.000.000.000', {reverse: true});
    $('#totalPajak').mask('000.000.000.000.000', {reverse: true});

    var totA = $('#totalAnggaran').val().replaceAll('.','');
        totA = Number(totA);
    var totB =  $('#totalBKP').val().replaceAll('.','');
        totB = Number(totB);
    var progress = (totB/totA)*100;
        progress = progress.toFixed(2);
    $('#progress').val(progress);



    $('.file_bkp').on('change', function(e){
        e.preventDefault();
        var file = $(this).val();
        getURL(this, file);

    })


    function getURL(input, data) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var filename = data;
        filename = filename.substring(filename.lastIndexOf('\\') + 1);
        var cekgb = filename.substring(filename.lastIndexOf('.') + 1);
    
        if (cekgb == 'pdf' || cekgb == 'PDF') {
            if(input.files[0]['size'] > 1024000){
                alert('ukuran file tidak boleh > 1 Mb !');
                $('.file_bkp').val("");
                
                $('.nama_file').html("Choose file PDF (max-size: 1MB)");
            }else{
                
            }
            
        }else {
            alert ("file harus berjenis 'pdf' ");
            $('.file_bkp').val("");
            $('.nama_file').html("Choose PDF (max-size: 1MB)");
            
        }
               
        }

    }

    $('.file_lampiran').on('change', function(e){
        e.preventDefault();
        var file = $(this).val();
        getURL2(this, file);

    })
    function getURL2(input, data) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var filename = data;
        filename = filename.substring(filename.lastIndexOf('\\') + 1);
        var cekgb = filename.substring(filename.lastIndexOf('.') + 1);
    
        if (cekgb == 'pdf' || cekgb == 'PDF') {
            if(input.files[0]['size'] > 2048000){
                alert('ukuran file tidak boleh > 2 Mb !');
                $('.file_lampiran').val("");
                
                $('.nama_file2').html("Choose file PDF (max-size: 2MB)");
            }else{
                
            }
            
        }else {
            alert ("file harus berjenis 'pdf' ");
            $('.file_lampiran').val("");
            $('.nama_file2').html("Choose PDF (max-size: 2MB)");
            
        }
               
        }

    }




</script>


@endpush