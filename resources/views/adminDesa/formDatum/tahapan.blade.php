<!-- Smart Wizard -->
<style>
    .circle_percent {
        font-size: 1rem;
        width: 100px;
        height: 100px;
        position: relative;
        background: #eee;
        border-radius: 50%;
        overflow: hidden;
        display: inline-block;
        margin: 20px;
    }

    .circle_inner {
        position: absolute;
        left: 0;
        top: 0;
        width: 100px;
        height: 100px;
        clip: rect(0 100px 100px 50px);
    }

    .round_per {
        position: absolute;
        left: 0;
        top: 0;
        width: 100px;
        height: 100px;
        background: blue;
        clip: rect(0 100px 100px 50px);
        transform: rotate(180deg);
        transition: 1.05s;
    }

    .percent_more .circle_inner {
        clip: rect(0 50px 100px 0em);
    }

    .percent_more:after {
        position: absolute;
        left: 50px;
        top: 0em;
        right: 0;
        bottom: 0;
        background: blue;
        content: '';
    }

    .circle_inbox {
        position: absolute;
        top: 10px;
        left: 10px;
        right: 10px;
        bottom: 10px;
        background: #fff;
        z-index: 3;
        border-radius: 50%;
    }

    .percent_text {
        position: absolute;
        font-size: 1rem;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        z-index: 3;
    }
</style>
<br>
<div class="row">
    <div class="col-md-2 d-flex justify-content-center">
        <div class="card">
            <h6 class="py-1 text-center">Data Monografi</h6>
            <div class="circle_percent mt-0" data-percent="75">
                <div class="circle_inner">
                    <div class="round_per"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<hr>
<!-- End SmartWizard Content -->