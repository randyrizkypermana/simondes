@extends('templates.desa.main')

@section('content')
<div class="row justify-content-center mt-2 mb-4">
    <div class="col-md-12 col-sm-12  ">
        <h5 class="alert alert-info">Form Input/Update Data Badan Usaha Milik Desa</h5>
        <div class="x_panel">
            <div class="x_title">
                <div class="d-flex">
                    <form class="form-inline" action="/Akunbumdes" method="get">
                        @csrf
                        <div class="form-group mx-sm-3 mb-2">
                            <h6>Masukkan tahun data :</h6>
                            <input type="text" name="tahun" class="form-control ml-3" placeholder="{{ $tahun }}"
                                data-inputmask="'mask': '9999'" style="font-size: .85rem">
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm mb-2">Cek Data</button>
                    </form>
                </div>
                <hr>
                @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Error !</strong> Harap periksa inputan dan file berupa pdf dengan maksimal yang ditentukan 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>

                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div>Tahun Data : {{ $tahun }} &emsp; &emsp; <span class="text-info">(Form Input Data Akuntabilitas Badan Usaha Milik Desa)</span></div>
            <div class="clearfix"></div>
            
            <br>
            <div class="x_content">
                <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#dataumum" role="tab" aria-controls="kiba" aria-selected="true">Data Umum BUMDES</a>
                    </li>       
                    @foreach($Akunbumdess as $ab)
                    @if($ab->terdaftar == 'Terdaftar Kemendes')                   
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#akuntabilitas" role="tab" aria-controls="kiba" aria-selected="true">Akuntabilitas BUMDES</a>
                    </li>
                    @endif
                    @endforeach
                    {{-- 
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#keuangan" role="tab" aria-controls="kiba" aria-selected="true">Keuangan BUMDES</a>
                    </li> --}}
                </ul>
                
                <div class="modal fade" id="modalMd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="modalMdTitle"></h4>
                            </div>
                            <div class="modal-body">
                                <div class="modalError"></div>
                                <div id="modalMdContent"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <div class="container">
                    <div class="tab-content" id="myTabContent">
                        <!-- view data Umum BUMDES-->
                        <div class="tab-pane fade show active" id="dataumum" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-tools">
                                            <button class="btn btn-primary btn-sm btn-flat" data-toggle="modal" data-target="#modal-add">Tambah Bumdes</button>
                                            <button type="button" class="btn btn-secondary btn-sm mb-2  float-right" data-toggle="modal" data-target="#copyAkunbumdes">Copy Seluruh Data Umum BUMDES</button>
                                        </div>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                                <thead>
                                                    <tr>
                                                        <th style="width: 5%">No</th>
                                                        <th style="width: 10%; text-align: center">Nama BUMDES</th>
                                                        <th style="width: 10%; text-align: center">Sifat BUMDES</th>
                                                        <th style="width: 13%; text-align: center">Terdaftar Kemendes</th>
                                                        <th style="text-align: center">Dokumen</th>
                                                        <th style="text-align: center">Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($Akunbumdess as $akunbumdes)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $akunbumdes->nama_bumdes }}</th>
                                                        <td>{{ $akunbumdes->sifat }}</td>
                                                        <td>{{ $akunbumdes->terdaftar }}</td>
                                                        <td>
                                                            @if($akunbumdes->terdaftar == 'Terdaftar Kemendes')
                                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Berita Acara Musyawarah Desa</td>
                                                                        @if($akunbumdes->bac)
                                                                        <td style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->bac}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Peraturan Desa Pendirian BUMDES</td>
                                                                        @if($akunbumdes->perdes)
                                                                        <td style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->perdes}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Anggaran Rumah Tangga BUMDES</td>
                                                                        @if($akunbumdes->adart)
                                                                        <td style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->adart}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Rencana Program Kerja BUMDES</td>
                                                                        @if($akunbumdes->rencana_program)
                                                                        <td style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->rencana_program}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Struktur Organisasi</td>
                                                                        @if($akunbumdes->struktur_organisasi)
                                                                        <td style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->struktur_organisasi}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            @else
                                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Peraturan Desa Pendirian BUMDES</td>
                                                                        @if($akunbumdes->perdes)
                                                                        <td colspan="2" style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->perdes}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>SK Kepengurusan Terakhir</td>
                                                                        @if($akunbumdes->sk_kepengurusan)
                                                                        <td colspan="2" style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->sk_kepengurusan}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Buku Rekening Terkhir BUMDES</td>
                                                                        @if($akunbumdes->buku_rekening)
                                                                        <td colspan="2" style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->buku_rekening}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Tahun Penyertaan Modal Pertama | Anggaran Penyertaan Modal</td>
                                                                        @if($akunbumdes->modal_pertama)
                                                                            <td> {{ $akunbumdes->modal_pertama }}</td>
                                                                            <td>@rupiah($akunbumdes->dana_modal_pertama)</td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Tahun Penyertaan Modal Terakhir | Anggaran Penyertaan Modal</td>
                                                                        @if($akunbumdes->modal_terakhir)
                                                                            <td> {{ $akunbumdes->modal_terakhir }}</td>
                                                                            <td>@rupiah($akunbumdes->dana_modal_terakhir)</td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Laporan Keuangan BUMDES Terkahir</td>
                                                                        @if($akunbumdes->lap_keuangan)
                                                                        <td colspan="2" style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->lap_keuangan}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                </tbody>
                                                            </table>                                                        
                                                            @endif
                                                        </td>
                                                        @if($akunbumdes->terdaftar == 'Terdaftar Kemendes')
                                                        <td style="width: 10%"><button type="button" class="btn btn-xs btn-success edit float-left" data-toggle="modal" data-target="#modal-edit{{ $akunbumdes->id }}"><i class="fa fa-pencil"></i></button>
                                                            <!-- Modal Edit BUMDES Terdaftar Kemendes-->
                                                            <div id="modal-edit{{ $akunbumdes->id }}" class="modal fade" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-md">
                                                                    <form name="frm_add" id="frm_add" class="form-horizontal" action="/Akunbumdes/update" method="POST" enctype="multipart/form-data">
                                                                        @csrf
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                                <h6 class="modal-title">Edit Data</h6>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="form-group">
                                                                                    <input type="hidden" name="tahun" value="{{ $tahun }}">
                                                                                    <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                                                                                    <input type="hidden" name="id" value="{{ $akunbumdes->id }}">
                                                                                    <label class="font-weight-bold">Nama Badan Usaha Milik Desa</label>
                                                                                    <input type="text" class="form-control @error('nama_bumdes') is-invalid @enderror" id="nama_bumdes" name="nama_bumdes" value="{{ $akunbumdes->nama_bumdes }}" placeholder="Masukkan Nama BUMDES" required>
                                                                                </div>                      
                                                                                <div class="form-group">
                                                                                    <label class="font-weight-bold">Terdaftar</label>
                                                                                        @if($akunbumdes->terdaftar == 'Terdaftar Kemendes')
                                                                                        <select class="form-control" id="terdaftar" name="terdaftar">
                                                                                            <option value="">== Pilih ==</option>
                                                                                            <option value selected="Terdaftar Kemendes">Terdaftar Kemendes</option>
                                                                                            <option value="Belum Terdaftar Kemendes">Belum Terdaftar Kemendes</option>
                                                                                        </select>
                                                                                        @elseif($akunbumdes->terdaftar == 'Belum Terdaftar Kemendes')
                                                                                        <select class="form-control" id="terdaftar" name="terdaftar">
                                                                                            <option value="">== Pilih ==</option>
                                                                                            <option value ="Terdaftar Kemendes">Terdaftar Kemendes</option>
                                                                                            <option value selected="Belum Terdaftar Kemendes">Belum Terdaftar Kemendes</option>
                                                                                        </select>
                                                                                        @else
                                                                                        <select class="form-control" id="terdaftar" name="terdaftar">
                                                                                            <option value selected="">== Pilih ==</option>
                                                                                            <option value ="Terdaftar Kemendes">Terdaftar Kemendes</option>
                                                                                            <option value ="Belum Terdaftar Kemendes">Belum Terdaftar Kemendes</option>
                                                                                        </select>
                                                                                        @endif
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="font-weight-bold">Sifat BUMDES</label>
                                                                                        @if($akunbumdes->sifat == 'Badan Usaha Milik Desa')
                                                                                        <select class="form-control" id="sifat" name="sifat">
                                                                                            <option value="">== Pilih ==</option>
                                                                                            <option value selected="Badan Usaha Milik Desa">Badan Usaha Milik Desa</option>
                                                                                            <option value="Badan Usaha Milik Desa Bersama">Badan Usaha Milik Desa Bersama</option>
                                                                                        </select>
                                                                                        @elseif($akunbumdes->sifat == 'Badan Usaha Milik Desa Bersama')
                                                                                        <select class="form-control" id="sifat" name="sifat">
                                                                                            <option value="">== Pilih ==</option>
                                                                                            <option value ="Badan Usaha Milik Desa">Badan Usaha Milik Desa</option>
                                                                                            <option value selected="Badan Usaha Milik Desa Bersama">Badan Usaha Milik Desa Bersama</option>
                                                                                        </select>
                                                                                        @else
                                                                                        <select class="form-control" id="sifat" name="sifat">
                                                                                            <option value selected="">== Pilih ==</option>
                                                                                            <option value ="Badan Usaha Milik Desa">Badan Usaha Milik Desa</option>
                                                                                            <option value ="Badan Usaha Milik Desa Bersama">Badan Usaha Milik Desa Bersama</option>
                                                                                        </select>
                                                                                        @endif
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="font-weight-bold">Berita Acara Musyawarah Desa</label>
                                                                                    <input type="hidden" name="old_bac" value="{{ $akunbumdes->bac }}">
                                                                                    <input type="file" class="form-control @error('bac') is-invalid @enderror" name="bac" placeholder="Unggah Dokumen">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="font-weight-bold">Peraturan Desa Pendirian BUMDES</label>
                                                                                    <input type="hidden" name="old_perdes" value="{{ $akunbumdes->perdes }}">
                                                                                    <input type="file" class="form-control @error('perdes') is-invalid @enderror" name="perdes" placeholder="Unggah Dokumen">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="font-weight-bold">Anggaran Rumah Tangga BUMDES</label>
                                                                                    <input type="hidden" name="old_adart" value="{{ $akunbumdes->adart }}">
                                                                                    <input type="file" class="form-control @error('adart') is-invalid @enderror" name="adart" placeholder="Unggah Dokumen">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="font-weight-bold">Rencana Program Kerja BUMDES</label>
                                                                                    <input type="hidden" name="old_rencana_program" value="{{ $akunbumdes->rencana_program }}">
                                                                                    <input type="file" class="form-control @error('rencana_program') is-invalid @enderror" name="rencana_program" placeholder="Unggah Dokumen">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="font-weight-bold">Struktur Organisasi</label>
                                                                                    <input type="hidden" name="old_struktur_organisasi" value="{{ $akunbumdes->struktur_organisasi }}">
                                                                                    <input type="file" class="form-control @error('struktur_organisasi') is-invalid @enderror" name="struktur_organisasi" placeholder="Unggah Dokumen">
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        @else
                                                        <td style="width: 10%"><button type="button" class="btn btn-xs btn-success edit float-left" data-toggle="modal" data-target="#modal-edittidak{{ $akunbumdes->id }}"><i class="fa fa-pencil"></i></button>
                                                            <!-- Modal Edit BUMDES Tidak Terdaftar Kemendes-->
                                                            <div id="modal-edittidak{{ $akunbumdes->id }}" class="modal fade" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <form name="frm_add" id="frm_add" class="form-horizontal" action="/Akunbumdes/update" method="POST" enctype="multipart/form-data">
                                                                        @csrf
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                                <h6 class="modal-title">Edit Data</h6>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="container-fluid">
                                                                                    <div class="row">
                                                                                        <div class="col-sm-12">
                                                                                            <div class="row">
                                                                                                <div class="col-8 col-sm-5">
                                                                                                    <div class="form-group">
                                                                                                        <input type="hidden" name="tahun" value="{{ $tahun }}">
                                                                                                        <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                                                                                                        <input type="hidden" name="id" value="{{ $akunbumdes->id }}">
                                                                                                        <label class="font-weight-bold">Nama Badan Usaha Milik Desa</label>
                                                                                                        <input type="text" class="form-control @error('nama_bumdes') is-invalid @enderror" id="nama_bumdes" name="nama_bumdes" value="{{ $akunbumdes->nama_bumdes }}" placeholder="Masukkan Nama BUMDES" required>
                                                                                                    </div>                      
                                                                                                    <div class="form-group">
                                                                                                        <label class="font-weight-bold">Terdaftar</label>
                                                                                                            @if($akunbumdes->terdaftar == 'Terdaftar Kemendes')
                                                                                                            <select class="form-control" id="terdaftar" name="terdaftar">
                                                                                                                <option value="">== Pilih ==</option>
                                                                                                                <option value selected="Terdaftar Kemendes">Terdaftar Kemendes</option>
                                                                                                                <option value="Belum Terdaftar Kemendes">Belum Terdaftar Kemendes</option>
                                                                                                            </select>
                                                                                                            @elseif($akunbumdes->terdaftar == 'Belum Terdaftar Kemendes')
                                                                                                            <select class="form-control" id="terdaftar" name="terdaftar">
                                                                                                                <option value="">== Pilih ==</option>
                                                                                                                <option value ="Terdaftar Kemendes">Terdaftar Kemendes</option>
                                                                                                                <option value selected="Belum Terdaftar Kemendes">Belum Terdaftar Kemendes</option>
                                                                                                            </select>
                                                                                                            @else
                                                                                                            <select class="form-control" id="terdaftar" name="terdaftar">
                                                                                                                <option value selected="">== Pilih ==</option>
                                                                                                                <option value ="Terdaftar Kemendes">Terdaftar Kemendes</option>
                                                                                                                <option value ="Belum Terdaftar Kemendes">Belum Terdaftar Kemendes</option>
                                                                                                            </select>
                                                                                                            @endif
                                                                                                    </div>
                                                                                                    <div class="form-group">
                                                                                                        <label class="font-weight-bold">Sifat BUMDES</label>
                                                                                                            @if($akunbumdes->sifat == 'Badan Usaha Milik Desa')
                                                                                                            <select class="form-control" id="sifat" name="sifat">
                                                                                                                <option value="">== Pilih ==</option>
                                                                                                                <option value selected="Badan Usaha Milik Desa">Badan Usaha Milik Desa</option>
                                                                                                                <option value="Badan Usaha Milik Desa Bersama">Badan Usaha Milik Desa Bersama</option>
                                                                                                            </select>
                                                                                                            @elseif($akunbumdes->sifat == 'Badan Usaha Milik Desa Bersama')
                                                                                                            <select class="form-control" id="sifat" name="sifat">
                                                                                                                <option value="">== Pilih ==</option>
                                                                                                                <option value ="Badan Usaha Milik Desa">Badan Usaha Milik Desa</option>
                                                                                                                <option value selected="Badan Usaha Milik Desa Bersama">Badan Usaha Milik Desa Bersama</option>
                                                                                                            </select>
                                                                                                            @else
                                                                                                            <select class="form-control" id="sifat" name="sifat">
                                                                                                                <option value selected="">== Pilih ==</option>
                                                                                                                <option value ="Badan Usaha Milik Desa">Badan Usaha Milik Desa</option>
                                                                                                                <option value ="Badan Usaha Milik Desa Bersama">Badan Usaha Milik Desa Bersama</option>
                                                                                                            </select>
                                                                                                            @endif
                                                                                                    </div>
                                                                                                    <div class="form-group">
                                                                                                        <label class="font-weight-bold">Peraturan Desa Pendirian BUMDES</label>
                                                                                                        <input type="hidden" name="old_perdes" value="{{ $akunbumdes->perdes }}">
                                                                                                        <input type="file" class="form-control @error('perdes') is-invalid @enderror" name="perdes" placeholder="Unggah Dokumen">
                                                                                                    </div>
                                                                                                    <div class="form-group">
                                                                                                        <label class="font-weight-bold">SK Kepengurusan Terakhir</label>
                                                                                                        <input type="hidden" name="old_sk_kepengurusan" value="{{ $akunbumdes->sk_kepengurusan }}">
                                                                                                        <input type="file" class="form-control @error('sk_kepengurusan') is-invalid @enderror" name="sk_kepengurusan" placeholder="Unggah Dokumen">
                                                                                                    </div>
                                                                                                    <div class="form-group">
                                                                                                        <label class="font-weight-bold">Buku Rekening Terakhir BUMDES</label>
                                                                                                        <input type="hidden" name="old_buku_rekening" value="{{ $akunbumdes->buku_rekening }}">
                                                                                                        <input type="file" class="form-control @error('buku_rekening') is-invalid @enderror" name="buku_rekening" placeholder="Unggah Dokumen">
                                                                                                    </div>
                                                                                                    <div class="form-group">
                                                                                                        <label class="font-weight-bold">Laporan Keuangan Terakhir BUMDES</label>
                                                                                                        <input type="hidden" name="old_lap_keuangan" value="{{ $akunbumdes->lap_keuangan }}">
                                                                                                        <input type="file" class="form-control @error('lap_keuangan') is-invalid @enderror" name="lap_keuangan" placeholder="Unggah Dokumen">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-4 col-sm-7">
                                                                                                    <div class="form-group">
                                                                                                        <label class="font-weight-bold">Tahun & Anggaran Penyertaan Modal Pertama</label>
                                                                                                        <div class="col-sm-12" style="margin-bottom: 30px"> 
                                                                                                            <div class="col-md-4 col-sm-4  form-group has-feedback">
                                                                                                                <input style="text-align:right" type="text" maxlength="4" class="form-control has-feedback-left @error('modal_pertama') is-invalid @enderror" maxlength="4" name="modal_pertama" value="{{ $akunbumdes->modal_pertama }}">
                                                                                                                <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                                                                                                            </div>
                                                                                                            <div class="col-md-8 col-sm-8  form-group has-feedback">
                                                                                                                <input style="text-align: right" type="text" class="angka has-feedback-left form-control @error('dana_modal_pertama') is-invalid @enderror" name="dana_modal_pertama" value="{{ $akunbumdes->dana_modal_pertama }}">
                                                                                                                <span class="form-control-feedback left" aria-hidden="true">Rp </span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group">
                                                                                                        <label class="font-weight-bold">Tahun & Anggaran Penyertaan Modal Terakhir</label>
                                                                                                        <div class="col-sm-12" style="margin-bottom: 10px">    
                                                                                                            <div class="col-md-4 col-sm-4  form-group has-feedback">
                                                                                                                <input style="text-align:right" type="text" maxlength="4" class="form-control has-feedback-left @error('modal_terakhir') is-invalid @enderror" maxlength="4" name="modal_terakhir" value="{{ $akunbumdes->modal_terakhir }}">
                                                                                                                <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                                                                                                            </div>
                                                                                                            <div class="col-md-8 col-sm-8  form-group has-feedback">
                                                                                                                <input style="text-align: right" type="text" class="angka has-feedback-left form-control @error('dana_modal_terakhir') is-invalid @enderror" name="dana_modal_terakhir" value="{{ $akunbumdes->dana_modal_terakhir }}">
                                                                                                                <span class="form-control-feedback left" aria-hidden="true">Rp </span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{ route('Akunbumdes.destroy', $akunbumdes->id) }}" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-xs btn-danger hapus"><i class="fa fa-trash"></i></button>
                                                        </form> 
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                        <!-- view data Akuntabilitas BUMDES-->
                        <div class="tab-pane fade" id="akuntabilitas" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox-content">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                                <thead>
                                                    <tr>
                                                        <th style="width: 5%">No</th>
                                                        <th style="width: 30%; text-align: center">Nama BUMDES</th>
                                                        <th style="text-align: center">Dokumen</th>
                                                        <th style="text-align: center">Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($Akunbumdesterdaftars as $akunbumdes)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $akunbumdes->nama_bumdes }}</th>
                                                        <td>
                                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Laporan Pengawasan</td>
                                                                        @if($akunbumdes->lap_pengawas)
                                                                        <td style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->lap_pengawas}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Laporan Keuangan</td>
                                                                        @if($akunbumdes->lap_keuangan)
                                                                        <td style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->lap_keuangan}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Laporan Semester-1</td>
                                                                        @if($akunbumdes->lap_sem1)
                                                                        <td style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->lap_sem1}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Laporan Semester-2</td>
                                                                        @if($akunbumdes->lap_sem2)
                                                                        <td style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->lap_sem2}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Laporan Tahunan</td>
                                                                        @if($akunbumdes->lap_tahunan)
                                                                        <td style="text-align: center"><a target="_blank" rel="noopener"  href="storage/{{$akunbumdes->lap_tahunan}}"><i style="font-size: 20px" class="fa-sharp fa-solid fa-download"></i></a></td>
                                                                        @endif
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td style="width: 5%">
                                                            <button type="button" class="btn btn-xs btn-success edit float-left" data-toggle="modal" data-target="#modal-editakun{{ $akunbumdes->id }}"><i class="fa fa-pencil"></i></button>
                                                            <!-- modal edit data Akuntabilitas BUMDES-->
                                                            <div id="modal-editakun{{ $akunbumdes->id }}" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-md">
                                                                    <form name="frm_add" id="frm_add" class="form-horizontal" action="/Akunbumdes/update" method="POST" enctype="multipart/form-data">
                                                                        @csrf
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                                <h6 class="modal-title">Edit Data</h6>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="form-group">
                                                                                    <input type="hidden" name="tahun" value="{{ $tahun }}">
                                                                                    <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                                                                                    <input type="hidden" name="id" value="{{ $akunbumdes->id }}">
                                                                                    <label class="font-weight-bold">Laporan Pengawasan</label>
                                                                                    <input type="hidden" name="old_lap_pengawas" value="{{ $akunbumdes->lap_pengawas }}">
                                                                                    <input type="file" class="form-control @error('lap_pengawas') is-invalid @enderror" name="lap_pengawas" placeholder="Unggah Dokumen">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="font-weight-bold">Laporan Keuangan</label>
                                                                                    <input type="hidden" name="old_lap_keuangan" value="{{ $akunbumdes->lap_keuangan }}">
                                                                                    <input type="file" class="form-control @error('lap_keuangan') is-invalid @enderror" name="lap_keuangan" placeholder="Unggah Dokumen">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="font-weight-bold">Laporan Semester 1</label>
                                                                                    <input type="hidden" name="old_lap_sem1" value="{{ $akunbumdes->lap_sem1 }}">
                                                                                    <input type="file" class="form-control @error('lap_sem1') is-invalid @enderror" name="lap_sem1" placeholder="Unggah Dokumen">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="font-weight-bold">Laporan Semester 2</label>
                                                                                    <input type="hidden" name="old_lap_sem2" value="{{ $akunbumdes->lap_sem2 }}">
                                                                                    <input type="file" class="form-control @error('lap_sem2') is-invalid @enderror" name="lap_sem2" placeholder="Unggah Dokumen">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="font-weight-bold">Laporan Tahunan</label>
                                                                                    <input type="hidden" name="old_lap_tahunan" value="{{ $akunbumdes->lap_tahunan }}">
                                                                                    <input type="file" class="form-control @error('lap_tahunan') is-invalid @enderror" name="lap_tahunan" placeholder="Unggah Dokumen">
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <!-- modal add data umum BUMDES-->
                    <div class="modal inmodal fade" id="modal-add" tabindex="-1" role="dialog" aria-hidden="true" wire:ignore.self>
                        <div class="modal-dialog modal-md">
                            <form name="frm_add" id="frm_add" class="form-horizontal" action="{{ route('Akunbumdes.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title">Tambah Data</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type="hidden" name="tahun" value="{{ $tahun }}">
                                            <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                                            <label class="font-weight-bold">Nama Badan Usaha Milik Desa</label>
                                            <input type="text" class="form-control @error('nama_bumdes') is-invalid @enderror" id="nama_bumdes" name="nama_bumdes" value="{{ old('nama_bumdes') }}" placeholder="Masukkan Nama BUMDES" required>
                                        </div>                      
                                        <div class="form-group">
                                            <label class="font-weight-bold">Sudah Terdaftar di Kemendes</label>
                                            <select class="form-control" id="terdaftar" name="terdaftar" required>
                                                <option value="">== Pilih ==</option>
                                                <option value="Terdaftar Kemendes">Terdaftar Kemendes</option>
                                                <option value="Belum Terdaftar Kemendes">Belum Terdaftar Kemendes</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="font-weight-bold">Sifat BUMDES</label>
                                            <select class="form-control" id="sifat" name="sifat" required>
                                                <option value="">== Pilih ==</option>
                                                <option value="Badan Usaha Milik Desa">Badan Usaha Milik Desa</option>
                                                <option value="Badan Usaha Milik Desa Bersama">Badan Usaha Milik Desa Bersama</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                        <button type="submit" class="btn btn-primary" id="tambah">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            

            
@endsection

@push('script')
<script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script>
    bsCustomFileInput.init();
    $('.jumlah').mask('000.000.000.000.000', {reverse: true});
    $('.angka').mask('000.000.000.000.000', {reverse: true});
</script>
<script>
    $(document).ready(function() {
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
    });
</script>

<script>
document.querySelectorAll('input[type-currency="IDR"]').forEach((element) => {
    element.addEventListener('keyup', function(e) {
    let cursorPostion = this.selectionStart;
      let value = parseInt(this.value.replace(/[^,\d]/g, ''));
      let originalLenght = this.value.length;
      if (isNaN(value)) {
        this.value = "";
      } else {    
        this.value = value.toLocaleString('id-ID', {
          currency: 'IDR',
          style: 'currency',
          minimumFractionDigits: 0
        });
        cursorPostion = this.value.length - originalLenght + cursorPostion;
        this.setSelectionRange(cursorPostion, cursorPostion);
      }
    });
  });
</script>

@if(session()->has('timpaAll'))
<script>
    $('#copyAkunaset').modal('show');
</script>
@endif

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

@endpush