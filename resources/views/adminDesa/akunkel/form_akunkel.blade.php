@extends('templates.desa.main')

@section('content')
<div class="row justify-content-center mt-2 mb-4">
    <div class="col-md-12 col-sm-12  ">
        <h5 class="alert alert-info">Form Input/Update Data Akuntabilitas Kelembagaan</h5>
        <div class="x_panel">

            <div class="x_title">

                <div class="d-flex">
                    <form class="form-inline" action="/adminDesa/formAkunkel" method="get">
                        @csrf
                        <div class="form-group mx-sm-3 mb-2">
                            <h6>Masukkan tahun data :</h6>
                            <input type="text" name="tahun" class="form-control ml-3" placeholder="{{ $tahun }}"
                                data-inputmask="'mask': '9999'" style="font-size: .85rem">
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm mb-2">Cek Data</button>
                    </form>
                </div>
                <hr>
                @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Error !</strong> Harap periksa inputan dan file berupa pdf dengan maksimal yang ditentukan 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>

                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div>Tahun Data : {{ $tahun }} &emsp; &emsp; <span class="text-info">(Form Input Data Akuntabilitas
                        Kelembagaan)</span></div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" role="tab">Kelembagaan

                        </a>
                    </li>

                </ul>

                <div class=" tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <form action="/adminDesa/tambahAkunkel" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                            <input type="hidden" name="tahun" value="{{ $tahun }}">

                            <div class="row akunwil">
                                <div class="col-md-8">
                                    <table class="table table-bordered">
                                        <tr>

                                            <th width="60%">
                                                <div class="form-group">
                                                    <label for="sotk">Peraturan Desa Tentang Susunan Organisasi Tatakerja (SOTK)</label>
                                                    <input type="hidden" name="nama_data[]" value="sotk">
                                                    
                                                </div>

                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_sotk">Upload Dokumen Perdes (SOTK)</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_sotk"
                                                            class="custom-file-input" id="upload_sotk">
                                                        <label class="custom-file-label text-muted upload_sotk"
                                                            for="upload_sotk">Choose
                                                            file PDF
                                                            (max-size: 1MB)</label>
                                                    </div>

                                                </div>

                                            </th>

                                        </tr>

                                        <tr>

                                            <th>
                                                <div class="form-group">
                                                    <label for="sklpm">SK Lembaga Pemberdayaan Masyarakat (LPM)</label>
                                                    <input type="hidden" name="nama_data[]" value="sklpm">
                                                    
                                                </div>

                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_sklpm">Upload SK LPM</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_sklpm"
                                                            class="custom-file-input" id="upload_sklpm">
                                                        <label class="custom-file-label text-muted upload_sklpm"
                                                            for="upload_batas_utara">Choose
                                                            file Image
                                                            (max-size: 1MB)</label>
                                                    </div>

                                                </div>

                                            </th>

                                        </tr>
                                        <tr>

                                            <th>
                                                <div class="form-group">
                                                    <label for="sktaruna">SK Karang Taruna</label>
                                                    <input type="hidden" name="nama_data[]" value="sktaruna">
                                                    
                                                </div>

                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_sktaruna">Upload SK Karang Taruna</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_sktaruna"
                                                            class="custom-file-input" id="upload_sktaruna">
                                                        <label class="custom-file-label text-muted upload_sktaruna"
                                                            for="upload_sktaruna">Choose
                                                            file Image
                                                            (max-size: 1MB)</label>
                                                    </div>

                                                </div>

                                            </th>

                                        </tr>
                                        <tr>

                                            <th>
                                                <div class="form-group">
                                                    <label for="sklinmas">SK Perlindungan Masyarakat (Linmas)</label>
                                                    <input type="hidden" name="nama_data[]" value="sklinmas">
                                                    
                                                </div>

                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_linmas">Upload SK Linmas</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_linmas"
                                                            class="custom-file-input" id="upload_linmas">
                                                        <label class="custom-file-label text-muted upload_linmas"
                                                            for="upload_linmas">Choose
                                                            file Image
                                                            (max-size: 1MB)</label>
                                                    </div>

                                                </div>

                                            </th>

                                        </tr>
                                        <tr>

                                            <th>
                                                <div class="form-group">
                                                    <label for="kantor_desa">Keberadaan & Keberfungsian Kantor Desa</label>
                                                    <input type="hidden" name="nama_data[]" value="kantor_desa">
                                                    
                                                    <input type="hidden" name="nama_data[]" value="kg_kdes">
                                                    
                                                </div>

                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_kantor_desa">Upload Tampak Depan Kantor Desa</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_kantor_desa"
                                                            class="custom-file-input" id="upload_kantor_desa">
                                                        <label class="custom-file-label text-muted upload_kantor_desa"
                                                            for="upload_kantor_desa">Choose
                                                            file Image
                                                            (max-size: 1MB)</label>
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <label for="upload_kg_kdes">Upload Kegiatan di Kantor Desa</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_kg_kdes"
                                                            class="custom-file-input" id="upload_kg_kdes">
                                                        <label class="custom-file-label text-muted upload_kg_kdes"
                                                            for="upload_kg_kdes">Choose
                                                            file Image
                                                            (max-size: 1MB)</label>
                                                    </div>

                                                </div>

                                            </th>

                                        </tr>
                                        <tr>
                                            <th>
                                                <div class="form-group">
                                                    <label for="kantor_bpd">Sekretariat / Kantor BPD</label>
                                                    <input type="hidden" name="nama_data[]" value="kantor_bpd">
                                                    
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_kantor_bpd">Upload Sekretariat / Kantor BPD</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_kantor_bpd"
                                                            class="custom-file-input" id="upload_kantor_bpd">
                                                        <label class="custom-file-label text-muted upload_kantor_bpd"
                                                            for="upload_kantor_bpd">Choose
                                                            file Image
                                                            (max-size: 5MB)</label>
                                                    </div>
                                            </th>

                                        </tr>
                                        <tr>
                                            <th>
                                                <div class="form-group">
                                                    <label for="kantor_lpm">Sekretariat / Kantor LPM</label>
                                                    <input type="hidden" name="nama_data[]" value="kantor_lpm">
                                                    
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_kantor_lpm">Upload Sekretariat / Kantor LPM</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_kantor_lpm"
                                                            class="custom-file-input" id="upload_kantor_lpm">
                                                        <label class="custom-file-label text-muted upload_kantor_lpm"
                                                            for="upload_kantor_lpm">Choose
                                                            file Image
                                                            (max-size: 5MB)</label>
                                                    </div>
                                            </th>

                                        </tr>


                                    </table>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group row justify-content-center">
                                <div class="col-md-7 col-sm-7">
                                    <button type="button" class="btn btn-primary">Cancel</button>
                                    <button type="reset" class="btn btn-primary">Reset</button>
                                    <button type="submit" class="btn btn-success">Kirim Data</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <br><br><br>
        </div>
    </div>
</div>
<br>
<br>
@endsection
@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bs-custom-file-input/1.1.1/bs-custom-file-input.min.js" integrity="sha512-LGq7YhCBCj/oBzHKu2XcPdDdYj6rA0G6KV0tCuCImTOeZOV/2iPOqEe5aSSnwviaxcm750Z8AQcAk9rouKtVSg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script>
    bsCustomFileInput.init();
</script>
<script src="/js/akunwil.js"></script>
@endpush