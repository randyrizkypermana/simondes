@extends('templates.desa.main')

@section('content')
<div class="row justify-content-center mt-2 mb-4">
    <div class="col-md-12 col-sm-12  ">
        <h5 class="alert alert-info">Form Input/Update Data Akuntabilitas Administrasi Umum</h5>
        <div class="x_panel">

            <div class="x_title">

                <div class="d-flex">
                    <form class="form-inline" action="/adminDesa/formAkunadum" method="get">
                        @csrf
                        <div class="form-group mx-sm-3 mb-2">
                            <h6>Masukkan tahun data :</h6>
                            <input type="text" name="tahun" class="form-control ml-3" placeholder="{{ $tahun }}"
                                data-inputmask="'mask': '9999'" style="font-size: .85rem">
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm mb-2">Cek Data</button>
                    </form>
                </div>
                <hr>
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Error !</strong> Harap periksa inputan dan file berupa pdf dengan maksimal yang ditentukan 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                @endif
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>

                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div>Tahun Data : {{ $tahun }} &emsp; &emsp; <span class="text-info">(Form Input Data Akuntabilitas
                        Administrasi Umum)</span></div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" role="tab">Administrasi Umum

                        </a>
                    </li>

                </ul>

                <div class=" tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <form action="/adminDesa/tambahAkunadum" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
                            <input type="hidden" name="tahun" value="{{ $tahun }}">

                            <div class="row akunwil">
                                <div class="col-md-8">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="60%">
                                                <div class="form-group">
                                                    <label for="surat">Buku Surat Masuk / Keluar</label>
                                                    <input type="hidden" name="nama_data[]" value="surat">
                                                    
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_surat">Upload Dokumen Surat Masuk / Keluar</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_surat"
                                                            class="custom-file-input" id="upload_surat">
                                                        <label class="custom-file-label text-muted upload_surat"
                                                            for="upload_surat">Choose
                                                            file PDF
                                                            (max-size: 20MB)</label>
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>
                                                <div class="form-group">
                                                    <label for="daftar_hadir">Daftar Hadir Perangkat Semester 1</label>
                                                    <input type="hidden" name="nama_data[]" value="daftar_hadir">        
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_daftar_hadir">Upload Daftar Hadir Perangkat</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_daftar_hadir"
                                                            class="custom-file-input" id="upload_daftar_hadir">
                                                        <label class="custom-file-label text-muted upload_daftar_hadir"
                                                            for="upload_daftar_hadir">Choose
                                                            file Image
                                                            (max-size: 20MB)</label>
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>
                                                <div class="form-group">
                                                    <label for="daftar_hadir">Daftar Hadir Perangkat Semester 2</label>
                                                    <input type="hidden" name="nama_data[]" value="daftar_hadir2">        
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_daftar_hadir2">Upload Daftar Hadir Perangkat</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_daftar_hadir2"
                                                            class="custom-file-input" id="upload_daftar_hadir2">
                                                        <label class="custom-file-label text-muted upload_daftar_hadir2"
                                                            for="upload_daftar_hadir2">Choose
                                                            file Image
                                                            (max-size: 20MB)</label>
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>
                                                <div class="form-group">
                                                    <label for="buku_register">Buku Register Peraturan Desa, Peraturan Kepala Desa & Surat Keputusan</label>
                                                    <input type="hidden" name="nama_data[]" value="buku_register">
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_buku_register">Upload Buku Register</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_buku_register"
                                                            class="custom-file-input" id="upload_buku_register">
                                                        <label class="custom-file-label text-muted upload_buku_register"
                                                            for="upload_buku_register">Choose
                                                            file Image
                                                            (max-size: 10MB)</label>
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>
                                                <div class="form-group">
                                                    <label for="rekap_penduduk">Buku Rekap Kependudukan</label>
                                                    <input type="hidden" name="nama_data[]" value="rekap_penduduk">
                                                    
                                                </div>

                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    <label for="upload_rekap_penduduk">Upload Dokumen Rekap Kependudukan</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="upload_rekap_penduduk"
                                                            class="custom-file-input" id="upload_rekap_penduduk">
                                                        <label class="custom-file-label text-muted upload_rekap_penduduk"
                                                            for="upload_rekap_penduduk">Choose
                                                            file Image
                                                            (max-size: 1MB)</label>
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group row justify-content-center">
                                <div class="col-md-7 col-sm-7">
                                    <button type="button" class="btn btn-primary">Cancel</button>
                                    <button type="reset" class="btn btn-primary">Reset</button>
                                    <button type="submit" class="btn btn-success">Kirim Data</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <br><br><br>
        </div>
    </div>
</div>
<br>
<br>
@endsection
@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bs-custom-file-input/1.1.1/bs-custom-file-input.min.js" integrity="sha512-LGq7YhCBCj/oBzHKu2XcPdDdYj6rA0G6KV0tCuCImTOeZOV/2iPOqEe5aSSnwviaxcm750Z8AQcAk9rouKtVSg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script>
    bsCustomFileInput.init();
</script>
<script src="/js/akunwil.js"></script>
@endpush