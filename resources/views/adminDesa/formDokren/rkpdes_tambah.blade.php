<form action="/adminDesa/tambahDokren" method="post" enctype="multipart/form-data"
    class="form-horizontal form-label-left">
    @csrf

    <input type="hidden" name="asal_id" value="{{ $infos->asal_id }}">
    <input type="hidden" name="tahun" value="{{ $tahun }}">
    <input type="hidden" name="nama_dokren" value="{{ $nama_dokren }}">
    <div class="form-group row">
        <div class="col-md-7">
            <h6 class="alert alert-primary">Form Input Data RKP Desa tahun {{ $tahun }} </h6>
        </div>
    </div>

    <div class="form-group row">

        <label class="control-label col-md-2 col-sm-2 py-0">Upload SK Tim Penyusun RKP
            Desa
            <input type="hidden" name="nama_data[]" value="sk_tim_rkpdes">
            <input type="hidden" name="isidata[]">

        </label>
        <div class="col-md-5 col-sm-5 ">

            @error('sk_tim')
            <small class="text-danger">{{ $message }}</small>
            @enderror

            <div class="input-group">
                <div class="custom-file py-0">
                    <input type="file" name="sk_tim_rkpdes" class="custom-file-input" id="sk_tim_rkpdes">
                    <label class="custom-file-label text-muted sk_tim_rkpdes" for="file_sk">Choose
                        file PDF
                        (max-size: 1MB)</label>
                </div>
            </div>


        </div>
    </div>
    <div class="form-group row">
        <label class="control-label col-md-2 col-sm-2 py-0">Upload BAC Musrenbangdes
        </label>
        <input type="hidden" name="nama_data[]" value="bac_musrenbangdes">
        <input type="hidden" name="isidata[]">
        <div class="col-md-5 col-sm-5 ">
            <div class="input-group mb-3">
                <div class="custom-file py-0">
                    <input type="file" name="bac_musrenbangdes" class="custom-file-input" id="bac_musrenbangdes">
                    <label class="custom-file-label text-muted bac_musrenbangdes" for="bac_musrenbangdes">Choose
                        file PDF
                        (max-size: 1MB)</label>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="control-label col-md-2 col-sm-2 py-0">Upload Daftar hadir
            Musrenbangdes</label>
        <input type="hidden" name="nama_data[]" value="daftar_hadir_musrenbangdes">
        <input type="hidden" name="isidata[]">
        <div class="col-md-5 col-sm-5 ">
            <div class="input-group mb-3">
                <div class="custom-file py-0">
                    <input type="file" name="daftar_hadir_musrenbangdes" class="custom-file-input"
                        id="daftar_hadir_musrenbangdes">
                    <label class="custom-file-label text-muted daftar_hadir_musrenbangdes"
                        for="daftar_hadir_musrenbangdes">Choose
                        file PDF
                        (max-size: 1MB)</label>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="control-label col-md-2 col-sm-2 py-0">Upload dokumentasi/foto
            Musrenbangdes</label>
        <input type="hidden" name="nama_data[]" value="foto_musrenbangdes">
        <input type="hidden" name="isidata[]">
        <div class="col-md-5 col-sm-5 ">
            @error('foto_musrenbangdes')
            <small class="text-danger">{{ $message }}</small>
            @enderror
            <div class="input-group mb-3">
                <div class="custom-file py-0">
                    <input type="file" name="foto_musrenbangdes" class="custom-file-input" id="foto_musrenbangdes">
                    <label class="custom-file-label text-muted foto_musrenbangdes" for="foto_musrenbangdes">Choose
                        file Image
                        (max-size: 1MB)</label>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="control-label col-md-2 col-sm-2 ">Perdes RKP Desa Th {{ $tahun }}</label>

        <div class="col-md-5 col-sm-5 ">
            <span class="input-group-text" style="font-size: .85rem; border-radius: 0;">Nomor
                : <input type="text" class="form-control ml-2" name="isidata[]" style="font-size: .85rem"
                    required></span>
            <input type="hidden" name="nama_data[]" value="nomor_rkpdes">

        </div>
    </div>
    <div class="form-group row">
        <label class="control-label col-md-2 col-sm-2 ">Tanggal Penetapan Perdes </label>

        <div class="col-md-5 col-sm-5 ">
            <input type="text" class="form-control " name="isidata[]" style="font-size: .85rem"
                data-inputmask="'mask': '99/99/9999'" required>
            <input type="hidden" name="nama_data[]" value="tanggal_penetapan_rkpdes">

        </div>
    </div>
    <div class="form-group row">
        <label class="control-label col-md-2 col-sm-2 ">Upload RKP Desa Th {{ $tahun }}</label>
        <input type="hidden" name="nama_data[]" value="dokumen_rkpdes">
        <input type="hidden" name="isidata[]">
        <div class="col-md-5 col-sm-5 ">
            @error('dokumen_rkpdes')
            <small class="text-danger">{{ $message }}</small>
            @enderror
            <div class="input-group mb-3">
                <div class="custom-file">
                    <input type="file" name="dokumen_rkpdes" class="custom-file-input" id="dokumen_rkpdes" required>
                    <label class="custom-file-label text-muted dokumen_rkpdes" for="dokumen_rkpdes">Choose
                        file PDF
                        (max-size: 20MB)</label>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label class="control-label col-md-2 col-sm-3 ">Jumlah Pagu Indikatif <br /> RKP Desa Th
            {{
            $tahun }}</label>
        <div class="col-md-5 col-sm-5 ">
            <div class="form-group row">
                <label class="control-label col-md-5 col-sm-5 ">Pagu Indikatif
                    Pendapatan</label>
                <input type="hidden" name="nama_data[]" value="pagu_pendapatan">
                <div class="col-md-7 d-inline-flex ">
                    <span class="input-group-text border-right-0"
                        style="font-size: .85rem; border-radius: 0;">Rp.</span>
                    <input type="text" class="form-control pendapatan" name="isidata[]"></input>

                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-md-5 col-sm-5 ">Pagu Indikatif Belanja</label>
                <input type="hidden" name="nama_data[]" value="pagu_belanja">
                <div class="col-md-7 d-inline-flex ">
                    <span class="input-group-text border-right-0"
                        style="font-size: .85rem; border-radius: 0;">Rp.</span>
                    <input type="text" class="form-control pendapatan" name="isidata[]"></input>

                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-md-5 col-sm-5 ">Pagu Indikatif
                    Pembiayaan</label>
                <input type="hidden" name="nama_data[]" value="pagu_pembiayaan">
                <div class="col-md-7 d-inline-flex ">
                    <span class="input-group-text border-right-0"
                        style="font-size: .85rem; border-radius: 0;">Rp.</span>
                    <input type="text" class="form-control pendapatan" name="isidata[]"></input>

                </div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-2">
            Jumlah Total Kegiatan dalam <br>RKP Desa Th {{ $tahun }}
            <input type="hidden" name="nama_data[]" value="jumlah_total_kegiatan">
        </div>
        <div class="col-md-2">
            <div class="input-group ">
                <input type="number" name="isidata[]" class="form-control" style="font-size: .85rem; border-radius: 0;">
                <div class="input-group-append">
                    <span class="input-group-text" style="font-size: .85rem; border-radius: 0;">kegiatan</span>
                </div>
            </div>
        </div>


    </div>
    <div class="form-group row">
        <div class="col-md-2">
            Jumlah Rencana Kegiatan Pembangunan Fisik/Infrastruktur
            <input type="hidden" name="nama_data[]" value="jumlah_kegiatan_fisik">

        </div>
        <div class="col-md-2">
            <div class="input-group ">
                <input type="number" name="isidata[]" class="form-control" style="font-size: .85rem; border-radius: 0;">
                <div class="input-group-append">
                    <span class="input-group-text" style="font-size: .85rem; border-radius: 0;">kegiatan</span>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="form-group row">
        <div class="col-md-2">
            Jumlah Penerima BLT DD Tahun {{ $tahun }}
            <input type="hidden" name="nama_data[]" value="jumlah_penerima_bltdd">
        </div>
        <div class="col-md-2">
            <div class="input-group ">
                <input type="number" name="isidata[]" class="form-control" style="font-size: .85rem; border-radius: 0;">
                <div class="input-group-append">
                    <span class="input-group-text" style="font-size: .85rem; border-radius: 0;">KPM</span>
                </div>
            </div>
        </div>
    </div> --}}
    {{-- <div class="form-group row">
        <label class="control-label col-md-2 col-sm-2 ">Upload BAC Musdes Khusus BLT DD Th {{ $tahun }}</label>
        <input type="hidden" name="nama_data[]" value="dokumen_rkpdes">
        <input type="hidden" name="isidata[]">
        <div class="col-md-5 col-sm-5 ">
            @error('dokumen_rkpdes')
            <small class="text-danger">{{ $message }}</small>
            @enderror
            <div class="input-group mb-3">
                <div class="custom-file">
                    <input type="file" name="dokumen_rkpdes" class="custom-file-input" id="dokumen_rkpdes" required>
                    <label class="custom-file-label text-muted dokumen_rkpdes" for="dokumen_rkpdes">Choose
                        file PDF
                        (max-size: 20MB)</label>
                </div>
            </div>
        </div>
    </div> --}}

    </div>
    </div>

    <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-5 col-sm-5  offset-md-2">
            <button type="button" class="btn btn-primary">Cancel</button>
            <button type="reset" class="btn btn-primary">Reset</button>
            <button type="submit" class="btn btn-success">Kirim Data</button>
        </div>
    </div>

</form>