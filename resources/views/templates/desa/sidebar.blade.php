<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        {{-- <h3>General</h3> --}}
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> BERANDA <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="/progress">Progress Input Data</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-book"></i> INPUT DATA UMUM <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="/adminDesa/formKewilayahan">Data Monografi</a></li>
                    <li><a href="/adminDesa/formPerangkat">Data Perangkat</a></li>
                    <li><a href="/adminDesa/formDusun">Data Kadus & RT</a></li>
                    <li><a href="/adminDesa/formBPD">Data BPD</a></li>
                </ul>
            </li>
            <li><a style="font-size: .65rem"><i class="fa fa-book"></i>AKUNTABILITAS PEMDES <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="/adminDesa/formAkunwil">Kewilayahan</a></li>
                    <li><a>Dokumen Perencanaan<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li class="sub_menu"><a href="/adminDesa/formRpjmd">RPJMDes</a>
                            </li>
                            <li><a href="/adminDesa/formDokren">RKPDes</a>
                            </li>

                        </ul>
                    </li>
                    <li><a href="/adminDesa/formAkunkel">Kelembagaan</a></li>
                    <li><a href="/adminDesa/formAkunadum">Administrasi Umum</a></li>
                </ul>
            </li>
            <li><a style="font-size: .65rem"><i class="fa fa-book"></i>AKUNTABILITAS KEUDES <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="/adminDesa/formApbdes">Data APBDes</a></li>
                    <li><a href="/adminDesa/formApbdesP">Data APBDes Perubahan</a></li>
                    <li><a href="/adminDesa/formPenataanPendapatan">Penata Usahaan Pendapatan</a></li>
                    <li><a href="/adminDesa/formPenataanBelanja">Penata Usahaan Belanja</a></li>
                    <li><a href="/adminDesa/formPenataanPajak">Penata Usahaan Pajak</a></li>
                    <li><a href="/barjas">Pengadaan Barang dan Jasa</a></li>
                    <li><a>Penataan Aset Desa<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li class="sub_menu"><a href="/Akunasetpenggunaan">Buku Inventarisasi Aset Desa</a></li>
                            <li><a href="/Akunasetkiba">Kartu Inventaris Barang</a></li>
                            <li><a href="/Akunasetkir">Kartu Inventaris Ruangan</a></li>
                            <li><a href="/Akunasetholder">Surat Kuasa Pemegang Barang (Holder)</a></li>
                        </ul>
                    </li>
                    <li><a href="/Akunbumdes">Penataan BumDes</a></li>
                </ul>
            </li>

        </ul>
    </div>

</div>
<!-- /sidebar menu -->