@extends('adminSimondes.templates.main')

@section('content')

<section role="main" class="content-body">
    <header class="page-header">
        <h2>Pembagian Wilayah &emsp;<a class="modal-with-form btn btn-primary btn-sm" href="#tahun"><i class="fa fa-calendar"></i> Tahun {{ $tahuns }}</a></h2>
        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="/irban">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>

                <li><span>Beranda</span></li>

                <li>&nbsp;</li>
            </ol>            
        </div>
    </header>

    <div class="row">
        <div class="col">
            <section class="card">                
                <div class="card-body">
                    <div class="tabs">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="nav-item active" style="width: 200px; margin-right: 3px;">
                                <a class="nav-link" data-bs-target="#irban1" href="#irban1" data-bs-toggle="tab">Irban Wilayah 1</a>
                            </li>
                            <li class="nav-item" style="width: 200px; margin-right: 3px;">
                                <a class="nav-link" data-bs-target="#irban2" href="#irban2" data-bs-toggle="tab">Irban Wilayah 2</a>
                            </li>
                            <li class="nav-item" style="width: 200px; margin-right: 3px;">
                                <a class="nav-link" data-bs-target="#irban3" href="#irban3" data-bs-toggle="tab">Irban Wilayah 3</a>
                            </li>
                            <li class="nav-item" style="width: 200px;">
                                <a class="nav-link" data-bs-target="#irban4" href="#irban4" data-bs-toggle="tab">Irban Wilayah 4</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="irban1" class="tab-pane active">
                                
                                <h4 class="text-4 font-weight-semibold text-center mb-4" style="text-decoration: underline;"><a class="modal-with-form btn btn-primary btn-sm pull-left" title="Tambah Obrik Irban Wilayah 1" href="#addirban1"><i class="fa fa-plus-circle"></i> Tambah Obrik</a> Irban Wilayah 1</h4>                                                                
                                <table class="table table-bordered table-striped mb-0" id="datatable1">
                                    <thead>
                                        <tr>
                                            <th style="width: 3%">No.</th>
                                            <th style="width: 30%">Kecamatan</th>
                                            <th style="width: 30%">Tahun</th>
                                            <th style="text-align: center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($obrik1 as $ob1)
                                        <tr>                                
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $ob1->kecamatan }}</td>
                                            <td>{{ $ob1->tahun }}</td>                                            
                                            <td style="width:10%; white-space: nowrap; text-align: center;">
                                                <a data-id="/admin/deleteobrik/{{ $ob1->id }}" class="btn btn-danger btn-sm btn-delete" title="Hapus Data Obrik Irban Wilayah 1"><i class="fa fa-trash"></i></a>
                                            </td>                                
                                        </tr>
                                        @empty
                                        <tr>                                
                                            <td colspan="4" style="text-align: center;">Data belum tersedia</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>


                            </div>
                            <div id="irban2" class="tab-pane">
                                
                                <h4 class="text-4 font-weight-semibold text-center mb-4" style="text-decoration: underline;"><a class="modal-with-form btn btn-primary btn-sm pull-left" title="Tambah Obrik Irban Wilayah 2" href="#addirban2"><i class="fa fa-plus-circle"></i> Tambah Obrik</a> Irban Wilayah 2</h4>                                
                                <table class="table table-bordered table-striped mb-0" id="datatable2">
                                    <thead>
                                        <tr>
                                            <th style="width: 3%">No.</th>
                                            <th style="width: 30%">Kecamatan</th>
                                            <th style="width: 30%">Tahun</th>
                                            <th style="text-align: center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($obrik2 as $ob2)
                                        <tr>                                
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $ob2->kecamatan }}</td>
                                            <td>{{ $ob2->tahun }}</td>                                            
                                            <td style="width:10%; white-space: nowrap; text-align: center;">
                                                <a data-id="/admin/deleteobrik/{{ $ob2->id }}" class="btn btn-danger btn-sm btn-delete" title="Hapus Data Obrik Irban Wilayah 2"><i class="fa fa-trash"></i></a>
                                            </td>                                
                                        </tr>
                                        @empty
                                        <tr>                                
                                            <td colspan="4" style="text-align: center;">Data belum tersedia</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>

                            </div>

                            <div id="irban3" class="tab-pane">
                                
                                <h4 class="text-4 font-weight-semibold text-center mb-4" style="text-decoration: underline;"><a class="modal-with-form btn btn-primary btn-sm pull-left" title="Tambah Obrik Irban Wilayah 3" href="#addirban3"><i class="fa fa-plus-circle"></i> Tambah Obrik</a> Irban Wilayah 3</h4>                                
                                <table class="table table-bordered table-striped mb-0" id="datatable3">
                                    <thead>
                                        <tr>
                                            <th style="width: 3%">No.</th>
                                            <th style="width: 30%">Kecamatan</th>
                                            <th style="width: 30%">Tahun</th>
                                            <th style="text-align: center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($obrik3 as $ob3)
                                        <tr>                                
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $ob3->kecamatan }}</td>
                                            <td>{{ $ob3->tahun }}</td>                                            
                                            <td style="width:10%; white-space: nowrap; text-align: center;">
                                                <a data-id="/admin/deleteobrik/{{ $ob3->id }}" class="btn btn-danger btn-sm btn-delete" title="Hapus Data Obrik Irban Wilayah 3"><i class="fa fa-trash"></i></a>
                                            </td>                                
                                        </tr>
                                        @empty
                                        <tr>                                
                                            <td colspan="4" style="text-align: center;">Data belum tersedia</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>

                            </div>

                            <div id="irban4" class="tab-pane">
                                
                                <h4 class="text-4 font-weight-semibold text-center mb-4" style="text-decoration: underline;"><a class="modal-with-form btn btn-primary btn-sm pull-left" title="Tambah Obrik Irban Wilayah 4" href="#addirban4"><i class="fa fa-plus-circle"></i> Tambah Obrik</a> Irban Wilayah 4</h4>
                                <table class="table table-bordered table-striped mb-0" id="datatable4">
                                    <thead>
                                        <tr>
                                            <th style="width: 3%">No.</th>
                                            <th style="width: 30%">Kecamatan</th>
                                            <th style="width: 30%">Tahun</th>
                                            <th style="text-align: center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($obrik4 as $ob4)
                                        <tr>                                
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $ob4->kecamatan }}</td>
                                            <td>{{ $ob4->tahun }}</td>                                            
                                            <td style="width:10%; white-space: nowrap; text-align: center;">
                                                <a data-id="/admin/deleteobrik/{{ $ob4->id }}" class="btn btn-danger btn-sm btn-delete" title="Hapus Data Obrik Irban Wilayah 4"><i class="fa fa-trash"></i></a>
                                            </td>                                
                                        </tr>
                                        @empty
                                        <tr>                                
                                            <td colspan="4" style="text-align: center;">Data belum tersedia</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </div>                    
                </div>
            </section>
        </div>
    </div>
</section>


<div id="tahun" class="modal-block modal-header-color modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Pilih Tahun PKPT</h2>
        </header>
        <div class="card-body">
            <form action="/admin/pilihtahun" method="get">
                @csrf
                <div class="form-group">
                    <label class="pb-2">Inspektorat Kabupaten Lampung Utara</label>
                    <select data-plugin-selectTwo class="form-control populate" name="tahun">
                        <option value="" selected disabled>-- Pilih Tahun --</option>    
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                    </select>
                </div>                           
        </div>
        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-end">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button class="btn btn-default modal-dismiss">Batal</button>
                </div>
            </div>
        </footer>
    </form>
    </section>
</div>


<div id="addirban1" class="modal-block modal-header-color modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Tambah Obrik Irban Wilayah 1</h2>
        </header>
        <div class="card-body">                                                        
            <form action="/admin/addobrik" method="POST" class="form-horizontal mb-lg">
            @csrf                                                                             
            <input type="hidden" name="obrik" value="irban1">
            <div class="form-group">                        
                <label class="pb-2">Tahun</label>
                <input type="text" class="form-control" name="tahun"  value="{{ $tahuns }}" readonly>
            </div>                                          
            <div class="form-group">                        
                <label class="pb-2">Kecamatan</label>                        
                <select data-plugin-selectTwo class="form-control populate" name="kecamatan">
                    <option value="" selected disabled>-- Pilih Kecamatan --</option>    
                    @foreach($kecamatans as $kc)
                    <option value="{{ $kc->kecamatan }}">{{ $kc->kecamatan }}</option>
                    @endforeach
                </select>
            </div>                                                                                                                                        
        </div>            
        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-end">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button class="btn btn-default modal-dismiss">Batal</button>
                </div>
            </div>
        </footer>
        </form>
    </section>
</div>

<div id="addirban2" class="modal-block modal-header-color modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Tambah Obrik Irban Wilayah 2</h2>
        </header>
        <div class="card-body">                                                        
            <form action="/admin/addobrik" method="POST" class="form-horizontal mb-lg">
            @csrf                                                                             
            <input type="hidden" name="obrik" value="irban2">
            <div class="form-group">                        
                <label class="pb-2">Tahun</label>
                <input type="text" class="form-control" name="tahun"  value="{{ $tahuns }}" readonly>
            </div>                                          
            <div class="form-group">                        
                <label class="pb-2">Kecamatan</label>                        
                <select data-plugin-selectTwo class="form-control populate" name="kecamatan">
                    <option value="" selected disabled>-- Pilih Kecamatan --</option>    
                    @foreach($kecamatans as $kc)
                    <option value="{{ $kc->kecamatan }}">{{ $kc->kecamatan }}</option>
                    @endforeach
                </select>
            </div>                                                                                                                                        
        </div>            
        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-end">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button class="btn btn-default modal-dismiss">Batal</button>
                </div>
            </div>
        </footer>
        </form>
    </section>
</div>

<div id="addirban3" class="modal-block modal-header-color modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Tambah Obrik Irban Wilayah 3</h2>
        </header>
        <div class="card-body">                                                        
            <form action="/admin/addobrik" method="POST" class="form-horizontal mb-lg">
            @csrf                                                                             
            <input type="hidden" name="obrik" value="irban3">
            <div class="form-group">                        
                <label class="pb-2">Tahun</label>
                <input type="text" class="form-control" name="tahun"  value="{{ $tahuns }}" readonly>
            </div>                                          
            <div class="form-group">                        
                <label class="pb-2">Kecamatan</label>                        
                <select data-plugin-selectTwo class="form-control populate" name="kecamatan">
                    <option value="" selected disabled>-- Pilih Kecamatan --</option>    
                    @foreach($kecamatans as $kc)
                    <option value="{{ $kc->kecamatan }}">{{ $kc->kecamatan }}</option>
                    @endforeach
                </select>
            </div>                                                                                                                                        
        </div>            
        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-end">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button class="btn btn-default modal-dismiss">Batal</button>
                </div>
            </div>
        </footer>
        </form>
    </section>
</div>

<div id="addirban4" class="modal-block modal-header-color modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Tambah Obrik Irban Wilayah 4</h2>
        </header>
        <div class="card-body">                                                        
            <form action="/admin/addobrik" method="POST" class="form-horizontal mb-lg">
            @csrf                                                                             
            <input type="hidden" name="obrik" value="irban4">
            <div class="form-group">                        
                <label class="pb-2">Tahun</label>
                <input type="text" class="form-control" name="tahun"  value="{{ $tahuns }}" readonly>
            </div>                                          
            <div class="form-group">                        
                <label class="pb-2">Kecamatan</label>                        
                <select data-plugin-selectTwo class="form-control populate" name="kecamatan">
                    <option value="" selected disabled>-- Pilih Kecamatan --</option>    
                    @foreach($kecamatans as $kc)
                    <option value="{{ $kc->kecamatan }}">{{ $kc->kecamatan }}</option>
                    @endforeach
                </select>
            </div>                                                                                                                                        
        </div>            
        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-end">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button class="btn btn-default modal-dismiss">Batal</button>
                </div>
            </div>
        </footer>
        </form>
    </section>
</div>


@foreach($kecamatans as $kec)
<div id="ubah{{ $kec->kecamatan }}" class="modal-block modal-header-color modal-block-success mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Ubah Obrik Wilayah {{ $tahuns }}</h2>
        </header>
        <div class="card-body">
            <form action="/admin/wilayah_update/{{ $kec->kecamatan }}" method="POST">
                @csrf
                <input type="text" name="kecamatan" value="{{ $kec->kecamatan }}" disabled>
                <div class="form-group">
                    <label>Irban</label>
                    <select data-plugin-selectTwo class="form-control populate" name="obrik">
                        <option value="" selected disabled>-- Pilih Irban --</option>    
                        <option value="irban1">Irban Wilayah 1</option>
                        <option value="irban2">Irban Wilayah 2</option>
                        <option value="irban3">Irban Wilayah 3</option>
                        <option value="irban4">Irban Wilayah 4</option>
                    </select>
                </div>                         
        </div>
        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-end">
                    <button type="submit" class="btn btn-success">Ubah</button>
                    <button class="btn btn-default modal-dismiss">Batal</button>
                </div>
            </div>
        </footer>
    </form>
    </section>
</div>
@endforeach

<script>
    $('#myTab a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

    // store the currently selected tab in the hash value
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });

    // on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
    $('#myTab a[href="' + hash + '"]').tab('show');
</script>

@endsection