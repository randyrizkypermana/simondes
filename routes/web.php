<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\AdminDesaController;
use App\Http\Controllers\PerangkatController;
use App\Http\Controllers\EditorController;
use App\Http\Controllers\BeritaController;
use App\Http\Controllers\ApbdesController;
use App\Http\Controllers\GaleriController;
use App\Http\Controllers\ViewController;
use App\Http\Controllers\DatumController;
use App\Http\Controllers\PeraturanController;
use App\Http\Controllers\PengumumanController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\KeudesController;
use App\Http\Controllers\PenataanasetController;
use App\Http\Controllers\BumdesController;
use App\Http\Controllers\AdmpemController;
use App\Http\Controllers\RegulasiperangkatController;
use App\Http\Controllers\DataperangkatController;
use App\Http\Controllers\DokrenController;

use App\Http\Controllers\RTController;
use App\Http\Controllers\DusunController;
use App\Http\Controllers\BpdController;
use App\Http\Controllers\RpjmdController;

use App\Http\Controllers\AsetController;
use App\Http\Controllers\AkunkelController;
use App\Http\Controllers\AkunadumController;
use App\Http\Controllers\AkunwilController;
use App\Http\Controllers\AkunasetkibController;
use App\Http\Controllers\AkunasetkibaController;
use App\Http\Controllers\AkunasetpenggunaanController;
use App\Http\Controllers\AkunasetkirController;
use App\Http\Controllers\AkunasetbiaController;
use App\Http\Controllers\AkunasetholderController;
use App\Http\Controllers\AkunbumdesController;
use App\Http\Controllers\BarjasController;
use App\Http\Controllers\AdminSimondesController;
use App\Http\Controllers\ApbdesPerubahanController;
use App\Http\Controllers\PenataanPendapatanController;
use App\Http\Controllers\PenataanBelanjaController;
use App\Http\Controllers\PenataanPajakController;
use App\Http\Controllers\IrbanController;

use App\Models\Admin;
use App\Models\Berita;
use App\Models\Galeri;
use App\Models\Asal;
use App\Models\Peraturan;
use App\Models\Pengumuman;
use App\Models\Rpjmd;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Page Homepage
Route::get('/', function () {
    $blog = Berita::latest()->paginate(4);
    $peraturan = Peraturan::orderBy('no_urut')->get();
    $pengumuman = Pengumuman::latest()->paginate(6);
    $galeri = Galeri::all();
    $info = Admin::where('id', session('loggedAdmin'))->first();
    return view('beranda.utama', [
        'blogs' => $blog,
        'peraturans' => $peraturan,
        'pengumumans' => $pengumuman,
        'galeris' => $galeri,
        'infos' => $info
    ]);
});
// page berita
Route::get('/allberita', [ViewController::class, 'allberita']);
Route::get('/allpengumuman', [ViewController::class, 'allpengumuman']);

// Pege Pengumuman
Route::get('/view/{id}', [ViewController::class, 'lihat']);
Route::get('/viewperaturan/{id}', [ViewController::class, 'lihatperaturan']);
Route::get('/viewpengumuman/{id}', [ViewController::class, 'lihatpengumuman']);

//Page Regulasi Pemerintahan & Keudes
Route::get('/regulasi', function () {
    $peraturan = Peraturan::orderBy('no_urut')->get();
    return view('beranda.regulasi', [
        'peraturans' => $peraturan
    ]);
});

//Page Regulasi Pengawasan APIP
Route::get('/pengawasanapip', function () {
    $peraturan = Peraturan::orderBy('no_urut')->get();
    return view('beranda.pengawasanapip', [
        'peraturans' => $peraturan
    ]);
});

//Page RPJMDes
Route::get('/rpjmdes', function () {
    $asal = Asal::where('kecamatan', '!=', '')
        ->orderBy('kecamatan')
        ->get();
    $rpjmdes = Rpjmd::where(['nama_data' => 'dokumen_rpjmd'])->get();
    return view('beranda.rpjmdes', [
        'asals' => $asal,
        'rpjmdes' => $rpjmdes
    ]);
});

//Page APBDES Desa
Route::resource('apbdes', ApbdesController::class);
Route::post('/apbdes/lihatapbdes', [ApbdesController::class, 'lihatapbdes']);
Route::post('/getDesa', [ApbdesController::class, 'getDesa']);

//Page Peta Desa
Route::get('/peta', function () {
    $peta = Asal::orderBy('asal')->get();
    return view('beranda.peta', [
        'petas' => $peta
    ]);
});

//Page Profil Desa
Route::resource('profil', ProfilController::class);
Route::post('/profil/lihatprofil', [ProfilController::class, 'lihatprofil']);
Route::post('/getDesa', [ProfilController::class, 'getDesa']);

//Page Pengelolaan Keuangan Desa
Route::resource('keudes', KeudesController::class);
Route::post('/keudes/lihatkeudes', [KeudesController::class, 'lihatkeudes']);
Route::post('/getDesa', [KeudesController::class, 'getDesa']);

//Page Penataan Aset Desa
Route::resource('penataanaset', PenataanasetController::class);
Route::post('/penataanaset/lihatpenataanaset', [PenataanasetController::class, 'lihatpenataanaset']);
Route::post('/getDesa', [PenataanasetController::class, 'getDesa']);

//Page BUMDES
Route::resource('bumdes', BumdesController::class);
Route::post('/bumdes/lihatbumdes', [BumdesController::class, 'lihatbumdes']);
Route::post('/getDesa', [BumdesController::class, 'getDesa']);

//Page Administrasi Pemerintahan
Route::resource('admpem', AdmpemController::class);
Route::post('/admpem/lihatadmpem', [AdmpemController::class, 'lihatadmpem']);
Route::post('/getDesa', [AdmpemController::class, 'getDesa']);

//Page Regulasi Perangkat Desa
Route::resource('regulasiperangkat', RegulasiperangkatController::class);
Route::post('/regulasiperangkat/lihatregulasiperangkat', [RegulasiperangkatController::class, 'lihatregulasiperangkat']);
Route::post('/getDesa', [RegulasiperangkatController::class, 'getDesa']);

//Page Data & Informasi Perangkat Desa
Route::resource('dataperangkat', DataperangkatController::class);
Route::post('/dataperangkat/lihatdataperangkat', [DataperangkatController::class, 'lihatdataperangkat']);
Route::post('/getDesa', [DataperangkatController::class, 'getDesa']);

//Page Pengelolaan Keuangan Desa
Route::resource('keudes', KeudesController::class);
Route::post('/keudes/lihatkeudes', [KeudesController::class, 'lihatkeudes']);
Route::post('/getDesa', [KeudesController::class, 'getDesa']);

//Page Pengelolaan Keuangan Desa
Route::resource('aset', AsetController::class);
Route::post('/aset/lihataset', [AsetController::class, 'lihataset']);
Route::post('/getDesa', [AsetController::class, 'getDesa']);


Route::post('/masuk', [LoginController::class, 'cekMasuk']);
Route::get('/login', function () {
    return view('beranda.login');
});

Route::group(['middleware' => ['AuthDesa']], function () {

    Route::get('/adminDesa', [AdminDesaController::class, 'index']);
    Route::post('/logoutDesa', [AdminDesaController::class, 'logout']);
    Route::get('/logoutDesa', [AdminDesaController::class, 'logout']);


    Route::get('/adminDesa/formKewilayahan', [DatumController::class, 'formKewilayahan']);
    Route::post('/adminDesa/tambahDatumWil', [DatumController::class, 'tambahDatumWil']);
    Route::post('/adminDesa/updateDatumWil', [DatumController::class, 'updateDatumWil']);
    Route::post('/adminDesa/tambahDatumDuk', [DatumController::class, 'tambahDatumDuk']);
    Route::post('/adminDesa/updateDatumDuk', [DatumController::class, 'updateDatumDuk']);
    Route::post('/adminDesa/tambahDatumPras', [DatumController::class, 'tambahDatumPras']);
    Route::post('/adminDesa/updateDatumPras', [DatumController::class, 'updateDatumPras']);
    Route::post('/adminDesa/tambahDatumLembaga', [DatumController::class, 'tambahDatumLembaga']);
    Route::post('/adminDesa/updateDatumLembaga', [DatumController::class, 'updateDatumLembaga']);
    Route::post('/adminDesa/copyDatum', [DatumController::class, 'copyDatum']);
    Route::post('/adminDesa/copyDatumAll', [DatumController::class, 'copyDatumAll']);

    Route::get('/adminDesa/formPerangkat', [PerangkatController::class, 'formPerangkat']);
    Route::post('/adminDesa/tambahDatumPer', [PerangkatController::class, 'tambahDatumPer']);
    Route::post('/adminDesa/updateDatumPer', [PerangkatController::class, 'updateDatumPer']);
    Route::post('/adminDesa/copyDatumPer', [PerangkatController::class, 'copyDatumPer']);
    Route::post('/adminDesa/copyDatumPerAll', [PerangkatController::class, 'copyDatumPerAll']);

    Route::get('/adminDesa/formBPD', [BpdController::class, 'formBPD']);
    Route::post('/adminDesa/tambahDatumBpd', [BpdController::class, 'tambahDatumBpd']);
    Route::post('/adminDesa/updateDatumBpd', [BpdController::class, 'updateDatumBpd']);
    Route::post('/adminDesa/copyDatumBpdAll', [BpdController::class, 'copyDatumBpdAll']);
    Route::post('/adminDesa/copyDatumBpd', [BpdController::class, 'copyDatumBpd']);

    Route::get('/adminDesa/formDusun', [DusunController::class, 'formDusun']);
    Route::post('/adminDesa/tambahDatumDus', [DusunController::class, 'tambahDatumDus']);
    Route::post('/adminDesa/updateDatumDus', [DusunController::class, 'updateDatumDus']);
    Route::post('/adminDesa/copyDatumDus', [DusunController::class, 'copyDatumDus']);
    Route::post('/adminDesa/copyDatumDusAll', [DusunController::class, 'copyDatumDusAll']);

    Route::get('/adminDesa/formRT', [RTController::class, 'formRT']);
    Route::post('/adminDesa/tambahDatumRT', [RTController::class, 'tambahDatumRT']);
    Route::post('/adminDesa/updateDatumRT', [RTController::class, 'updateDatumRT']);
    Route::post('/adminDesa/copyDatumRT', [RTController::class, 'copyDatumRT']);
    Route::post('/adminDesa/copyDatumRTAll', [RTController::class, 'copyDatumRTAll']);

    Route::get('/adminDesa/formDokren', [DokrenController::class, 'formDokren']);
    Route::post('/adminDesa/tambahDokren', [DokrenController::class, 'tambahDokren']);
    Route::post('/adminDesa/updateDokren', [DokrenController::class, 'updateDokren']);
    Route::post('/adminDesa/tambahRenfisik', [DokrenController::class, 'tambahRenfisik']);
    Route::post('/adminDesa/tambahRenfisik', [DokrenController::class, 'tambahRenfisik']);
    Route::post('/adminDesa/updateRenfisik', [DokrenController::class, 'updateRenfisik']);
    Route::post('/adminDesa/tambahBlt', [DokrenController::class, 'tambahBlt']);
    Route::post('/adminDesa/updateBlt', [DokrenController::class, 'updateBlt']);

    Route::get('/adminDesa/formAkunwil', [AkunwilController::class, 'formAkunwil']);
    Route::post('/adminDesa/tambahAkunwil', [AkunwilController::class, 'tambahAkunwil']);
    Route::post('/adminDesa/updateAkunwil', [AkunwilController::class, 'updateAkunwil']);
    Route::post('/adminDesa/copyAkunwil', [AkunwilController::class, 'copyAkunwil']);

    Route::get('/adminDesa/formAkunadum', [AkunadumController::class, 'formAkunadum']);
    Route::post('/adminDesa/tambahAkunadum', [AkunadumController::class, 'tambahAkunadum']);
    Route::post('/adminDesa/updateAkunadum', [AkunadumController::class, 'updateAkunadum']);
    Route::post('/adminDesa/copyAkunadum', [AkunadumController::class, 'copyAkunadum']);

    Route::get('/adminDesa/formRpjmd', [RpjmdController::class, 'formRpjmd']);
    Route::post('/adminDesa/tambahRpjmd', [RpjmdController::class, 'tambahRpjmd']);
    Route::post('/adminDesa/updateRpjmd', [RpjmdController::class, 'updateRpjmd']);
    Route::post('/adminDesa/tambahVisimisi', [RpjmdController::class, 'tambahVisimisi']);
    Route::get('/adminDesa/deleteVisimisi/{id}', [RpjmdController::class, 'deleteVisimisi']);
    Route::post('/adminDesa/updateVisimisi', [RpjmdController::class, 'updateVisimisi']);

    Route::post('/adminDesa/updateVisimisi', [RpjmdController::class, 'updateVisimisi']);

    Route::get('/adminDesa/formAkunkel', [AkunkelController::class, 'formAkunkel']);
    Route::post('/adminDesa/tambahAkunkel', [AkunkelController::class, 'tambahAkunkel']);
    Route::post('/adminDesa/updateAkunkel', [AkunkelController::class, 'updateAkunkel']);
    Route::post('/adminDesa/copyAkunkel', [AkunkelController::class, 'copyAkunkel']);

    Route::get('/adminDesa/formAkunadum', [AkunadumController::class, 'formAkunadum']);
    Route::post('/adminDesa/tambahAkunadum', [AkunadumController::class, 'tambahAkunadum']);
    Route::post('/adminDesa/updateAkunadum', [AkunadumController::class, 'updateAkunadum']);
    Route::post('/adminDesa/copyAkunadum', [AkunadumController::class, 'copyAkunadum']);

    //===ASET====// 
    Route::resource('/Akunasetkib', AkunasetkibController::class);
    Route::get('/Akunasetkib/view/{id}', [AkunasetkibController::class, 'viewkib']);
    Route::resource('/Akunasetkiba', AkunasetkibaController::class);
    Route::post('/Akunasetkiba/addkibb', [AkunasetkibaController::class, 'addkibb']);
    Route::post('/Akunasetkiba/addkibc', [AkunasetkibaController::class, 'addkibc']);
    Route::post('/Akunasetkiba/addkibd', [AkunasetkibaController::class, 'addkibd']);
    Route::post('/Akunasetkiba/addkibe', [AkunasetkibaController::class, 'addkibe']);
    Route::post('/Akunasetkiba/addkibf', [AkunasetkibaController::class, 'addkibf']);
    Route::post('/Akunasetkiba/editkibb', [AkunasetkibaController::class, 'editkibb']);
    Route::post('/Akunasetkiba/editkibc', [AkunasetkibaController::class, 'editkibc']);
    Route::post('/Akunasetkiba/editkibd', [AkunasetkibaController::class, 'editkibd']);
    Route::post('/Akunasetkiba/editkibe', [AkunasetkibaController::class, 'editkibe']);
    Route::post('/Akunasetkiba/editkibf', [AkunasetkibaController::class, 'editkibf']);
    Route::post('/Akunasetkiba/copyAkunaset', [AkunasetkibaController::class, 'copyAkunaset']);
    Route::get('/Akunasetkiba/cetak/{id}/{tahun}/{lhi}', [AkunasetkibaController::class, 'cetak']);
    Route::get('/report/word/{id}/{tahun}/{lhi}', [AkunasetkibaController::class, 'word']);
    Route::get('/Akunasetkiba/pdf/{id}/{tahun}/{lhi}', [AkunasetkibaController::class, 'pdf']);
    Route::get('/Akunasetkiba/word/{id}/{tahun}/{lhi}', [AkunasetkibaController::class, 'word']);
    Route::get('/cetak/{id}/{tahun}/{detail}', [CetakController::class, 'index']);

    Route::resource('/Akunasetpenggunaan', AkunasetpenggunaanController::class);
    Route::post('/Akunasetpenggunaan/copyAkunaset', [AkunasetpenggunaanController::class, 'copyAkunaset']);
    Route::post('/Akunasetpenggunaan/hapus', [AkunasetpenggunaanController::class, 'hapus']);
    Route::post('/Akunasetpenggunaan/addpenghapusan', [AkunasetpenggunaanController::class, 'addpenghapusan']);
    Route::post('/Akunasetpenggunaan/updatepenghapusan', [AkunasetpenggunaanController::class, 'updatepenghapusan']);
    Route::post('/Akunasetpenggunaan/hapuspenghapusan', [AkunasetpenggunaanController::class, 'hapuspenghapusan']);
    Route::post('/Akunasetpenggunaan/addbia', [AkunasetpenggunaanController::class, 'addbia']);
    Route::post('/Akunasetpenggunaan/biaupdate', [AkunasetpenggunaanController::class, 'biaupdate']);
    Route::post('/Akunasetpenggunaan/updatepenggunaan', [AkunasetpenggunaanController::class, 'updatepenggunaan']);

    Route::post('/Akunasetbia/copyAkunaset', [AkunasetbiaController::class, 'copyAkunaset']);

    Route::resource('/Akunasetkir', AkunasetkirController::class);
    Route::post('/Akunasetkir/copyAkunaset', [AkunasetkirController::class, 'copyAkunaset']);

    Route::resource('/Akunasetholder', AkunasetholderController::class);
    Route::post('/Akunasetholder/copyAkunaset', [AkunasetholderController::class, 'copyAkunaset']);

    Route::resource('/Akunbumdes', AkunbumdesController::class);
    Route::post('/Akunbumdes/update', [AkunbumdesController::class, 'update']);
    Route::post('/Akunbumdes/copyAkunbumdes', [AkunbumdesController::class, 'copyAkunbumdes']);
    Route::post('/Akunbumdes/akuntabilitas_update', [AkunbumdesController::class, 'akuntabilitas_update']);

    Route::get('/barjas', [BarjasController::class, 'index']);
    Route::post('/barjas/tambahtpk', [BarjasController::class, 'tambahtpk']);
    Route::post('/barjas/edittpk', [BarjasController::class, 'edittpk']);
    Route::post('/barjas/tambahsurvey', [BarjasController::class, 'tambahsurvey']);
    Route::post('/barjas/editsurvey', [BarjasController::class, 'editsurvey']);
    Route::post('/barjas/copybarjas', [BarjasController::class, 'copybarjas']);


    //APBDES
    Route::get('/adminDesa/formApbdes', [ApbdesController::class, 'formApbdes']);
    Route::post('/adminDesa/tambahKegiatanA', [ApbdesController::class, 'tambahKegiatanA']);
    Route::post('/adminDesa/updateKegiatanA', [ApbdesController::class, 'updateKegiatanA']);
    Route::post('/adminDesa/cekKegiatanA', [ApbdesController::class, 'cekKegiatanA']);
    Route::post('/adminDesa/cekUpdateKegiatanA', [ApbdesController::class, 'cekUpdateKegiatanA']);
    Route::post('/adminDesa/tambahPendapatanA', [ApbdesController::class, 'tambahPendapatanA']);
    Route::post('/adminDesa/updatePendapatanA', [ApbdesController::class, 'updatePendapatanA']);
    Route::post('/adminDesa/tambahPembiayaanA', [ApbdesController::class, 'tambahPembiayaanA']);
    Route::post('/adminDesa/updatePembiayaanA', [ApbdesController::class, 'updatePembiayaanA']);
    Route::post('/adminDesa/tambahDokumenA', [ApbdesController::class, 'tambahDokumenA']);
    Route::post('/adminDesa/updateDokumenA', [ApbdesController::class, 'updateDokumenA']);
    Route::post('/adminDesa/tambahBelanjaA', [ApbdesController::class, 'tambahBelanjaA']);
    Route::post('/adminDesa/updateBelanjaA', [ApbdesController::class, 'updateBelanjaA']);

    Route::get('/adminDesa/formApbdesP', [ApbdesPerubahanController::class, 'formApbdesP']);
    Route::post('/adminDesa/tambahPendapatanP', [ApbdesPerubahanController::class, 'tambahPendapatanP']);
    Route::post('/adminDesa/tambahBelanjaP', [ApbdesPerubahanController::class, 'tambahBelanjaP']);
    Route::post('/adminDesa/tambahKegiatanP', [ApbdesPerubahanController::class, 'tambahKegiatanP']);
    Route::post('/adminDesa/tambahDokumenP', [ApbdesPerubahanController::class, 'tambahDokumenP']);
    Route::post('/adminDesa/cekKegiatanP', [ApbdesPerubahanController::class, 'cekKegiatanP']);
    Route::post('/adminDesa/tambahPembiayaanP', [ApbdesPerubahanController::class, 'tambahPembiayaanP']);

    //Penatausahaan Pendapatan
    Route::get('/adminDesa/formPenataanPendapatan', [PenataanPendapatanController::class, 'formPenataanPendapatan']);
    Route::post('/adminDesa/tambahPengajuan', [PenataanPendapatanController::class, 'tambahPengajuan']);
    Route::post('/adminDesa/updatePengajuan', [PenataanPendapatanController::class, 'updatePengajuan']);
    Route::get('/adminDesa/deletePengajuan/{id}', [PenataanPendapatanController::class, 'deletePengajuan']);
    Route::post('/adminDesa/tambahBukuBank', [PenataanPendapatanController::class, 'tambahBukuBank']);
    Route::post('/adminDesa/updateBukuBank', [PenataanPendapatanController::class, 'updateBukuBank']);

    //Belanja
    Route::get('/adminDesa/formPenataanBelanja', [PenataanBelanjaController::class, 'formPenataanBelanja']);
    Route::post('/adminDesa/tambahSPP', [PenataanBelanjaController::class, 'tambahSPP']);
    Route::post('/adminDesa/tambahTBPU', [PenataanBelanjaController::class, 'tambahTBPU']);
    Route::post('/adminDesa/updateTBPU', [PenataanBelanjaController::class, 'updateTBPU']);
    Route::post('/adminDesa/hapusTBPU/{id}', [PenataanBelanjaController::class, 'hapusTBPU']);
    Route::get('/adminDesa/cekDokTbpu', [PenataanBelanjaController::class, 'cekDokTbpu']);
    Route::get('/adminDesa/cekDokSPP', [PenataanBelanjaController::class, 'cekDokSPP']);
    Route::post('/adminDesa/updateSPP', [PenataanBelanjaController::class, 'updateSPP']);
    Route::post('/adminDesa/hapusSPP/{id}', [PenataanBelanjaController::class, 'hapusSPP']);
    Route::get('/adminDesa/cariSPP', [PenataanBelanjaController::class, 'cariSPP']);

    //Pajak
    Route::get('/adminDesa/formPenataanPajak', [PenataanPajakController::class, 'formPenataanPajak']);
    Route::post('/adminDesa/tambahBilling', [PenataanPajakController::class, 'tambahBilling']);
    Route::post('/adminDesa/hapusBilling', [PenataanPajakController::class, 'hapusBilling']);
});

Route::group(['middleware' => ['AuthEditor']], function () {
    Route::get('/editor', [EditorController::class, 'index']);
    Route::post('/logoutEditor', [EditorController::class, 'logoutEditor']);
    Route::get('/logoutEditor', [EditorController::class, 'logoutEditor']);
    Route::resource('berita', BeritaController::class);
    Route::get('berita/edit/{berita}', [BeritaController::class, 'edit']);
    Route::post('berita/update', [BeritaController::class, 'update']);
    Route::get('/logoutEditor', [EditorController::class, 'logoutEditor']);
    Route::resource('galeri', GaleriController::class);
    Route::post('galeri/update', [GaleriController::class, 'update']);

    Route::resource('peraturan', PeraturanController::class);
    Route::resource('pengumuman', PengumumanController::class);
});

Route::group(['middleware' => ['AuthIrban']], function () {
    Route::get('/irban', [IrbanController::class, 'index']);
    Route::get('/logoutIrban', [IrbanController::class, 'logoutIrban']);
    Route::get('/irban/kewilayahan', [IrbanController::class, 'kewilayahan']);
    Route::get('/irban/kelembagaan', [IrbanController::class, 'kelembagaan']);
    Route::post('/irban/getDesa', [IrbanController::class, 'getDesa']);
    Route::post('/irban/getKecamatan', [IrbanController::class, 'getKecamatan']);
    Route::post('/irban/sesi', [IrbanController::class, 'sesi']);
    Route::post('/irban/nilai_kewilayahan', [IrbanController::class, 'nilai_kewilayahan']);
    Route::get('/lihat/{subindikator}/{id}/{tahun}', [IrbanController::class, 'lihat']);
    Route::get('/irban/monografi', [IrbanController::class, 'monografi']);
    Route::get('/lihatkelembagaan/{subindikator}/{id}/{tahun}', [IrbanController::class, 'lihatkelembagaan']);
    Route::post('/irban/nilaipemdes', [IrbanController::class, 'nilaipemdes']);
    Route::post('/irban/nilai_pddusia', [IrbanController::class, 'nilai_pddusia']);
    Route::get('/irban/viewnilai_pddusia/{id}/{tahun}', [IrbanController::class, 'viewnilai_pddusia']);
    Route::post('/irban/nilai_pddmiskin', [IrbanController::class, 'nilai_pddmiskin']);
    Route::post('/irban/nilai_pddusia', [IrbanController::class, 'nilai_pddusia']);
    Route::get('/lihat/{subindikator}/{id}/{tahun}', [IrbanController::class, 'lihat']);
    Route::get('/irban/monografi', [IrbanController::class, 'monografi']);
    Route::post('/irban/nilaipemdes', [IrbanController::class, 'nilaipemdes']);
    Route::post('/irban/updatenilaipemdes', [IrbanController::class, 'updatenilaipemdes']);
    Route::get('/irban/perencanaan', [IrbanController::class, 'perencanaan']);
    Route::get('/lihat_perencanaan/{subindikator}/{id}/{tahun}', [IrbanController::class, 'lihat_perencanaan']);
});

Route::group(['middleware' => ['AuthAdmin']], function () {
    Route::get('/admin', [AdminSimondesController::class, 'index']);
    Route::get('/logoutAdmin', [AdminSimondesController::class, 'logoutAdmin']);
    Route::get('/admin/akun', [AdminSimondesController::class, 'akun']);
    Route::get('/admin/aspek', [AdminSimondesController::class, 'aspek']);
    Route::get('/admin/wilayah', [AdminSimondesController::class, 'wilayah']);
    Route::post('/admin/wilayah_update/{id}', [AdminSimondesController::class, 'wilayah_update']);
    Route::post('/admin/addobrik', [AdminSimondesController::class, 'addobrik']);
    Route::get('/admin/deleteobrik/{id}', [AdminSimondesController::class, 'deleteobrik']);
    Route::get('/admin/pilihtahun', [AdminSimondesController::class, 'pilihtahun']);
    Route::post('/admin/tambahirban', [AdminSimondesController::class, 'tambahirban']);
    Route::post('/admin/update/{id}', [AdminSimondesController::class, 'ubahirban']);
    Route::get('/admin/delete/{id}', [AdminSimondesController::class, 'delete']);
});


Route::get('/register', [RegisterController::class, 'index']);
Route::get('/register/tambah', [RegisterController::class, 'formTambah']);
Route::post('/register/store', [RegisterController::class, 'store']);
Route::post('/register/delete/{data}', [RegisterController::class, 'delete']);
